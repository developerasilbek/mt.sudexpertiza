package uz.sudex.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import uz.sudex.IntegrationTest;
import uz.sudex.domain.Attachment;
import uz.sudex.domain.Course;
import uz.sudex.domain.Listener;
import uz.sudex.domain.User;
import uz.sudex.domain.enumeration.ConditionType;
import uz.sudex.domain.enumeration.StudyType;
import uz.sudex.repository.ListenerRepository;
import uz.sudex.service.criteria.ListenerCriteria;
import uz.sudex.service.dto.ListenerDTO;
import uz.sudex.service.mapper.ListenerMapper;

/**
 * Integration tests for the {@link ListenerResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ListenerResourceIT {

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_REGION = "AAAAAAAAAA";
    private static final String UPDATED_REGION = "BBBBBBBBBB";

    private static final String DEFAULT_WORKPLACE = "AAAAAAAAAA";
    private static final String UPDATED_WORKPLACE = "BBBBBBBBBB";

    private static final String DEFAULT_DATE_OF_BIRTH = "AAAAAAAAAA";
    private static final String UPDATED_DATE_OF_BIRTH = "BBBBBBBBBB";

    private static final ConditionType DEFAULT_CONDITION = ConditionType.WAITHING;
    private static final ConditionType UPDATED_CONDITION = ConditionType.INPROGRESS;

    private static final StudyType DEFAULT_TYPE = StudyType.SHARTNOMA;
    private static final StudyType UPDATED_TYPE = StudyType.REJA_BUYICHA;

    private static final String ENTITY_API_URL = "/api/listeners";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ListenerRepository listenerRepository;

    @Autowired
    private ListenerMapper listenerMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restListenerMockMvc;

    private Listener listener;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Listener createEntity(EntityManager em) {
        Listener listener = new Listener()
            .phone(DEFAULT_PHONE)
            .region(DEFAULT_REGION)
            .workplace(DEFAULT_WORKPLACE)
            .dateOfBirth(DEFAULT_DATE_OF_BIRTH)
            .condition(DEFAULT_CONDITION)
            .type(DEFAULT_TYPE);
        return listener;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Listener createUpdatedEntity(EntityManager em) {
        Listener listener = new Listener()
            .phone(UPDATED_PHONE)
            .region(UPDATED_REGION)
            .workplace(UPDATED_WORKPLACE)
            .dateOfBirth(UPDATED_DATE_OF_BIRTH)
            .condition(UPDATED_CONDITION)
            .type(UPDATED_TYPE);
        return listener;
    }

    @BeforeEach
    public void initTest() {
        listener = createEntity(em);
    }

    @Test
    @Transactional
    void createListener() throws Exception {
        int databaseSizeBeforeCreate = listenerRepository.findAll().size();
        // Create the Listener
        ListenerDTO listenerDTO = listenerMapper.toDto(listener);
        restListenerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(listenerDTO)))
            .andExpect(status().isCreated());

        // Validate the Listener in the database
        List<Listener> listenerList = listenerRepository.findAll();
        assertThat(listenerList).hasSize(databaseSizeBeforeCreate + 1);
        Listener testListener = listenerList.get(listenerList.size() - 1);
        assertThat(testListener.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testListener.getRegion()).isEqualTo(DEFAULT_REGION);
        assertThat(testListener.getWorkplace()).isEqualTo(DEFAULT_WORKPLACE);
        assertThat(testListener.getDateOfBirth()).isEqualTo(DEFAULT_DATE_OF_BIRTH);
        assertThat(testListener.getCondition()).isEqualTo(DEFAULT_CONDITION);
        assertThat(testListener.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    @Transactional
    void createListenerWithExistingId() throws Exception {
        // Create the Listener with an existing ID
        listener.setId(1L);
        ListenerDTO listenerDTO = listenerMapper.toDto(listener);

        int databaseSizeBeforeCreate = listenerRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restListenerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(listenerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Listener in the database
        List<Listener> listenerList = listenerRepository.findAll();
        assertThat(listenerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkPhoneIsRequired() throws Exception {
        int databaseSizeBeforeTest = listenerRepository.findAll().size();
        // set the field null
        listener.setPhone(null);

        // Create the Listener, which fails.
        ListenerDTO listenerDTO = listenerMapper.toDto(listener);

        restListenerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(listenerDTO)))
            .andExpect(status().isBadRequest());

        List<Listener> listenerList = listenerRepository.findAll();
        assertThat(listenerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllListeners() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList
        restListenerMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(listener.getId().intValue())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].region").value(hasItem(DEFAULT_REGION)))
            .andExpect(jsonPath("$.[*].workplace").value(hasItem(DEFAULT_WORKPLACE)))
            .andExpect(jsonPath("$.[*].dateOfBirth").value(hasItem(DEFAULT_DATE_OF_BIRTH)))
            .andExpect(jsonPath("$.[*].condition").value(hasItem(DEFAULT_CONDITION.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }

    @Test
    @Transactional
    void getListener() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get the listener
        restListenerMockMvc
            .perform(get(ENTITY_API_URL_ID, listener.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(listener.getId().intValue()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
            .andExpect(jsonPath("$.region").value(DEFAULT_REGION))
            .andExpect(jsonPath("$.workplace").value(DEFAULT_WORKPLACE))
            .andExpect(jsonPath("$.dateOfBirth").value(DEFAULT_DATE_OF_BIRTH))
            .andExpect(jsonPath("$.condition").value(DEFAULT_CONDITION.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }

    @Test
    @Transactional
    void getListenersByIdFiltering() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        Long id = listener.getId();

        defaultListenerShouldBeFound("id.equals=" + id);
        defaultListenerShouldNotBeFound("id.notEquals=" + id);

        defaultListenerShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultListenerShouldNotBeFound("id.greaterThan=" + id);

        defaultListenerShouldBeFound("id.lessThanOrEqual=" + id);
        defaultListenerShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllListenersByPhoneIsEqualToSomething() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where phone equals to DEFAULT_PHONE
        defaultListenerShouldBeFound("phone.equals=" + DEFAULT_PHONE);

        // Get all the listenerList where phone equals to UPDATED_PHONE
        defaultListenerShouldNotBeFound("phone.equals=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    void getAllListenersByPhoneIsInShouldWork() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where phone in DEFAULT_PHONE or UPDATED_PHONE
        defaultListenerShouldBeFound("phone.in=" + DEFAULT_PHONE + "," + UPDATED_PHONE);

        // Get all the listenerList where phone equals to UPDATED_PHONE
        defaultListenerShouldNotBeFound("phone.in=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    void getAllListenersByPhoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where phone is not null
        defaultListenerShouldBeFound("phone.specified=true");

        // Get all the listenerList where phone is null
        defaultListenerShouldNotBeFound("phone.specified=false");
    }

    @Test
    @Transactional
    void getAllListenersByPhoneContainsSomething() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where phone contains DEFAULT_PHONE
        defaultListenerShouldBeFound("phone.contains=" + DEFAULT_PHONE);

        // Get all the listenerList where phone contains UPDATED_PHONE
        defaultListenerShouldNotBeFound("phone.contains=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    void getAllListenersByPhoneNotContainsSomething() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where phone does not contain DEFAULT_PHONE
        defaultListenerShouldNotBeFound("phone.doesNotContain=" + DEFAULT_PHONE);

        // Get all the listenerList where phone does not contain UPDATED_PHONE
        defaultListenerShouldBeFound("phone.doesNotContain=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    void getAllListenersByRegionIsEqualToSomething() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where region equals to DEFAULT_REGION
        defaultListenerShouldBeFound("region.equals=" + DEFAULT_REGION);

        // Get all the listenerList where region equals to UPDATED_REGION
        defaultListenerShouldNotBeFound("region.equals=" + UPDATED_REGION);
    }

    @Test
    @Transactional
    void getAllListenersByRegionIsInShouldWork() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where region in DEFAULT_REGION or UPDATED_REGION
        defaultListenerShouldBeFound("region.in=" + DEFAULT_REGION + "," + UPDATED_REGION);

        // Get all the listenerList where region equals to UPDATED_REGION
        defaultListenerShouldNotBeFound("region.in=" + UPDATED_REGION);
    }

    @Test
    @Transactional
    void getAllListenersByRegionIsNullOrNotNull() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where region is not null
        defaultListenerShouldBeFound("region.specified=true");

        // Get all the listenerList where region is null
        defaultListenerShouldNotBeFound("region.specified=false");
    }

    @Test
    @Transactional
    void getAllListenersByRegionContainsSomething() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where region contains DEFAULT_REGION
        defaultListenerShouldBeFound("region.contains=" + DEFAULT_REGION);

        // Get all the listenerList where region contains UPDATED_REGION
        defaultListenerShouldNotBeFound("region.contains=" + UPDATED_REGION);
    }

    @Test
    @Transactional
    void getAllListenersByRegionNotContainsSomething() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where region does not contain DEFAULT_REGION
        defaultListenerShouldNotBeFound("region.doesNotContain=" + DEFAULT_REGION);

        // Get all the listenerList where region does not contain UPDATED_REGION
        defaultListenerShouldBeFound("region.doesNotContain=" + UPDATED_REGION);
    }

    @Test
    @Transactional
    void getAllListenersByWorkplaceIsEqualToSomething() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where workplace equals to DEFAULT_WORKPLACE
        defaultListenerShouldBeFound("workplace.equals=" + DEFAULT_WORKPLACE);

        // Get all the listenerList where workplace equals to UPDATED_WORKPLACE
        defaultListenerShouldNotBeFound("workplace.equals=" + UPDATED_WORKPLACE);
    }

    @Test
    @Transactional
    void getAllListenersByWorkplaceIsInShouldWork() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where workplace in DEFAULT_WORKPLACE or UPDATED_WORKPLACE
        defaultListenerShouldBeFound("workplace.in=" + DEFAULT_WORKPLACE + "," + UPDATED_WORKPLACE);

        // Get all the listenerList where workplace equals to UPDATED_WORKPLACE
        defaultListenerShouldNotBeFound("workplace.in=" + UPDATED_WORKPLACE);
    }

    @Test
    @Transactional
    void getAllListenersByWorkplaceIsNullOrNotNull() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where workplace is not null
        defaultListenerShouldBeFound("workplace.specified=true");

        // Get all the listenerList where workplace is null
        defaultListenerShouldNotBeFound("workplace.specified=false");
    }

    @Test
    @Transactional
    void getAllListenersByWorkplaceContainsSomething() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where workplace contains DEFAULT_WORKPLACE
        defaultListenerShouldBeFound("workplace.contains=" + DEFAULT_WORKPLACE);

        // Get all the listenerList where workplace contains UPDATED_WORKPLACE
        defaultListenerShouldNotBeFound("workplace.contains=" + UPDATED_WORKPLACE);
    }

    @Test
    @Transactional
    void getAllListenersByWorkplaceNotContainsSomething() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where workplace does not contain DEFAULT_WORKPLACE
        defaultListenerShouldNotBeFound("workplace.doesNotContain=" + DEFAULT_WORKPLACE);

        // Get all the listenerList where workplace does not contain UPDATED_WORKPLACE
        defaultListenerShouldBeFound("workplace.doesNotContain=" + UPDATED_WORKPLACE);
    }

    @Test
    @Transactional
    void getAllListenersByDateOfBirthIsEqualToSomething() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where dateOfBirth equals to DEFAULT_DATE_OF_BIRTH
        defaultListenerShouldBeFound("dateOfBirth.equals=" + DEFAULT_DATE_OF_BIRTH);

        // Get all the listenerList where dateOfBirth equals to UPDATED_DATE_OF_BIRTH
        defaultListenerShouldNotBeFound("dateOfBirth.equals=" + UPDATED_DATE_OF_BIRTH);
    }

    @Test
    @Transactional
    void getAllListenersByDateOfBirthIsInShouldWork() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where dateOfBirth in DEFAULT_DATE_OF_BIRTH or UPDATED_DATE_OF_BIRTH
        defaultListenerShouldBeFound("dateOfBirth.in=" + DEFAULT_DATE_OF_BIRTH + "," + UPDATED_DATE_OF_BIRTH);

        // Get all the listenerList where dateOfBirth equals to UPDATED_DATE_OF_BIRTH
        defaultListenerShouldNotBeFound("dateOfBirth.in=" + UPDATED_DATE_OF_BIRTH);
    }

    @Test
    @Transactional
    void getAllListenersByDateOfBirthIsNullOrNotNull() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where dateOfBirth is not null
        defaultListenerShouldBeFound("dateOfBirth.specified=true");

        // Get all the listenerList where dateOfBirth is null
        defaultListenerShouldNotBeFound("dateOfBirth.specified=false");
    }

    @Test
    @Transactional
    void getAllListenersByDateOfBirthContainsSomething() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where dateOfBirth contains DEFAULT_DATE_OF_BIRTH
        defaultListenerShouldBeFound("dateOfBirth.contains=" + DEFAULT_DATE_OF_BIRTH);

        // Get all the listenerList where dateOfBirth contains UPDATED_DATE_OF_BIRTH
        defaultListenerShouldNotBeFound("dateOfBirth.contains=" + UPDATED_DATE_OF_BIRTH);
    }

    @Test
    @Transactional
    void getAllListenersByDateOfBirthNotContainsSomething() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where dateOfBirth does not contain DEFAULT_DATE_OF_BIRTH
        defaultListenerShouldNotBeFound("dateOfBirth.doesNotContain=" + DEFAULT_DATE_OF_BIRTH);

        // Get all the listenerList where dateOfBirth does not contain UPDATED_DATE_OF_BIRTH
        defaultListenerShouldBeFound("dateOfBirth.doesNotContain=" + UPDATED_DATE_OF_BIRTH);
    }

    @Test
    @Transactional
    void getAllListenersByConditionIsEqualToSomething() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where condition equals to DEFAULT_CONDITION
        defaultListenerShouldBeFound("condition.equals=" + DEFAULT_CONDITION);

        // Get all the listenerList where condition equals to UPDATED_CONDITION
        defaultListenerShouldNotBeFound("condition.equals=" + UPDATED_CONDITION);
    }

    @Test
    @Transactional
    void getAllListenersByConditionIsInShouldWork() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where condition in DEFAULT_CONDITION or UPDATED_CONDITION
        defaultListenerShouldBeFound("condition.in=" + DEFAULT_CONDITION + "," + UPDATED_CONDITION);

        // Get all the listenerList where condition equals to UPDATED_CONDITION
        defaultListenerShouldNotBeFound("condition.in=" + UPDATED_CONDITION);
    }

    @Test
    @Transactional
    void getAllListenersByConditionIsNullOrNotNull() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where condition is not null
        defaultListenerShouldBeFound("condition.specified=true");

        // Get all the listenerList where condition is null
        defaultListenerShouldNotBeFound("condition.specified=false");
    }

    @Test
    @Transactional
    void getAllListenersByTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where type equals to DEFAULT_TYPE
        defaultListenerShouldBeFound("type.equals=" + DEFAULT_TYPE);

        // Get all the listenerList where type equals to UPDATED_TYPE
        defaultListenerShouldNotBeFound("type.equals=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    void getAllListenersByTypeIsInShouldWork() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where type in DEFAULT_TYPE or UPDATED_TYPE
        defaultListenerShouldBeFound("type.in=" + DEFAULT_TYPE + "," + UPDATED_TYPE);

        // Get all the listenerList where type equals to UPDATED_TYPE
        defaultListenerShouldNotBeFound("type.in=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    void getAllListenersByTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        // Get all the listenerList where type is not null
        defaultListenerShouldBeFound("type.specified=true");

        // Get all the listenerList where type is null
        defaultListenerShouldNotBeFound("type.specified=false");
    }

    @Test
    @Transactional
    void getAllListenersByUserIsEqualToSomething() throws Exception {
        User user;
        if (TestUtil.findAll(em, User.class).isEmpty()) {
            listenerRepository.saveAndFlush(listener);
            user = UserResourceIT.createEntity(em);
        } else {
            user = TestUtil.findAll(em, User.class).get(0);
        }
        em.persist(user);
        em.flush();
        listener.setUser(user);
        listenerRepository.saveAndFlush(listener);
        Long userId = user.getId();

        // Get all the listenerList where user equals to userId
        defaultListenerShouldBeFound("userId.equals=" + userId);

        // Get all the listenerList where user equals to (userId + 1)
        defaultListenerShouldNotBeFound("userId.equals=" + (userId + 1));
    }

    @Test
    @Transactional
    void getAllListenersByContractIsEqualToSomething() throws Exception {
        Attachment contract;
        if (TestUtil.findAll(em, Attachment.class).isEmpty()) {
            listenerRepository.saveAndFlush(listener);
            contract = AttachmentResourceIT.createEntity(em);
        } else {
            contract = TestUtil.findAll(em, Attachment.class).get(0);
        }
        em.persist(contract);
        em.flush();
        listener.setContract(contract);
        listenerRepository.saveAndFlush(listener);
        Long contractId = contract.getId();

        // Get all the listenerList where contract equals to contractId
        defaultListenerShouldBeFound("contractId.equals=" + contractId);

        // Get all the listenerList where contract equals to (contractId + 1)
        defaultListenerShouldNotBeFound("contractId.equals=" + (contractId + 1));
    }

    @Test
    @Transactional
    void getAllListenersByCourseIsEqualToSomething() throws Exception {
        Course course;
        if (TestUtil.findAll(em, Course.class).isEmpty()) {
            listenerRepository.saveAndFlush(listener);
            course = CourseResourceIT.createEntity(em);
        } else {
            course = TestUtil.findAll(em, Course.class).get(0);
        }
        em.persist(course);
        em.flush();
        listener.setCourse(course);
        listenerRepository.saveAndFlush(listener);
        Long courseId = course.getId();

        // Get all the listenerList where course equals to courseId
        defaultListenerShouldBeFound("courseId.equals=" + courseId);

        // Get all the listenerList where course equals to (courseId + 1)
        defaultListenerShouldNotBeFound("courseId.equals=" + (courseId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultListenerShouldBeFound(String filter) throws Exception {
        restListenerMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(listener.getId().intValue())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].region").value(hasItem(DEFAULT_REGION)))
            .andExpect(jsonPath("$.[*].workplace").value(hasItem(DEFAULT_WORKPLACE)))
            .andExpect(jsonPath("$.[*].dateOfBirth").value(hasItem(DEFAULT_DATE_OF_BIRTH)))
            .andExpect(jsonPath("$.[*].condition").value(hasItem(DEFAULT_CONDITION.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));

        // Check, that the count call also returns 1
        restListenerMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultListenerShouldNotBeFound(String filter) throws Exception {
        restListenerMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restListenerMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingListener() throws Exception {
        // Get the listener
        restListenerMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingListener() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        int databaseSizeBeforeUpdate = listenerRepository.findAll().size();

        // Update the listener
        Listener updatedListener = listenerRepository.findById(listener.getId()).get();
        // Disconnect from session so that the updates on updatedListener are not directly saved in db
        em.detach(updatedListener);
        updatedListener
            .phone(UPDATED_PHONE)
            .region(UPDATED_REGION)
            .workplace(UPDATED_WORKPLACE)
            .dateOfBirth(UPDATED_DATE_OF_BIRTH)
            .condition(UPDATED_CONDITION)
            .type(UPDATED_TYPE);
        ListenerDTO listenerDTO = listenerMapper.toDto(updatedListener);

        restListenerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, listenerDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(listenerDTO))
            )
            .andExpect(status().isOk());

        // Validate the Listener in the database
        List<Listener> listenerList = listenerRepository.findAll();
        assertThat(listenerList).hasSize(databaseSizeBeforeUpdate);
        Listener testListener = listenerList.get(listenerList.size() - 1);
        assertThat(testListener.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testListener.getRegion()).isEqualTo(UPDATED_REGION);
        assertThat(testListener.getWorkplace()).isEqualTo(UPDATED_WORKPLACE);
        assertThat(testListener.getDateOfBirth()).isEqualTo(UPDATED_DATE_OF_BIRTH);
        assertThat(testListener.getCondition()).isEqualTo(UPDATED_CONDITION);
        assertThat(testListener.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    void putNonExistingListener() throws Exception {
        int databaseSizeBeforeUpdate = listenerRepository.findAll().size();
        listener.setId(count.incrementAndGet());

        // Create the Listener
        ListenerDTO listenerDTO = listenerMapper.toDto(listener);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restListenerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, listenerDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(listenerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Listener in the database
        List<Listener> listenerList = listenerRepository.findAll();
        assertThat(listenerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchListener() throws Exception {
        int databaseSizeBeforeUpdate = listenerRepository.findAll().size();
        listener.setId(count.incrementAndGet());

        // Create the Listener
        ListenerDTO listenerDTO = listenerMapper.toDto(listener);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restListenerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(listenerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Listener in the database
        List<Listener> listenerList = listenerRepository.findAll();
        assertThat(listenerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamListener() throws Exception {
        int databaseSizeBeforeUpdate = listenerRepository.findAll().size();
        listener.setId(count.incrementAndGet());

        // Create the Listener
        ListenerDTO listenerDTO = listenerMapper.toDto(listener);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restListenerMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(listenerDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Listener in the database
        List<Listener> listenerList = listenerRepository.findAll();
        assertThat(listenerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateListenerWithPatch() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        int databaseSizeBeforeUpdate = listenerRepository.findAll().size();

        // Update the listener using partial update
        Listener partialUpdatedListener = new Listener();
        partialUpdatedListener.setId(listener.getId());

        partialUpdatedListener.region(UPDATED_REGION).type(UPDATED_TYPE);

        restListenerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedListener.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedListener))
            )
            .andExpect(status().isOk());

        // Validate the Listener in the database
        List<Listener> listenerList = listenerRepository.findAll();
        assertThat(listenerList).hasSize(databaseSizeBeforeUpdate);
        Listener testListener = listenerList.get(listenerList.size() - 1);
        assertThat(testListener.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testListener.getRegion()).isEqualTo(UPDATED_REGION);
        assertThat(testListener.getWorkplace()).isEqualTo(DEFAULT_WORKPLACE);
        assertThat(testListener.getDateOfBirth()).isEqualTo(DEFAULT_DATE_OF_BIRTH);
        assertThat(testListener.getCondition()).isEqualTo(DEFAULT_CONDITION);
        assertThat(testListener.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    void fullUpdateListenerWithPatch() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        int databaseSizeBeforeUpdate = listenerRepository.findAll().size();

        // Update the listener using partial update
        Listener partialUpdatedListener = new Listener();
        partialUpdatedListener.setId(listener.getId());

        partialUpdatedListener
            .phone(UPDATED_PHONE)
            .region(UPDATED_REGION)
            .workplace(UPDATED_WORKPLACE)
            .dateOfBirth(UPDATED_DATE_OF_BIRTH)
            .condition(UPDATED_CONDITION)
            .type(UPDATED_TYPE);

        restListenerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedListener.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedListener))
            )
            .andExpect(status().isOk());

        // Validate the Listener in the database
        List<Listener> listenerList = listenerRepository.findAll();
        assertThat(listenerList).hasSize(databaseSizeBeforeUpdate);
        Listener testListener = listenerList.get(listenerList.size() - 1);
        assertThat(testListener.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testListener.getRegion()).isEqualTo(UPDATED_REGION);
        assertThat(testListener.getWorkplace()).isEqualTo(UPDATED_WORKPLACE);
        assertThat(testListener.getDateOfBirth()).isEqualTo(UPDATED_DATE_OF_BIRTH);
        assertThat(testListener.getCondition()).isEqualTo(UPDATED_CONDITION);
        assertThat(testListener.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    void patchNonExistingListener() throws Exception {
        int databaseSizeBeforeUpdate = listenerRepository.findAll().size();
        listener.setId(count.incrementAndGet());

        // Create the Listener
        ListenerDTO listenerDTO = listenerMapper.toDto(listener);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restListenerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, listenerDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(listenerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Listener in the database
        List<Listener> listenerList = listenerRepository.findAll();
        assertThat(listenerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchListener() throws Exception {
        int databaseSizeBeforeUpdate = listenerRepository.findAll().size();
        listener.setId(count.incrementAndGet());

        // Create the Listener
        ListenerDTO listenerDTO = listenerMapper.toDto(listener);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restListenerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(listenerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Listener in the database
        List<Listener> listenerList = listenerRepository.findAll();
        assertThat(listenerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamListener() throws Exception {
        int databaseSizeBeforeUpdate = listenerRepository.findAll().size();
        listener.setId(count.incrementAndGet());

        // Create the Listener
        ListenerDTO listenerDTO = listenerMapper.toDto(listener);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restListenerMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(listenerDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Listener in the database
        List<Listener> listenerList = listenerRepository.findAll();
        assertThat(listenerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteListener() throws Exception {
        // Initialize the database
        listenerRepository.saveAndFlush(listener);

        int databaseSizeBeforeDelete = listenerRepository.findAll().size();

        // Delete the listener
        restListenerMockMvc
            .perform(delete(ENTITY_API_URL_ID, listener.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Listener> listenerList = listenerRepository.findAll();
        assertThat(listenerList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
