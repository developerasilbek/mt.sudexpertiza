package uz.sudex.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import uz.sudex.IntegrationTest;
import uz.sudex.domain.Course;
import uz.sudex.domain.enumeration.Centre;
import uz.sudex.repository.CourseRepository;
import uz.sudex.service.criteria.CourseCriteria;
import uz.sudex.service.dto.CourseDTO;
import uz.sudex.service.mapper.CourseMapper;

/**
 * Integration tests for the {@link CourseResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CourseResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Centre DEFAULT_CENTRE = Centre.RSEM;
    private static final Centre UPDATED_CENTRE = Centre.RSEM;

    private static final String DEFAULT_LANGUAGE = "AAAAAAAAAA";
    private static final String UPDATED_LANGUAGE = "BBBBBBBBBB";

    private static final Integer DEFAULT_TEACHING_HOUR = 1;
    private static final Integer UPDATED_TEACHING_HOUR = 2;
    private static final Integer SMALLER_TEACHING_HOUR = 1 - 1;

    private static final Instant DEFAULT_STARTED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_STARTED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_FINISHED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FINISHED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/courses";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private CourseMapper courseMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCourseMockMvc;

    private Course course;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Course createEntity(EntityManager em) {
        Course course = new Course()
            .name(DEFAULT_NAME)
            .centre(DEFAULT_CENTRE)
            .language(DEFAULT_LANGUAGE)
            .teachingHour(DEFAULT_TEACHING_HOUR)
            .startedAt(DEFAULT_STARTED_AT)
            .finishedAt(DEFAULT_FINISHED_AT);
        return course;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Course createUpdatedEntity(EntityManager em) {
        Course course = new Course()
            .name(UPDATED_NAME)
            .centre(UPDATED_CENTRE)
            .language(UPDATED_LANGUAGE)
            .teachingHour(UPDATED_TEACHING_HOUR)
            .startedAt(UPDATED_STARTED_AT)
            .finishedAt(UPDATED_FINISHED_AT);
        return course;
    }

    @BeforeEach
    public void initTest() {
        course = createEntity(em);
    }

    @Test
    @Transactional
    void createCourse() throws Exception {
        int databaseSizeBeforeCreate = courseRepository.findAll().size();
        // Create the Course
        CourseDTO courseDTO = courseMapper.toDto(course);
        restCourseMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(courseDTO)))
            .andExpect(status().isCreated());

        // Validate the Course in the database
        List<Course> courseList = courseRepository.findAll();
        assertThat(courseList).hasSize(databaseSizeBeforeCreate + 1);
        Course testCourse = courseList.get(courseList.size() - 1);
        assertThat(testCourse.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCourse.getCentre()).isEqualTo(DEFAULT_CENTRE);
        assertThat(testCourse.getLanguage()).isEqualTo(DEFAULT_LANGUAGE);
        assertThat(testCourse.getTeachingHour()).isEqualTo(DEFAULT_TEACHING_HOUR);
        assertThat(testCourse.getStartedAt()).isEqualTo(DEFAULT_STARTED_AT);
        assertThat(testCourse.getFinishedAt()).isEqualTo(DEFAULT_FINISHED_AT);
    }

    @Test
    @Transactional
    void createCourseWithExistingId() throws Exception {
        // Create the Course with an existing ID
        course.setId(1L);
        CourseDTO courseDTO = courseMapper.toDto(course);

        int databaseSizeBeforeCreate = courseRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCourseMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(courseDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Course in the database
        List<Course> courseList = courseRepository.findAll();
        assertThat(courseList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllCourses() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList
        restCourseMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(course.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].centre").value(hasItem(DEFAULT_CENTRE.toString())))
            .andExpect(jsonPath("$.[*].language").value(hasItem(DEFAULT_LANGUAGE)))
            .andExpect(jsonPath("$.[*].teachingHour").value(hasItem(DEFAULT_TEACHING_HOUR)))
            .andExpect(jsonPath("$.[*].startedAt").value(hasItem(DEFAULT_STARTED_AT.toString())))
            .andExpect(jsonPath("$.[*].finishedAt").value(hasItem(DEFAULT_FINISHED_AT.toString())));
    }

    @Test
    @Transactional
    void getCourse() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get the course
        restCourseMockMvc
            .perform(get(ENTITY_API_URL_ID, course.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(course.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.centre").value(DEFAULT_CENTRE.toString()))
            .andExpect(jsonPath("$.language").value(DEFAULT_LANGUAGE))
            .andExpect(jsonPath("$.teachingHour").value(DEFAULT_TEACHING_HOUR))
            .andExpect(jsonPath("$.startedAt").value(DEFAULT_STARTED_AT.toString()))
            .andExpect(jsonPath("$.finishedAt").value(DEFAULT_FINISHED_AT.toString()));
    }

    @Test
    @Transactional
    void getCoursesByIdFiltering() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        Long id = course.getId();

        defaultCourseShouldBeFound("id.equals=" + id);
        defaultCourseShouldNotBeFound("id.notEquals=" + id);

        defaultCourseShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCourseShouldNotBeFound("id.greaterThan=" + id);

        defaultCourseShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCourseShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllCoursesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where name equals to DEFAULT_NAME
        defaultCourseShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the courseList where name equals to UPDATED_NAME
        defaultCourseShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCoursesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where name in DEFAULT_NAME or UPDATED_NAME
        defaultCourseShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the courseList where name equals to UPDATED_NAME
        defaultCourseShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCoursesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where name is not null
        defaultCourseShouldBeFound("name.specified=true");

        // Get all the courseList where name is null
        defaultCourseShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllCoursesByNameContainsSomething() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where name contains DEFAULT_NAME
        defaultCourseShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the courseList where name contains UPDATED_NAME
        defaultCourseShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCoursesByNameNotContainsSomething() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where name does not contain DEFAULT_NAME
        defaultCourseShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the courseList where name does not contain UPDATED_NAME
        defaultCourseShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCoursesByCentreIsEqualToSomething() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where centre equals to DEFAULT_CENTRE
        defaultCourseShouldBeFound("centre.equals=" + DEFAULT_CENTRE);

        // Get all the courseList where centre equals to UPDATED_CENTRE
        defaultCourseShouldNotBeFound("centre.equals=" + UPDATED_CENTRE);
    }

    @Test
    @Transactional
    void getAllCoursesByCentreIsInShouldWork() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where centre in DEFAULT_CENTRE or UPDATED_CENTRE
        defaultCourseShouldBeFound("centre.in=" + DEFAULT_CENTRE + "," + UPDATED_CENTRE);

        // Get all the courseList where centre equals to UPDATED_CENTRE
        defaultCourseShouldNotBeFound("centre.in=" + UPDATED_CENTRE);
    }

    @Test
    @Transactional
    void getAllCoursesByCentreIsNullOrNotNull() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where centre is not null
        defaultCourseShouldBeFound("centre.specified=true");

        // Get all the courseList where centre is null
        defaultCourseShouldNotBeFound("centre.specified=false");
    }

    @Test
    @Transactional
    void getAllCoursesByLanguageIsEqualToSomething() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where language equals to DEFAULT_LANGUAGE
        defaultCourseShouldBeFound("language.equals=" + DEFAULT_LANGUAGE);

        // Get all the courseList where language equals to UPDATED_LANGUAGE
        defaultCourseShouldNotBeFound("language.equals=" + UPDATED_LANGUAGE);
    }

    @Test
    @Transactional
    void getAllCoursesByLanguageIsInShouldWork() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where language in DEFAULT_LANGUAGE or UPDATED_LANGUAGE
        defaultCourseShouldBeFound("language.in=" + DEFAULT_LANGUAGE + "," + UPDATED_LANGUAGE);

        // Get all the courseList where language equals to UPDATED_LANGUAGE
        defaultCourseShouldNotBeFound("language.in=" + UPDATED_LANGUAGE);
    }

    @Test
    @Transactional
    void getAllCoursesByLanguageIsNullOrNotNull() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where language is not null
        defaultCourseShouldBeFound("language.specified=true");

        // Get all the courseList where language is null
        defaultCourseShouldNotBeFound("language.specified=false");
    }

    @Test
    @Transactional
    void getAllCoursesByLanguageContainsSomething() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where language contains DEFAULT_LANGUAGE
        defaultCourseShouldBeFound("language.contains=" + DEFAULT_LANGUAGE);

        // Get all the courseList where language contains UPDATED_LANGUAGE
        defaultCourseShouldNotBeFound("language.contains=" + UPDATED_LANGUAGE);
    }

    @Test
    @Transactional
    void getAllCoursesByLanguageNotContainsSomething() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where language does not contain DEFAULT_LANGUAGE
        defaultCourseShouldNotBeFound("language.doesNotContain=" + DEFAULT_LANGUAGE);

        // Get all the courseList where language does not contain UPDATED_LANGUAGE
        defaultCourseShouldBeFound("language.doesNotContain=" + UPDATED_LANGUAGE);
    }

    @Test
    @Transactional
    void getAllCoursesByTeachingHourIsEqualToSomething() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where teachingHour equals to DEFAULT_TEACHING_HOUR
        defaultCourseShouldBeFound("teachingHour.equals=" + DEFAULT_TEACHING_HOUR);

        // Get all the courseList where teachingHour equals to UPDATED_TEACHING_HOUR
        defaultCourseShouldNotBeFound("teachingHour.equals=" + UPDATED_TEACHING_HOUR);
    }

    @Test
    @Transactional
    void getAllCoursesByTeachingHourIsInShouldWork() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where teachingHour in DEFAULT_TEACHING_HOUR or UPDATED_TEACHING_HOUR
        defaultCourseShouldBeFound("teachingHour.in=" + DEFAULT_TEACHING_HOUR + "," + UPDATED_TEACHING_HOUR);

        // Get all the courseList where teachingHour equals to UPDATED_TEACHING_HOUR
        defaultCourseShouldNotBeFound("teachingHour.in=" + UPDATED_TEACHING_HOUR);
    }

    @Test
    @Transactional
    void getAllCoursesByTeachingHourIsNullOrNotNull() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where teachingHour is not null
        defaultCourseShouldBeFound("teachingHour.specified=true");

        // Get all the courseList where teachingHour is null
        defaultCourseShouldNotBeFound("teachingHour.specified=false");
    }

    @Test
    @Transactional
    void getAllCoursesByTeachingHourIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where teachingHour is greater than or equal to DEFAULT_TEACHING_HOUR
        defaultCourseShouldBeFound("teachingHour.greaterThanOrEqual=" + DEFAULT_TEACHING_HOUR);

        // Get all the courseList where teachingHour is greater than or equal to UPDATED_TEACHING_HOUR
        defaultCourseShouldNotBeFound("teachingHour.greaterThanOrEqual=" + UPDATED_TEACHING_HOUR);
    }

    @Test
    @Transactional
    void getAllCoursesByTeachingHourIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where teachingHour is less than or equal to DEFAULT_TEACHING_HOUR
        defaultCourseShouldBeFound("teachingHour.lessThanOrEqual=" + DEFAULT_TEACHING_HOUR);

        // Get all the courseList where teachingHour is less than or equal to SMALLER_TEACHING_HOUR
        defaultCourseShouldNotBeFound("teachingHour.lessThanOrEqual=" + SMALLER_TEACHING_HOUR);
    }

    @Test
    @Transactional
    void getAllCoursesByTeachingHourIsLessThanSomething() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where teachingHour is less than DEFAULT_TEACHING_HOUR
        defaultCourseShouldNotBeFound("teachingHour.lessThan=" + DEFAULT_TEACHING_HOUR);

        // Get all the courseList where teachingHour is less than UPDATED_TEACHING_HOUR
        defaultCourseShouldBeFound("teachingHour.lessThan=" + UPDATED_TEACHING_HOUR);
    }

    @Test
    @Transactional
    void getAllCoursesByTeachingHourIsGreaterThanSomething() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where teachingHour is greater than DEFAULT_TEACHING_HOUR
        defaultCourseShouldNotBeFound("teachingHour.greaterThan=" + DEFAULT_TEACHING_HOUR);

        // Get all the courseList where teachingHour is greater than SMALLER_TEACHING_HOUR
        defaultCourseShouldBeFound("teachingHour.greaterThan=" + SMALLER_TEACHING_HOUR);
    }

    @Test
    @Transactional
    void getAllCoursesByStartedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where startedAt equals to DEFAULT_STARTED_AT
        defaultCourseShouldBeFound("startedAt.equals=" + DEFAULT_STARTED_AT);

        // Get all the courseList where startedAt equals to UPDATED_STARTED_AT
        defaultCourseShouldNotBeFound("startedAt.equals=" + UPDATED_STARTED_AT);
    }

    @Test
    @Transactional
    void getAllCoursesByStartedAtIsInShouldWork() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where startedAt in DEFAULT_STARTED_AT or UPDATED_STARTED_AT
        defaultCourseShouldBeFound("startedAt.in=" + DEFAULT_STARTED_AT + "," + UPDATED_STARTED_AT);

        // Get all the courseList where startedAt equals to UPDATED_STARTED_AT
        defaultCourseShouldNotBeFound("startedAt.in=" + UPDATED_STARTED_AT);
    }

    @Test
    @Transactional
    void getAllCoursesByStartedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where startedAt is not null
        defaultCourseShouldBeFound("startedAt.specified=true");

        // Get all the courseList where startedAt is null
        defaultCourseShouldNotBeFound("startedAt.specified=false");
    }

    @Test
    @Transactional
    void getAllCoursesByFinishedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where finishedAt equals to DEFAULT_FINISHED_AT
        defaultCourseShouldBeFound("finishedAt.equals=" + DEFAULT_FINISHED_AT);

        // Get all the courseList where finishedAt equals to UPDATED_FINISHED_AT
        defaultCourseShouldNotBeFound("finishedAt.equals=" + UPDATED_FINISHED_AT);
    }

    @Test
    @Transactional
    void getAllCoursesByFinishedAtIsInShouldWork() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where finishedAt in DEFAULT_FINISHED_AT or UPDATED_FINISHED_AT
        defaultCourseShouldBeFound("finishedAt.in=" + DEFAULT_FINISHED_AT + "," + UPDATED_FINISHED_AT);

        // Get all the courseList where finishedAt equals to UPDATED_FINISHED_AT
        defaultCourseShouldNotBeFound("finishedAt.in=" + UPDATED_FINISHED_AT);
    }

    @Test
    @Transactional
    void getAllCoursesByFinishedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList where finishedAt is not null
        defaultCourseShouldBeFound("finishedAt.specified=true");

        // Get all the courseList where finishedAt is null
        defaultCourseShouldNotBeFound("finishedAt.specified=false");
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCourseShouldBeFound(String filter) throws Exception {
        restCourseMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(course.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].centre").value(hasItem(DEFAULT_CENTRE.toString())))
            .andExpect(jsonPath("$.[*].language").value(hasItem(DEFAULT_LANGUAGE)))
            .andExpect(jsonPath("$.[*].teachingHour").value(hasItem(DEFAULT_TEACHING_HOUR)))
            .andExpect(jsonPath("$.[*].startedAt").value(hasItem(DEFAULT_STARTED_AT.toString())))
            .andExpect(jsonPath("$.[*].finishedAt").value(hasItem(DEFAULT_FINISHED_AT.toString())));

        // Check, that the count call also returns 1
        restCourseMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCourseShouldNotBeFound(String filter) throws Exception {
        restCourseMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCourseMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingCourse() throws Exception {
        // Get the course
        restCourseMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingCourse() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        int databaseSizeBeforeUpdate = courseRepository.findAll().size();

        // Update the course
        Course updatedCourse = courseRepository.findById(course.getId()).get();
        // Disconnect from session so that the updates on updatedCourse are not directly saved in db
        em.detach(updatedCourse);
        updatedCourse
            .name(UPDATED_NAME)
            .centre(UPDATED_CENTRE)
            .language(UPDATED_LANGUAGE)
            .teachingHour(UPDATED_TEACHING_HOUR)
            .startedAt(UPDATED_STARTED_AT)
            .finishedAt(UPDATED_FINISHED_AT);
        CourseDTO courseDTO = courseMapper.toDto(updatedCourse);

        restCourseMockMvc
            .perform(
                put(ENTITY_API_URL_ID, courseDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(courseDTO))
            )
            .andExpect(status().isOk());

        // Validate the Course in the database
        List<Course> courseList = courseRepository.findAll();
        assertThat(courseList).hasSize(databaseSizeBeforeUpdate);
        Course testCourse = courseList.get(courseList.size() - 1);
        assertThat(testCourse.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCourse.getCentre()).isEqualTo(UPDATED_CENTRE);
        assertThat(testCourse.getLanguage()).isEqualTo(UPDATED_LANGUAGE);
        assertThat(testCourse.getTeachingHour()).isEqualTo(UPDATED_TEACHING_HOUR);
        assertThat(testCourse.getStartedAt()).isEqualTo(UPDATED_STARTED_AT);
        assertThat(testCourse.getFinishedAt()).isEqualTo(UPDATED_FINISHED_AT);
    }

    @Test
    @Transactional
    void putNonExistingCourse() throws Exception {
        int databaseSizeBeforeUpdate = courseRepository.findAll().size();
        course.setId(count.incrementAndGet());

        // Create the Course
        CourseDTO courseDTO = courseMapper.toDto(course);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCourseMockMvc
            .perform(
                put(ENTITY_API_URL_ID, courseDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(courseDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Course in the database
        List<Course> courseList = courseRepository.findAll();
        assertThat(courseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCourse() throws Exception {
        int databaseSizeBeforeUpdate = courseRepository.findAll().size();
        course.setId(count.incrementAndGet());

        // Create the Course
        CourseDTO courseDTO = courseMapper.toDto(course);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCourseMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(courseDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Course in the database
        List<Course> courseList = courseRepository.findAll();
        assertThat(courseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCourse() throws Exception {
        int databaseSizeBeforeUpdate = courseRepository.findAll().size();
        course.setId(count.incrementAndGet());

        // Create the Course
        CourseDTO courseDTO = courseMapper.toDto(course);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCourseMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(courseDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Course in the database
        List<Course> courseList = courseRepository.findAll();
        assertThat(courseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCourseWithPatch() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        int databaseSizeBeforeUpdate = courseRepository.findAll().size();

        // Update the course using partial update
        Course partialUpdatedCourse = new Course();
        partialUpdatedCourse.setId(course.getId());

        partialUpdatedCourse.language(UPDATED_LANGUAGE).startedAt(UPDATED_STARTED_AT).finishedAt(UPDATED_FINISHED_AT);

        restCourseMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCourse.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCourse))
            )
            .andExpect(status().isOk());

        // Validate the Course in the database
        List<Course> courseList = courseRepository.findAll();
        assertThat(courseList).hasSize(databaseSizeBeforeUpdate);
        Course testCourse = courseList.get(courseList.size() - 1);
        assertThat(testCourse.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCourse.getCentre()).isEqualTo(DEFAULT_CENTRE);
        assertThat(testCourse.getLanguage()).isEqualTo(UPDATED_LANGUAGE);
        assertThat(testCourse.getTeachingHour()).isEqualTo(DEFAULT_TEACHING_HOUR);
        assertThat(testCourse.getStartedAt()).isEqualTo(UPDATED_STARTED_AT);
        assertThat(testCourse.getFinishedAt()).isEqualTo(UPDATED_FINISHED_AT);
    }

    @Test
    @Transactional
    void fullUpdateCourseWithPatch() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        int databaseSizeBeforeUpdate = courseRepository.findAll().size();

        // Update the course using partial update
        Course partialUpdatedCourse = new Course();
        partialUpdatedCourse.setId(course.getId());

        partialUpdatedCourse
            .name(UPDATED_NAME)
            .centre(UPDATED_CENTRE)
            .language(UPDATED_LANGUAGE)
            .teachingHour(UPDATED_TEACHING_HOUR)
            .startedAt(UPDATED_STARTED_AT)
            .finishedAt(UPDATED_FINISHED_AT);

        restCourseMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCourse.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCourse))
            )
            .andExpect(status().isOk());

        // Validate the Course in the database
        List<Course> courseList = courseRepository.findAll();
        assertThat(courseList).hasSize(databaseSizeBeforeUpdate);
        Course testCourse = courseList.get(courseList.size() - 1);
        assertThat(testCourse.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCourse.getCentre()).isEqualTo(UPDATED_CENTRE);
        assertThat(testCourse.getLanguage()).isEqualTo(UPDATED_LANGUAGE);
        assertThat(testCourse.getTeachingHour()).isEqualTo(UPDATED_TEACHING_HOUR);
        assertThat(testCourse.getStartedAt()).isEqualTo(UPDATED_STARTED_AT);
        assertThat(testCourse.getFinishedAt()).isEqualTo(UPDATED_FINISHED_AT);
    }

    @Test
    @Transactional
    void patchNonExistingCourse() throws Exception {
        int databaseSizeBeforeUpdate = courseRepository.findAll().size();
        course.setId(count.incrementAndGet());

        // Create the Course
        CourseDTO courseDTO = courseMapper.toDto(course);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCourseMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, courseDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(courseDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Course in the database
        List<Course> courseList = courseRepository.findAll();
        assertThat(courseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCourse() throws Exception {
        int databaseSizeBeforeUpdate = courseRepository.findAll().size();
        course.setId(count.incrementAndGet());

        // Create the Course
        CourseDTO courseDTO = courseMapper.toDto(course);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCourseMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(courseDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Course in the database
        List<Course> courseList = courseRepository.findAll();
        assertThat(courseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCourse() throws Exception {
        int databaseSizeBeforeUpdate = courseRepository.findAll().size();
        course.setId(count.incrementAndGet());

        // Create the Course
        CourseDTO courseDTO = courseMapper.toDto(course);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCourseMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(courseDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Course in the database
        List<Course> courseList = courseRepository.findAll();
        assertThat(courseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCourse() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        int databaseSizeBeforeDelete = courseRepository.findAll().size();

        // Delete the course
        restCourseMockMvc
            .perform(delete(ENTITY_API_URL_ID, course.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Course> courseList = courseRepository.findAll();
        assertThat(courseList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
