package uz.sudex.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import uz.sudex.IntegrationTest;
import uz.sudex.domain.Assessment;
import uz.sudex.domain.Links;
import uz.sudex.repository.LinksRepository;
import uz.sudex.service.criteria.LinksCriteria;
import uz.sudex.service.dto.LinksDTO;
import uz.sudex.service.mapper.LinksMapper;

/**
 * Integration tests for the {@link LinksResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class LinksResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/links";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private LinksRepository linksRepository;

    @Autowired
    private LinksMapper linksMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLinksMockMvc;

    private Links links;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Links createEntity(EntityManager em) {
        Links links = new Links().name(DEFAULT_NAME);
        return links;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Links createUpdatedEntity(EntityManager em) {
        Links links = new Links().name(UPDATED_NAME);
        return links;
    }

    @BeforeEach
    public void initTest() {
        links = createEntity(em);
    }

    @Test
    @Transactional
    void createLinks() throws Exception {
        int databaseSizeBeforeCreate = linksRepository.findAll().size();
        // Create the Links
        LinksDTO linksDTO = linksMapper.toDto(links);
        restLinksMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(linksDTO)))
            .andExpect(status().isCreated());

        // Validate the Links in the database
        List<Links> linksList = linksRepository.findAll();
        assertThat(linksList).hasSize(databaseSizeBeforeCreate + 1);
        Links testLinks = linksList.get(linksList.size() - 1);
        assertThat(testLinks.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    void createLinksWithExistingId() throws Exception {
        // Create the Links with an existing ID
        links.setId(1L);
        LinksDTO linksDTO = linksMapper.toDto(links);

        int databaseSizeBeforeCreate = linksRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restLinksMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(linksDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Links in the database
        List<Links> linksList = linksRepository.findAll();
        assertThat(linksList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllLinks() throws Exception {
        // Initialize the database
        linksRepository.saveAndFlush(links);

        // Get all the linksList
        restLinksMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(links.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }

    @Test
    @Transactional
    void getLinks() throws Exception {
        // Initialize the database
        linksRepository.saveAndFlush(links);

        // Get the links
        restLinksMockMvc
            .perform(get(ENTITY_API_URL_ID, links.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(links.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    void getLinksByIdFiltering() throws Exception {
        // Initialize the database
        linksRepository.saveAndFlush(links);

        Long id = links.getId();

        defaultLinksShouldBeFound("id.equals=" + id);
        defaultLinksShouldNotBeFound("id.notEquals=" + id);

        defaultLinksShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultLinksShouldNotBeFound("id.greaterThan=" + id);

        defaultLinksShouldBeFound("id.lessThanOrEqual=" + id);
        defaultLinksShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllLinksByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        linksRepository.saveAndFlush(links);

        // Get all the linksList where name equals to DEFAULT_NAME
        defaultLinksShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the linksList where name equals to UPDATED_NAME
        defaultLinksShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllLinksByNameIsInShouldWork() throws Exception {
        // Initialize the database
        linksRepository.saveAndFlush(links);

        // Get all the linksList where name in DEFAULT_NAME or UPDATED_NAME
        defaultLinksShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the linksList where name equals to UPDATED_NAME
        defaultLinksShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllLinksByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        linksRepository.saveAndFlush(links);

        // Get all the linksList where name is not null
        defaultLinksShouldBeFound("name.specified=true");

        // Get all the linksList where name is null
        defaultLinksShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllLinksByNameContainsSomething() throws Exception {
        // Initialize the database
        linksRepository.saveAndFlush(links);

        // Get all the linksList where name contains DEFAULT_NAME
        defaultLinksShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the linksList where name contains UPDATED_NAME
        defaultLinksShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllLinksByNameNotContainsSomething() throws Exception {
        // Initialize the database
        linksRepository.saveAndFlush(links);

        // Get all the linksList where name does not contain DEFAULT_NAME
        defaultLinksShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the linksList where name does not contain UPDATED_NAME
        defaultLinksShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllLinksByAssessmentIsEqualToSomething() throws Exception {
        Assessment assessment;
        if (TestUtil.findAll(em, Assessment.class).isEmpty()) {
            linksRepository.saveAndFlush(links);
            assessment = AssessmentResourceIT.createEntity(em);
        } else {
            assessment = TestUtil.findAll(em, Assessment.class).get(0);
        }
        em.persist(assessment);
        em.flush();
        links.setAssessment(assessment);
        linksRepository.saveAndFlush(links);
        Long assessmentId = assessment.getId();

        // Get all the linksList where assessment equals to assessmentId
        defaultLinksShouldBeFound("assessmentId.equals=" + assessmentId);

        // Get all the linksList where assessment equals to (assessmentId + 1)
        defaultLinksShouldNotBeFound("assessmentId.equals=" + (assessmentId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultLinksShouldBeFound(String filter) throws Exception {
        restLinksMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(links.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));

        // Check, that the count call also returns 1
        restLinksMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultLinksShouldNotBeFound(String filter) throws Exception {
        restLinksMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restLinksMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingLinks() throws Exception {
        // Get the links
        restLinksMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingLinks() throws Exception {
        // Initialize the database
        linksRepository.saveAndFlush(links);

        int databaseSizeBeforeUpdate = linksRepository.findAll().size();

        // Update the links
        Links updatedLinks = linksRepository.findById(links.getId()).get();
        // Disconnect from session so that the updates on updatedLinks are not directly saved in db
        em.detach(updatedLinks);
        updatedLinks.name(UPDATED_NAME);
        LinksDTO linksDTO = linksMapper.toDto(updatedLinks);

        restLinksMockMvc
            .perform(
                put(ENTITY_API_URL_ID, linksDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(linksDTO))
            )
            .andExpect(status().isOk());

        // Validate the Links in the database
        List<Links> linksList = linksRepository.findAll();
        assertThat(linksList).hasSize(databaseSizeBeforeUpdate);
        Links testLinks = linksList.get(linksList.size() - 1);
        assertThat(testLinks.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void putNonExistingLinks() throws Exception {
        int databaseSizeBeforeUpdate = linksRepository.findAll().size();
        links.setId(count.incrementAndGet());

        // Create the Links
        LinksDTO linksDTO = linksMapper.toDto(links);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLinksMockMvc
            .perform(
                put(ENTITY_API_URL_ID, linksDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(linksDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Links in the database
        List<Links> linksList = linksRepository.findAll();
        assertThat(linksList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchLinks() throws Exception {
        int databaseSizeBeforeUpdate = linksRepository.findAll().size();
        links.setId(count.incrementAndGet());

        // Create the Links
        LinksDTO linksDTO = linksMapper.toDto(links);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLinksMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(linksDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Links in the database
        List<Links> linksList = linksRepository.findAll();
        assertThat(linksList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamLinks() throws Exception {
        int databaseSizeBeforeUpdate = linksRepository.findAll().size();
        links.setId(count.incrementAndGet());

        // Create the Links
        LinksDTO linksDTO = linksMapper.toDto(links);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLinksMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(linksDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Links in the database
        List<Links> linksList = linksRepository.findAll();
        assertThat(linksList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateLinksWithPatch() throws Exception {
        // Initialize the database
        linksRepository.saveAndFlush(links);

        int databaseSizeBeforeUpdate = linksRepository.findAll().size();

        // Update the links using partial update
        Links partialUpdatedLinks = new Links();
        partialUpdatedLinks.setId(links.getId());

        partialUpdatedLinks.name(UPDATED_NAME);

        restLinksMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLinks.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLinks))
            )
            .andExpect(status().isOk());

        // Validate the Links in the database
        List<Links> linksList = linksRepository.findAll();
        assertThat(linksList).hasSize(databaseSizeBeforeUpdate);
        Links testLinks = linksList.get(linksList.size() - 1);
        assertThat(testLinks.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void fullUpdateLinksWithPatch() throws Exception {
        // Initialize the database
        linksRepository.saveAndFlush(links);

        int databaseSizeBeforeUpdate = linksRepository.findAll().size();

        // Update the links using partial update
        Links partialUpdatedLinks = new Links();
        partialUpdatedLinks.setId(links.getId());

        partialUpdatedLinks.name(UPDATED_NAME);

        restLinksMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLinks.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLinks))
            )
            .andExpect(status().isOk());

        // Validate the Links in the database
        List<Links> linksList = linksRepository.findAll();
        assertThat(linksList).hasSize(databaseSizeBeforeUpdate);
        Links testLinks = linksList.get(linksList.size() - 1);
        assertThat(testLinks.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void patchNonExistingLinks() throws Exception {
        int databaseSizeBeforeUpdate = linksRepository.findAll().size();
        links.setId(count.incrementAndGet());

        // Create the Links
        LinksDTO linksDTO = linksMapper.toDto(links);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLinksMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, linksDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(linksDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Links in the database
        List<Links> linksList = linksRepository.findAll();
        assertThat(linksList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchLinks() throws Exception {
        int databaseSizeBeforeUpdate = linksRepository.findAll().size();
        links.setId(count.incrementAndGet());

        // Create the Links
        LinksDTO linksDTO = linksMapper.toDto(links);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLinksMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(linksDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Links in the database
        List<Links> linksList = linksRepository.findAll();
        assertThat(linksList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamLinks() throws Exception {
        int databaseSizeBeforeUpdate = linksRepository.findAll().size();
        links.setId(count.incrementAndGet());

        // Create the Links
        LinksDTO linksDTO = linksMapper.toDto(links);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLinksMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(linksDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Links in the database
        List<Links> linksList = linksRepository.findAll();
        assertThat(linksList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteLinks() throws Exception {
        // Initialize the database
        linksRepository.saveAndFlush(links);

        int databaseSizeBeforeDelete = linksRepository.findAll().size();

        // Delete the links
        restLinksMockMvc
            .perform(delete(ENTITY_API_URL_ID, links.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Links> linksList = linksRepository.findAll();
        assertThat(linksList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
