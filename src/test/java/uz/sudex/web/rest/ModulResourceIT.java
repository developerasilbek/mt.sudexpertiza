package uz.sudex.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import uz.sudex.IntegrationTest;
import uz.sudex.domain.Block;
import uz.sudex.domain.Modul;
import uz.sudex.repository.ModulRepository;
import uz.sudex.service.criteria.ModulCriteria;
import uz.sudex.service.dto.ModulDTO;
import uz.sudex.service.mapper.ModulMapper;

/**
 * Integration tests for the {@link ModulResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ModulResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DECRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DECRIPTION = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/moduls";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ModulRepository modulRepository;

    @Autowired
    private ModulMapper modulMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restModulMockMvc;

    private Modul modul;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Modul createEntity(EntityManager em) {
        Modul modul = new Modul().name(DEFAULT_NAME).decription(DEFAULT_DECRIPTION);
        return modul;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Modul createUpdatedEntity(EntityManager em) {
        Modul modul = new Modul().name(UPDATED_NAME).decription(UPDATED_DECRIPTION);
        return modul;
    }

    @BeforeEach
    public void initTest() {
        modul = createEntity(em);
    }

    @Test
    @Transactional
    void createModul() throws Exception {
        int databaseSizeBeforeCreate = modulRepository.findAll().size();
        // Create the Modul
        ModulDTO modulDTO = modulMapper.toDto(modul);
        restModulMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(modulDTO)))
            .andExpect(status().isCreated());

        // Validate the Modul in the database
        List<Modul> modulList = modulRepository.findAll();
        assertThat(modulList).hasSize(databaseSizeBeforeCreate + 1);
        Modul testModul = modulList.get(modulList.size() - 1);
        assertThat(testModul.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testModul.getDecription()).isEqualTo(DEFAULT_DECRIPTION);
    }

    @Test
    @Transactional
    void createModulWithExistingId() throws Exception {
        // Create the Modul with an existing ID
        modul.setId(1L);
        ModulDTO modulDTO = modulMapper.toDto(modul);

        int databaseSizeBeforeCreate = modulRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restModulMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(modulDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Modul in the database
        List<Modul> modulList = modulRepository.findAll();
        assertThat(modulList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllModuls() throws Exception {
        // Initialize the database
        modulRepository.saveAndFlush(modul);

        // Get all the modulList
        restModulMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(modul.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].decription").value(hasItem(DEFAULT_DECRIPTION)));
    }

    @Test
    @Transactional
    void getModul() throws Exception {
        // Initialize the database
        modulRepository.saveAndFlush(modul);

        // Get the modul
        restModulMockMvc
            .perform(get(ENTITY_API_URL_ID, modul.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(modul.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.decription").value(DEFAULT_DECRIPTION));
    }

    @Test
    @Transactional
    void getModulsByIdFiltering() throws Exception {
        // Initialize the database
        modulRepository.saveAndFlush(modul);

        Long id = modul.getId();

        defaultModulShouldBeFound("id.equals=" + id);
        defaultModulShouldNotBeFound("id.notEquals=" + id);

        defaultModulShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultModulShouldNotBeFound("id.greaterThan=" + id);

        defaultModulShouldBeFound("id.lessThanOrEqual=" + id);
        defaultModulShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllModulsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        modulRepository.saveAndFlush(modul);

        // Get all the modulList where name equals to DEFAULT_NAME
        defaultModulShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the modulList where name equals to UPDATED_NAME
        defaultModulShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllModulsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        modulRepository.saveAndFlush(modul);

        // Get all the modulList where name in DEFAULT_NAME or UPDATED_NAME
        defaultModulShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the modulList where name equals to UPDATED_NAME
        defaultModulShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllModulsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        modulRepository.saveAndFlush(modul);

        // Get all the modulList where name is not null
        defaultModulShouldBeFound("name.specified=true");

        // Get all the modulList where name is null
        defaultModulShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllModulsByNameContainsSomething() throws Exception {
        // Initialize the database
        modulRepository.saveAndFlush(modul);

        // Get all the modulList where name contains DEFAULT_NAME
        defaultModulShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the modulList where name contains UPDATED_NAME
        defaultModulShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllModulsByNameNotContainsSomething() throws Exception {
        // Initialize the database
        modulRepository.saveAndFlush(modul);

        // Get all the modulList where name does not contain DEFAULT_NAME
        defaultModulShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the modulList where name does not contain UPDATED_NAME
        defaultModulShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllModulsByDecriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        modulRepository.saveAndFlush(modul);

        // Get all the modulList where decription equals to DEFAULT_DECRIPTION
        defaultModulShouldBeFound("decription.equals=" + DEFAULT_DECRIPTION);

        // Get all the modulList where decription equals to UPDATED_DECRIPTION
        defaultModulShouldNotBeFound("decription.equals=" + UPDATED_DECRIPTION);
    }

    @Test
    @Transactional
    void getAllModulsByDecriptionIsInShouldWork() throws Exception {
        // Initialize the database
        modulRepository.saveAndFlush(modul);

        // Get all the modulList where decription in DEFAULT_DECRIPTION or UPDATED_DECRIPTION
        defaultModulShouldBeFound("decription.in=" + DEFAULT_DECRIPTION + "," + UPDATED_DECRIPTION);

        // Get all the modulList where decription equals to UPDATED_DECRIPTION
        defaultModulShouldNotBeFound("decription.in=" + UPDATED_DECRIPTION);
    }

    @Test
    @Transactional
    void getAllModulsByDecriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        modulRepository.saveAndFlush(modul);

        // Get all the modulList where decription is not null
        defaultModulShouldBeFound("decription.specified=true");

        // Get all the modulList where decription is null
        defaultModulShouldNotBeFound("decription.specified=false");
    }

    @Test
    @Transactional
    void getAllModulsByDecriptionContainsSomething() throws Exception {
        // Initialize the database
        modulRepository.saveAndFlush(modul);

        // Get all the modulList where decription contains DEFAULT_DECRIPTION
        defaultModulShouldBeFound("decription.contains=" + DEFAULT_DECRIPTION);

        // Get all the modulList where decription contains UPDATED_DECRIPTION
        defaultModulShouldNotBeFound("decription.contains=" + UPDATED_DECRIPTION);
    }

    @Test
    @Transactional
    void getAllModulsByDecriptionNotContainsSomething() throws Exception {
        // Initialize the database
        modulRepository.saveAndFlush(modul);

        // Get all the modulList where decription does not contain DEFAULT_DECRIPTION
        defaultModulShouldNotBeFound("decription.doesNotContain=" + DEFAULT_DECRIPTION);

        // Get all the modulList where decription does not contain UPDATED_DECRIPTION
        defaultModulShouldBeFound("decription.doesNotContain=" + UPDATED_DECRIPTION);
    }

    @Test
    @Transactional
    void getAllModulsByBlockIsEqualToSomething() throws Exception {
        Block block;
        if (TestUtil.findAll(em, Block.class).isEmpty()) {
            modulRepository.saveAndFlush(modul);
            block = BlockResourceIT.createEntity(em);
        } else {
            block = TestUtil.findAll(em, Block.class).get(0);
        }
        em.persist(block);
        em.flush();
        modul.setBlock(block);
        modulRepository.saveAndFlush(modul);
        Long blockId = block.getId();

        // Get all the modulList where block equals to blockId
        defaultModulShouldBeFound("blockId.equals=" + blockId);

        // Get all the modulList where block equals to (blockId + 1)
        defaultModulShouldNotBeFound("blockId.equals=" + (blockId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultModulShouldBeFound(String filter) throws Exception {
        restModulMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(modul.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].decription").value(hasItem(DEFAULT_DECRIPTION)));

        // Check, that the count call also returns 1
        restModulMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultModulShouldNotBeFound(String filter) throws Exception {
        restModulMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restModulMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingModul() throws Exception {
        // Get the modul
        restModulMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingModul() throws Exception {
        // Initialize the database
        modulRepository.saveAndFlush(modul);

        int databaseSizeBeforeUpdate = modulRepository.findAll().size();

        // Update the modul
        Modul updatedModul = modulRepository.findById(modul.getId()).get();
        // Disconnect from session so that the updates on updatedModul are not directly saved in db
        em.detach(updatedModul);
        updatedModul.name(UPDATED_NAME).decription(UPDATED_DECRIPTION);
        ModulDTO modulDTO = modulMapper.toDto(updatedModul);

        restModulMockMvc
            .perform(
                put(ENTITY_API_URL_ID, modulDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(modulDTO))
            )
            .andExpect(status().isOk());

        // Validate the Modul in the database
        List<Modul> modulList = modulRepository.findAll();
        assertThat(modulList).hasSize(databaseSizeBeforeUpdate);
        Modul testModul = modulList.get(modulList.size() - 1);
        assertThat(testModul.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testModul.getDecription()).isEqualTo(UPDATED_DECRIPTION);
    }

    @Test
    @Transactional
    void putNonExistingModul() throws Exception {
        int databaseSizeBeforeUpdate = modulRepository.findAll().size();
        modul.setId(count.incrementAndGet());

        // Create the Modul
        ModulDTO modulDTO = modulMapper.toDto(modul);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restModulMockMvc
            .perform(
                put(ENTITY_API_URL_ID, modulDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(modulDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Modul in the database
        List<Modul> modulList = modulRepository.findAll();
        assertThat(modulList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchModul() throws Exception {
        int databaseSizeBeforeUpdate = modulRepository.findAll().size();
        modul.setId(count.incrementAndGet());

        // Create the Modul
        ModulDTO modulDTO = modulMapper.toDto(modul);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restModulMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(modulDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Modul in the database
        List<Modul> modulList = modulRepository.findAll();
        assertThat(modulList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamModul() throws Exception {
        int databaseSizeBeforeUpdate = modulRepository.findAll().size();
        modul.setId(count.incrementAndGet());

        // Create the Modul
        ModulDTO modulDTO = modulMapper.toDto(modul);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restModulMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(modulDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Modul in the database
        List<Modul> modulList = modulRepository.findAll();
        assertThat(modulList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateModulWithPatch() throws Exception {
        // Initialize the database
        modulRepository.saveAndFlush(modul);

        int databaseSizeBeforeUpdate = modulRepository.findAll().size();

        // Update the modul using partial update
        Modul partialUpdatedModul = new Modul();
        partialUpdatedModul.setId(modul.getId());

        partialUpdatedModul.name(UPDATED_NAME).decription(UPDATED_DECRIPTION);

        restModulMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedModul.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedModul))
            )
            .andExpect(status().isOk());

        // Validate the Modul in the database
        List<Modul> modulList = modulRepository.findAll();
        assertThat(modulList).hasSize(databaseSizeBeforeUpdate);
        Modul testModul = modulList.get(modulList.size() - 1);
        assertThat(testModul.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testModul.getDecription()).isEqualTo(UPDATED_DECRIPTION);
    }

    @Test
    @Transactional
    void fullUpdateModulWithPatch() throws Exception {
        // Initialize the database
        modulRepository.saveAndFlush(modul);

        int databaseSizeBeforeUpdate = modulRepository.findAll().size();

        // Update the modul using partial update
        Modul partialUpdatedModul = new Modul();
        partialUpdatedModul.setId(modul.getId());

        partialUpdatedModul.name(UPDATED_NAME).decription(UPDATED_DECRIPTION);

        restModulMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedModul.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedModul))
            )
            .andExpect(status().isOk());

        // Validate the Modul in the database
        List<Modul> modulList = modulRepository.findAll();
        assertThat(modulList).hasSize(databaseSizeBeforeUpdate);
        Modul testModul = modulList.get(modulList.size() - 1);
        assertThat(testModul.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testModul.getDecription()).isEqualTo(UPDATED_DECRIPTION);
    }

    @Test
    @Transactional
    void patchNonExistingModul() throws Exception {
        int databaseSizeBeforeUpdate = modulRepository.findAll().size();
        modul.setId(count.incrementAndGet());

        // Create the Modul
        ModulDTO modulDTO = modulMapper.toDto(modul);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restModulMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, modulDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(modulDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Modul in the database
        List<Modul> modulList = modulRepository.findAll();
        assertThat(modulList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchModul() throws Exception {
        int databaseSizeBeforeUpdate = modulRepository.findAll().size();
        modul.setId(count.incrementAndGet());

        // Create the Modul
        ModulDTO modulDTO = modulMapper.toDto(modul);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restModulMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(modulDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Modul in the database
        List<Modul> modulList = modulRepository.findAll();
        assertThat(modulList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamModul() throws Exception {
        int databaseSizeBeforeUpdate = modulRepository.findAll().size();
        modul.setId(count.incrementAndGet());

        // Create the Modul
        ModulDTO modulDTO = modulMapper.toDto(modul);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restModulMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(modulDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Modul in the database
        List<Modul> modulList = modulRepository.findAll();
        assertThat(modulList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteModul() throws Exception {
        // Initialize the database
        modulRepository.saveAndFlush(modul);

        int databaseSizeBeforeDelete = modulRepository.findAll().size();

        // Delete the modul
        restModulMockMvc
            .perform(delete(ENTITY_API_URL_ID, modul.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Modul> modulList = modulRepository.findAll();
        assertThat(modulList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
