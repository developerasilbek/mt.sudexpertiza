package uz.sudex.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import uz.sudex.IntegrationTest;
import uz.sudex.domain.Assessment;
import uz.sudex.domain.TopicTest;
import uz.sudex.repository.TopicTestRepository;
import uz.sudex.service.criteria.TopicTestCriteria;
import uz.sudex.service.dto.TopicTestDTO;
import uz.sudex.service.mapper.TopicTestMapper;

/**
 * Integration tests for the {@link TopicTestResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class TopicTestResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/topic-tests";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private TopicTestRepository topicTestRepository;

    @Autowired
    private TopicTestMapper topicTestMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTopicTestMockMvc;

    private TopicTest topicTest;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TopicTest createEntity(EntityManager em) {
        TopicTest topicTest = new TopicTest().name(DEFAULT_NAME);
        return topicTest;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TopicTest createUpdatedEntity(EntityManager em) {
        TopicTest topicTest = new TopicTest().name(UPDATED_NAME);
        return topicTest;
    }

    @BeforeEach
    public void initTest() {
        topicTest = createEntity(em);
    }

    @Test
    @Transactional
    void createTopicTest() throws Exception {
        int databaseSizeBeforeCreate = topicTestRepository.findAll().size();
        // Create the TopicTest
        TopicTestDTO topicTestDTO = topicTestMapper.toDto(topicTest);
        restTopicTestMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(topicTestDTO)))
            .andExpect(status().isCreated());

        // Validate the TopicTest in the database
        List<TopicTest> topicTestList = topicTestRepository.findAll();
        assertThat(topicTestList).hasSize(databaseSizeBeforeCreate + 1);
        TopicTest testTopicTest = topicTestList.get(topicTestList.size() - 1);
        assertThat(testTopicTest.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    void createTopicTestWithExistingId() throws Exception {
        // Create the TopicTest with an existing ID
        topicTest.setId(1L);
        TopicTestDTO topicTestDTO = topicTestMapper.toDto(topicTest);

        int databaseSizeBeforeCreate = topicTestRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restTopicTestMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(topicTestDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TopicTest in the database
        List<TopicTest> topicTestList = topicTestRepository.findAll();
        assertThat(topicTestList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllTopicTests() throws Exception {
        // Initialize the database
        topicTestRepository.saveAndFlush(topicTest);

        // Get all the topicTestList
        restTopicTestMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(topicTest.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }

    @Test
    @Transactional
    void getTopicTest() throws Exception {
        // Initialize the database
        topicTestRepository.saveAndFlush(topicTest);

        // Get the topicTest
        restTopicTestMockMvc
            .perform(get(ENTITY_API_URL_ID, topicTest.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(topicTest.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    void getTopicTestsByIdFiltering() throws Exception {
        // Initialize the database
        topicTestRepository.saveAndFlush(topicTest);

        Long id = topicTest.getId();

        defaultTopicTestShouldBeFound("id.equals=" + id);
        defaultTopicTestShouldNotBeFound("id.notEquals=" + id);

        defaultTopicTestShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTopicTestShouldNotBeFound("id.greaterThan=" + id);

        defaultTopicTestShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTopicTestShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllTopicTestsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        topicTestRepository.saveAndFlush(topicTest);

        // Get all the topicTestList where name equals to DEFAULT_NAME
        defaultTopicTestShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the topicTestList where name equals to UPDATED_NAME
        defaultTopicTestShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllTopicTestsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        topicTestRepository.saveAndFlush(topicTest);

        // Get all the topicTestList where name in DEFAULT_NAME or UPDATED_NAME
        defaultTopicTestShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the topicTestList where name equals to UPDATED_NAME
        defaultTopicTestShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllTopicTestsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        topicTestRepository.saveAndFlush(topicTest);

        // Get all the topicTestList where name is not null
        defaultTopicTestShouldBeFound("name.specified=true");

        // Get all the topicTestList where name is null
        defaultTopicTestShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllTopicTestsByNameContainsSomething() throws Exception {
        // Initialize the database
        topicTestRepository.saveAndFlush(topicTest);

        // Get all the topicTestList where name contains DEFAULT_NAME
        defaultTopicTestShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the topicTestList where name contains UPDATED_NAME
        defaultTopicTestShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllTopicTestsByNameNotContainsSomething() throws Exception {
        // Initialize the database
        topicTestRepository.saveAndFlush(topicTest);

        // Get all the topicTestList where name does not contain DEFAULT_NAME
        defaultTopicTestShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the topicTestList where name does not contain UPDATED_NAME
        defaultTopicTestShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllTopicTestsByAssessmentIsEqualToSomething() throws Exception {
        Assessment assessment;
        if (TestUtil.findAll(em, Assessment.class).isEmpty()) {
            topicTestRepository.saveAndFlush(topicTest);
            assessment = AssessmentResourceIT.createEntity(em);
        } else {
            assessment = TestUtil.findAll(em, Assessment.class).get(0);
        }
        em.persist(assessment);
        em.flush();
        topicTest.setAssessment(assessment);
        topicTestRepository.saveAndFlush(topicTest);
        Long assessmentId = assessment.getId();

        // Get all the topicTestList where assessment equals to assessmentId
        defaultTopicTestShouldBeFound("assessmentId.equals=" + assessmentId);

        // Get all the topicTestList where assessment equals to (assessmentId + 1)
        defaultTopicTestShouldNotBeFound("assessmentId.equals=" + (assessmentId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTopicTestShouldBeFound(String filter) throws Exception {
        restTopicTestMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(topicTest.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));

        // Check, that the count call also returns 1
        restTopicTestMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTopicTestShouldNotBeFound(String filter) throws Exception {
        restTopicTestMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTopicTestMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingTopicTest() throws Exception {
        // Get the topicTest
        restTopicTestMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingTopicTest() throws Exception {
        // Initialize the database
        topicTestRepository.saveAndFlush(topicTest);

        int databaseSizeBeforeUpdate = topicTestRepository.findAll().size();

        // Update the topicTest
        TopicTest updatedTopicTest = topicTestRepository.findById(topicTest.getId()).get();
        // Disconnect from session so that the updates on updatedTopicTest are not directly saved in db
        em.detach(updatedTopicTest);
        updatedTopicTest.name(UPDATED_NAME);
        TopicTestDTO topicTestDTO = topicTestMapper.toDto(updatedTopicTest);

        restTopicTestMockMvc
            .perform(
                put(ENTITY_API_URL_ID, topicTestDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(topicTestDTO))
            )
            .andExpect(status().isOk());

        // Validate the TopicTest in the database
        List<TopicTest> topicTestList = topicTestRepository.findAll();
        assertThat(topicTestList).hasSize(databaseSizeBeforeUpdate);
        TopicTest testTopicTest = topicTestList.get(topicTestList.size() - 1);
        assertThat(testTopicTest.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void putNonExistingTopicTest() throws Exception {
        int databaseSizeBeforeUpdate = topicTestRepository.findAll().size();
        topicTest.setId(count.incrementAndGet());

        // Create the TopicTest
        TopicTestDTO topicTestDTO = topicTestMapper.toDto(topicTest);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTopicTestMockMvc
            .perform(
                put(ENTITY_API_URL_ID, topicTestDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(topicTestDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TopicTest in the database
        List<TopicTest> topicTestList = topicTestRepository.findAll();
        assertThat(topicTestList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchTopicTest() throws Exception {
        int databaseSizeBeforeUpdate = topicTestRepository.findAll().size();
        topicTest.setId(count.incrementAndGet());

        // Create the TopicTest
        TopicTestDTO topicTestDTO = topicTestMapper.toDto(topicTest);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTopicTestMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(topicTestDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TopicTest in the database
        List<TopicTest> topicTestList = topicTestRepository.findAll();
        assertThat(topicTestList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamTopicTest() throws Exception {
        int databaseSizeBeforeUpdate = topicTestRepository.findAll().size();
        topicTest.setId(count.incrementAndGet());

        // Create the TopicTest
        TopicTestDTO topicTestDTO = topicTestMapper.toDto(topicTest);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTopicTestMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(topicTestDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the TopicTest in the database
        List<TopicTest> topicTestList = topicTestRepository.findAll();
        assertThat(topicTestList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateTopicTestWithPatch() throws Exception {
        // Initialize the database
        topicTestRepository.saveAndFlush(topicTest);

        int databaseSizeBeforeUpdate = topicTestRepository.findAll().size();

        // Update the topicTest using partial update
        TopicTest partialUpdatedTopicTest = new TopicTest();
        partialUpdatedTopicTest.setId(topicTest.getId());

        restTopicTestMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTopicTest.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTopicTest))
            )
            .andExpect(status().isOk());

        // Validate the TopicTest in the database
        List<TopicTest> topicTestList = topicTestRepository.findAll();
        assertThat(topicTestList).hasSize(databaseSizeBeforeUpdate);
        TopicTest testTopicTest = topicTestList.get(topicTestList.size() - 1);
        assertThat(testTopicTest.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    void fullUpdateTopicTestWithPatch() throws Exception {
        // Initialize the database
        topicTestRepository.saveAndFlush(topicTest);

        int databaseSizeBeforeUpdate = topicTestRepository.findAll().size();

        // Update the topicTest using partial update
        TopicTest partialUpdatedTopicTest = new TopicTest();
        partialUpdatedTopicTest.setId(topicTest.getId());

        partialUpdatedTopicTest.name(UPDATED_NAME);

        restTopicTestMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTopicTest.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTopicTest))
            )
            .andExpect(status().isOk());

        // Validate the TopicTest in the database
        List<TopicTest> topicTestList = topicTestRepository.findAll();
        assertThat(topicTestList).hasSize(databaseSizeBeforeUpdate);
        TopicTest testTopicTest = topicTestList.get(topicTestList.size() - 1);
        assertThat(testTopicTest.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void patchNonExistingTopicTest() throws Exception {
        int databaseSizeBeforeUpdate = topicTestRepository.findAll().size();
        topicTest.setId(count.incrementAndGet());

        // Create the TopicTest
        TopicTestDTO topicTestDTO = topicTestMapper.toDto(topicTest);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTopicTestMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, topicTestDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(topicTestDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TopicTest in the database
        List<TopicTest> topicTestList = topicTestRepository.findAll();
        assertThat(topicTestList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchTopicTest() throws Exception {
        int databaseSizeBeforeUpdate = topicTestRepository.findAll().size();
        topicTest.setId(count.incrementAndGet());

        // Create the TopicTest
        TopicTestDTO topicTestDTO = topicTestMapper.toDto(topicTest);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTopicTestMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(topicTestDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TopicTest in the database
        List<TopicTest> topicTestList = topicTestRepository.findAll();
        assertThat(topicTestList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamTopicTest() throws Exception {
        int databaseSizeBeforeUpdate = topicTestRepository.findAll().size();
        topicTest.setId(count.incrementAndGet());

        // Create the TopicTest
        TopicTestDTO topicTestDTO = topicTestMapper.toDto(topicTest);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTopicTestMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(topicTestDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the TopicTest in the database
        List<TopicTest> topicTestList = topicTestRepository.findAll();
        assertThat(topicTestList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteTopicTest() throws Exception {
        // Initialize the database
        topicTestRepository.saveAndFlush(topicTest);

        int databaseSizeBeforeDelete = topicTestRepository.findAll().size();

        // Delete the topicTest
        restTopicTestMockMvc
            .perform(delete(ENTITY_API_URL_ID, topicTest.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TopicTest> topicTestList = topicTestRepository.findAll();
        assertThat(topicTestList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
