package uz.sudex.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import uz.sudex.IntegrationTest;
import uz.sudex.domain.Assessment;
import uz.sudex.domain.References;
import uz.sudex.repository.ReferencesRepository;
import uz.sudex.service.criteria.ReferencesCriteria;
import uz.sudex.service.dto.ReferencesDTO;
import uz.sudex.service.mapper.ReferencesMapper;

/**
 * Integration tests for the {@link ReferencesResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ReferencesResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/references";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ReferencesRepository referencesRepository;

    @Autowired
    private ReferencesMapper referencesMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restReferencesMockMvc;

    private References references;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static References createEntity(EntityManager em) {
        References references = new References().name(DEFAULT_NAME);
        return references;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static References createUpdatedEntity(EntityManager em) {
        References references = new References().name(UPDATED_NAME);
        return references;
    }

    @BeforeEach
    public void initTest() {
        references = createEntity(em);
    }

    @Test
    @Transactional
    void createReferences() throws Exception {
        int databaseSizeBeforeCreate = referencesRepository.findAll().size();
        // Create the References
        ReferencesDTO referencesDTO = referencesMapper.toDto(references);
        restReferencesMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(referencesDTO)))
            .andExpect(status().isCreated());

        // Validate the References in the database
        List<References> referencesList = referencesRepository.findAll();
        assertThat(referencesList).hasSize(databaseSizeBeforeCreate + 1);
        References testReferences = referencesList.get(referencesList.size() - 1);
        assertThat(testReferences.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    void createReferencesWithExistingId() throws Exception {
        // Create the References with an existing ID
        references.setId(1L);
        ReferencesDTO referencesDTO = referencesMapper.toDto(references);

        int databaseSizeBeforeCreate = referencesRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restReferencesMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(referencesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the References in the database
        List<References> referencesList = referencesRepository.findAll();
        assertThat(referencesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllReferences() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        // Get all the referencesList
        restReferencesMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(references.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }

    @Test
    @Transactional
    void getReferences() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        // Get the references
        restReferencesMockMvc
            .perform(get(ENTITY_API_URL_ID, references.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(references.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    void getReferencesByIdFiltering() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        Long id = references.getId();

        defaultReferencesShouldBeFound("id.equals=" + id);
        defaultReferencesShouldNotBeFound("id.notEquals=" + id);

        defaultReferencesShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultReferencesShouldNotBeFound("id.greaterThan=" + id);

        defaultReferencesShouldBeFound("id.lessThanOrEqual=" + id);
        defaultReferencesShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllReferencesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        // Get all the referencesList where name equals to DEFAULT_NAME
        defaultReferencesShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the referencesList where name equals to UPDATED_NAME
        defaultReferencesShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllReferencesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        // Get all the referencesList where name in DEFAULT_NAME or UPDATED_NAME
        defaultReferencesShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the referencesList where name equals to UPDATED_NAME
        defaultReferencesShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllReferencesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        // Get all the referencesList where name is not null
        defaultReferencesShouldBeFound("name.specified=true");

        // Get all the referencesList where name is null
        defaultReferencesShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllReferencesByNameContainsSomething() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        // Get all the referencesList where name contains DEFAULT_NAME
        defaultReferencesShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the referencesList where name contains UPDATED_NAME
        defaultReferencesShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllReferencesByNameNotContainsSomething() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        // Get all the referencesList where name does not contain DEFAULT_NAME
        defaultReferencesShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the referencesList where name does not contain UPDATED_NAME
        defaultReferencesShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllReferencesByAssessmentIsEqualToSomething() throws Exception {
        Assessment assessment;
        if (TestUtil.findAll(em, Assessment.class).isEmpty()) {
            referencesRepository.saveAndFlush(references);
            assessment = AssessmentResourceIT.createEntity(em);
        } else {
            assessment = TestUtil.findAll(em, Assessment.class).get(0);
        }
        em.persist(assessment);
        em.flush();
        references.setAssessment(assessment);
        referencesRepository.saveAndFlush(references);
        Long assessmentId = assessment.getId();

        // Get all the referencesList where assessment equals to assessmentId
        defaultReferencesShouldBeFound("assessmentId.equals=" + assessmentId);

        // Get all the referencesList where assessment equals to (assessmentId + 1)
        defaultReferencesShouldNotBeFound("assessmentId.equals=" + (assessmentId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultReferencesShouldBeFound(String filter) throws Exception {
        restReferencesMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(references.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));

        // Check, that the count call also returns 1
        restReferencesMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultReferencesShouldNotBeFound(String filter) throws Exception {
        restReferencesMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restReferencesMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingReferences() throws Exception {
        // Get the references
        restReferencesMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingReferences() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        int databaseSizeBeforeUpdate = referencesRepository.findAll().size();

        // Update the references
        References updatedReferences = referencesRepository.findById(references.getId()).get();
        // Disconnect from session so that the updates on updatedReferences are not directly saved in db
        em.detach(updatedReferences);
        updatedReferences.name(UPDATED_NAME);
        ReferencesDTO referencesDTO = referencesMapper.toDto(updatedReferences);

        restReferencesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, referencesDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(referencesDTO))
            )
            .andExpect(status().isOk());

        // Validate the References in the database
        List<References> referencesList = referencesRepository.findAll();
        assertThat(referencesList).hasSize(databaseSizeBeforeUpdate);
        References testReferences = referencesList.get(referencesList.size() - 1);
        assertThat(testReferences.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void putNonExistingReferences() throws Exception {
        int databaseSizeBeforeUpdate = referencesRepository.findAll().size();
        references.setId(count.incrementAndGet());

        // Create the References
        ReferencesDTO referencesDTO = referencesMapper.toDto(references);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restReferencesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, referencesDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(referencesDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the References in the database
        List<References> referencesList = referencesRepository.findAll();
        assertThat(referencesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchReferences() throws Exception {
        int databaseSizeBeforeUpdate = referencesRepository.findAll().size();
        references.setId(count.incrementAndGet());

        // Create the References
        ReferencesDTO referencesDTO = referencesMapper.toDto(references);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restReferencesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(referencesDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the References in the database
        List<References> referencesList = referencesRepository.findAll();
        assertThat(referencesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamReferences() throws Exception {
        int databaseSizeBeforeUpdate = referencesRepository.findAll().size();
        references.setId(count.incrementAndGet());

        // Create the References
        ReferencesDTO referencesDTO = referencesMapper.toDto(references);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restReferencesMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(referencesDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the References in the database
        List<References> referencesList = referencesRepository.findAll();
        assertThat(referencesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateReferencesWithPatch() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        int databaseSizeBeforeUpdate = referencesRepository.findAll().size();

        // Update the references using partial update
        References partialUpdatedReferences = new References();
        partialUpdatedReferences.setId(references.getId());

        restReferencesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedReferences.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedReferences))
            )
            .andExpect(status().isOk());

        // Validate the References in the database
        List<References> referencesList = referencesRepository.findAll();
        assertThat(referencesList).hasSize(databaseSizeBeforeUpdate);
        References testReferences = referencesList.get(referencesList.size() - 1);
        assertThat(testReferences.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    void fullUpdateReferencesWithPatch() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        int databaseSizeBeforeUpdate = referencesRepository.findAll().size();

        // Update the references using partial update
        References partialUpdatedReferences = new References();
        partialUpdatedReferences.setId(references.getId());

        partialUpdatedReferences.name(UPDATED_NAME);

        restReferencesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedReferences.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedReferences))
            )
            .andExpect(status().isOk());

        // Validate the References in the database
        List<References> referencesList = referencesRepository.findAll();
        assertThat(referencesList).hasSize(databaseSizeBeforeUpdate);
        References testReferences = referencesList.get(referencesList.size() - 1);
        assertThat(testReferences.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void patchNonExistingReferences() throws Exception {
        int databaseSizeBeforeUpdate = referencesRepository.findAll().size();
        references.setId(count.incrementAndGet());

        // Create the References
        ReferencesDTO referencesDTO = referencesMapper.toDto(references);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restReferencesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, referencesDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(referencesDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the References in the database
        List<References> referencesList = referencesRepository.findAll();
        assertThat(referencesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchReferences() throws Exception {
        int databaseSizeBeforeUpdate = referencesRepository.findAll().size();
        references.setId(count.incrementAndGet());

        // Create the References
        ReferencesDTO referencesDTO = referencesMapper.toDto(references);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restReferencesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(referencesDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the References in the database
        List<References> referencesList = referencesRepository.findAll();
        assertThat(referencesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamReferences() throws Exception {
        int databaseSizeBeforeUpdate = referencesRepository.findAll().size();
        references.setId(count.incrementAndGet());

        // Create the References
        ReferencesDTO referencesDTO = referencesMapper.toDto(references);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restReferencesMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(referencesDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the References in the database
        List<References> referencesList = referencesRepository.findAll();
        assertThat(referencesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteReferences() throws Exception {
        // Initialize the database
        referencesRepository.saveAndFlush(references);

        int databaseSizeBeforeDelete = referencesRepository.findAll().size();

        // Delete the references
        restReferencesMockMvc
            .perform(delete(ENTITY_API_URL_ID, references.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<References> referencesList = referencesRepository.findAll();
        assertThat(referencesList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
