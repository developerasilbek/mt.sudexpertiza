package uz.sudex.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import uz.sudex.IntegrationTest;
import uz.sudex.domain.TestResult;
import uz.sudex.domain.TopicTest;
import uz.sudex.repository.TestResultRepository;
import uz.sudex.service.criteria.TestResultCriteria;
import uz.sudex.service.dto.TestResultDTO;
import uz.sudex.service.mapper.TestResultMapper;

/**
 * Integration tests for the {@link TestResultResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class TestResultResourceIT {

    private static final Instant DEFAULT_STARTED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_STARTED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_FINISHED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FINISHED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_SCORE = 1;
    private static final Integer UPDATED_SCORE = 2;
    private static final Integer SMALLER_SCORE = 1 - 1;

    private static final Integer DEFAULT_CORRECT_ANSWERS = 1;
    private static final Integer UPDATED_CORRECT_ANSWERS = 2;
    private static final Integer SMALLER_CORRECT_ANSWERS = 1 - 1;

    private static final String ENTITY_API_URL = "/api/test-results";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private TestResultRepository testResultRepository;

    @Autowired
    private TestResultMapper testResultMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTestResultMockMvc;

    private TestResult testResult;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TestResult createEntity(EntityManager em) {
        TestResult testResult = new TestResult()
            .startedAt(DEFAULT_STARTED_AT)
            .finishedAt(DEFAULT_FINISHED_AT)
            .score(DEFAULT_SCORE)
            .correctAnswers(DEFAULT_CORRECT_ANSWERS);
        return testResult;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TestResult createUpdatedEntity(EntityManager em) {
        TestResult testResult = new TestResult()
            .startedAt(UPDATED_STARTED_AT)
            .finishedAt(UPDATED_FINISHED_AT)
            .score(UPDATED_SCORE)
            .correctAnswers(UPDATED_CORRECT_ANSWERS);
        return testResult;
    }

    @BeforeEach
    public void initTest() {
        testResult = createEntity(em);
    }

    @Test
    @Transactional
    void createTestResult() throws Exception {
        int databaseSizeBeforeCreate = testResultRepository.findAll().size();
        // Create the TestResult
        TestResultDTO testResultDTO = testResultMapper.toDto(testResult);
        restTestResultMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(testResultDTO)))
            .andExpect(status().isCreated());

        // Validate the TestResult in the database
        List<TestResult> testResultList = testResultRepository.findAll();
        assertThat(testResultList).hasSize(databaseSizeBeforeCreate + 1);
        TestResult testTestResult = testResultList.get(testResultList.size() - 1);
        assertThat(testTestResult.getStartedAt()).isEqualTo(DEFAULT_STARTED_AT);
        assertThat(testTestResult.getFinishedAt()).isEqualTo(DEFAULT_FINISHED_AT);
        assertThat(testTestResult.getScore()).isEqualTo(DEFAULT_SCORE);
        assertThat(testTestResult.getCorrectAnswers()).isEqualTo(DEFAULT_CORRECT_ANSWERS);
    }

    @Test
    @Transactional
    void createTestResultWithExistingId() throws Exception {
        // Create the TestResult with an existing ID
        testResult.setId(1L);
        TestResultDTO testResultDTO = testResultMapper.toDto(testResult);

        int databaseSizeBeforeCreate = testResultRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restTestResultMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(testResultDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TestResult in the database
        List<TestResult> testResultList = testResultRepository.findAll();
        assertThat(testResultList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllTestResults() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get all the testResultList
        restTestResultMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(testResult.getId().intValue())))
            .andExpect(jsonPath("$.[*].startedAt").value(hasItem(DEFAULT_STARTED_AT.toString())))
            .andExpect(jsonPath("$.[*].finishedAt").value(hasItem(DEFAULT_FINISHED_AT.toString())))
            .andExpect(jsonPath("$.[*].score").value(hasItem(DEFAULT_SCORE)))
            .andExpect(jsonPath("$.[*].correctAnswers").value(hasItem(DEFAULT_CORRECT_ANSWERS)));
    }

    @Test
    @Transactional
    void getTestResult() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get the testResult
        restTestResultMockMvc
            .perform(get(ENTITY_API_URL_ID, testResult.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(testResult.getId().intValue()))
            .andExpect(jsonPath("$.startedAt").value(DEFAULT_STARTED_AT.toString()))
            .andExpect(jsonPath("$.finishedAt").value(DEFAULT_FINISHED_AT.toString()))
            .andExpect(jsonPath("$.score").value(DEFAULT_SCORE))
            .andExpect(jsonPath("$.correctAnswers").value(DEFAULT_CORRECT_ANSWERS));
    }

    @Test
    @Transactional
    void getTestResultsByIdFiltering() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        Long id = testResult.getId();

        defaultTestResultShouldBeFound("id.equals=" + id);
        defaultTestResultShouldNotBeFound("id.notEquals=" + id);

        defaultTestResultShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTestResultShouldNotBeFound("id.greaterThan=" + id);

        defaultTestResultShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTestResultShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllTestResultsByStartedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get all the testResultList where startedAt equals to DEFAULT_STARTED_AT
        defaultTestResultShouldBeFound("startedAt.equals=" + DEFAULT_STARTED_AT);

        // Get all the testResultList where startedAt equals to UPDATED_STARTED_AT
        defaultTestResultShouldNotBeFound("startedAt.equals=" + UPDATED_STARTED_AT);
    }

    @Test
    @Transactional
    void getAllTestResultsByStartedAtIsInShouldWork() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get all the testResultList where startedAt in DEFAULT_STARTED_AT or UPDATED_STARTED_AT
        defaultTestResultShouldBeFound("startedAt.in=" + DEFAULT_STARTED_AT + "," + UPDATED_STARTED_AT);

        // Get all the testResultList where startedAt equals to UPDATED_STARTED_AT
        defaultTestResultShouldNotBeFound("startedAt.in=" + UPDATED_STARTED_AT);
    }

    @Test
    @Transactional
    void getAllTestResultsByStartedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get all the testResultList where startedAt is not null
        defaultTestResultShouldBeFound("startedAt.specified=true");

        // Get all the testResultList where startedAt is null
        defaultTestResultShouldNotBeFound("startedAt.specified=false");
    }

    @Test
    @Transactional
    void getAllTestResultsByFinishedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get all the testResultList where finishedAt equals to DEFAULT_FINISHED_AT
        defaultTestResultShouldBeFound("finishedAt.equals=" + DEFAULT_FINISHED_AT);

        // Get all the testResultList where finishedAt equals to UPDATED_FINISHED_AT
        defaultTestResultShouldNotBeFound("finishedAt.equals=" + UPDATED_FINISHED_AT);
    }

    @Test
    @Transactional
    void getAllTestResultsByFinishedAtIsInShouldWork() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get all the testResultList where finishedAt in DEFAULT_FINISHED_AT or UPDATED_FINISHED_AT
        defaultTestResultShouldBeFound("finishedAt.in=" + DEFAULT_FINISHED_AT + "," + UPDATED_FINISHED_AT);

        // Get all the testResultList where finishedAt equals to UPDATED_FINISHED_AT
        defaultTestResultShouldNotBeFound("finishedAt.in=" + UPDATED_FINISHED_AT);
    }

    @Test
    @Transactional
    void getAllTestResultsByFinishedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get all the testResultList where finishedAt is not null
        defaultTestResultShouldBeFound("finishedAt.specified=true");

        // Get all the testResultList where finishedAt is null
        defaultTestResultShouldNotBeFound("finishedAt.specified=false");
    }

    @Test
    @Transactional
    void getAllTestResultsByScoreIsEqualToSomething() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get all the testResultList where score equals to DEFAULT_SCORE
        defaultTestResultShouldBeFound("score.equals=" + DEFAULT_SCORE);

        // Get all the testResultList where score equals to UPDATED_SCORE
        defaultTestResultShouldNotBeFound("score.equals=" + UPDATED_SCORE);
    }

    @Test
    @Transactional
    void getAllTestResultsByScoreIsInShouldWork() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get all the testResultList where score in DEFAULT_SCORE or UPDATED_SCORE
        defaultTestResultShouldBeFound("score.in=" + DEFAULT_SCORE + "," + UPDATED_SCORE);

        // Get all the testResultList where score equals to UPDATED_SCORE
        defaultTestResultShouldNotBeFound("score.in=" + UPDATED_SCORE);
    }

    @Test
    @Transactional
    void getAllTestResultsByScoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get all the testResultList where score is not null
        defaultTestResultShouldBeFound("score.specified=true");

        // Get all the testResultList where score is null
        defaultTestResultShouldNotBeFound("score.specified=false");
    }

    @Test
    @Transactional
    void getAllTestResultsByScoreIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get all the testResultList where score is greater than or equal to DEFAULT_SCORE
        defaultTestResultShouldBeFound("score.greaterThanOrEqual=" + DEFAULT_SCORE);

        // Get all the testResultList where score is greater than or equal to UPDATED_SCORE
        defaultTestResultShouldNotBeFound("score.greaterThanOrEqual=" + UPDATED_SCORE);
    }

    @Test
    @Transactional
    void getAllTestResultsByScoreIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get all the testResultList where score is less than or equal to DEFAULT_SCORE
        defaultTestResultShouldBeFound("score.lessThanOrEqual=" + DEFAULT_SCORE);

        // Get all the testResultList where score is less than or equal to SMALLER_SCORE
        defaultTestResultShouldNotBeFound("score.lessThanOrEqual=" + SMALLER_SCORE);
    }

    @Test
    @Transactional
    void getAllTestResultsByScoreIsLessThanSomething() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get all the testResultList where score is less than DEFAULT_SCORE
        defaultTestResultShouldNotBeFound("score.lessThan=" + DEFAULT_SCORE);

        // Get all the testResultList where score is less than UPDATED_SCORE
        defaultTestResultShouldBeFound("score.lessThan=" + UPDATED_SCORE);
    }

    @Test
    @Transactional
    void getAllTestResultsByScoreIsGreaterThanSomething() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get all the testResultList where score is greater than DEFAULT_SCORE
        defaultTestResultShouldNotBeFound("score.greaterThan=" + DEFAULT_SCORE);

        // Get all the testResultList where score is greater than SMALLER_SCORE
        defaultTestResultShouldBeFound("score.greaterThan=" + SMALLER_SCORE);
    }

    @Test
    @Transactional
    void getAllTestResultsByCorrectAnswersIsEqualToSomething() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get all the testResultList where correctAnswers equals to DEFAULT_CORRECT_ANSWERS
        defaultTestResultShouldBeFound("correctAnswers.equals=" + DEFAULT_CORRECT_ANSWERS);

        // Get all the testResultList where correctAnswers equals to UPDATED_CORRECT_ANSWERS
        defaultTestResultShouldNotBeFound("correctAnswers.equals=" + UPDATED_CORRECT_ANSWERS);
    }

    @Test
    @Transactional
    void getAllTestResultsByCorrectAnswersIsInShouldWork() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get all the testResultList where correctAnswers in DEFAULT_CORRECT_ANSWERS or UPDATED_CORRECT_ANSWERS
        defaultTestResultShouldBeFound("correctAnswers.in=" + DEFAULT_CORRECT_ANSWERS + "," + UPDATED_CORRECT_ANSWERS);

        // Get all the testResultList where correctAnswers equals to UPDATED_CORRECT_ANSWERS
        defaultTestResultShouldNotBeFound("correctAnswers.in=" + UPDATED_CORRECT_ANSWERS);
    }

    @Test
    @Transactional
    void getAllTestResultsByCorrectAnswersIsNullOrNotNull() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get all the testResultList where correctAnswers is not null
        defaultTestResultShouldBeFound("correctAnswers.specified=true");

        // Get all the testResultList where correctAnswers is null
        defaultTestResultShouldNotBeFound("correctAnswers.specified=false");
    }

    @Test
    @Transactional
    void getAllTestResultsByCorrectAnswersIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get all the testResultList where correctAnswers is greater than or equal to DEFAULT_CORRECT_ANSWERS
        defaultTestResultShouldBeFound("correctAnswers.greaterThanOrEqual=" + DEFAULT_CORRECT_ANSWERS);

        // Get all the testResultList where correctAnswers is greater than or equal to UPDATED_CORRECT_ANSWERS
        defaultTestResultShouldNotBeFound("correctAnswers.greaterThanOrEqual=" + UPDATED_CORRECT_ANSWERS);
    }

    @Test
    @Transactional
    void getAllTestResultsByCorrectAnswersIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get all the testResultList where correctAnswers is less than or equal to DEFAULT_CORRECT_ANSWERS
        defaultTestResultShouldBeFound("correctAnswers.lessThanOrEqual=" + DEFAULT_CORRECT_ANSWERS);

        // Get all the testResultList where correctAnswers is less than or equal to SMALLER_CORRECT_ANSWERS
        defaultTestResultShouldNotBeFound("correctAnswers.lessThanOrEqual=" + SMALLER_CORRECT_ANSWERS);
    }

    @Test
    @Transactional
    void getAllTestResultsByCorrectAnswersIsLessThanSomething() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get all the testResultList where correctAnswers is less than DEFAULT_CORRECT_ANSWERS
        defaultTestResultShouldNotBeFound("correctAnswers.lessThan=" + DEFAULT_CORRECT_ANSWERS);

        // Get all the testResultList where correctAnswers is less than UPDATED_CORRECT_ANSWERS
        defaultTestResultShouldBeFound("correctAnswers.lessThan=" + UPDATED_CORRECT_ANSWERS);
    }

    @Test
    @Transactional
    void getAllTestResultsByCorrectAnswersIsGreaterThanSomething() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get all the testResultList where correctAnswers is greater than DEFAULT_CORRECT_ANSWERS
        defaultTestResultShouldNotBeFound("correctAnswers.greaterThan=" + DEFAULT_CORRECT_ANSWERS);

        // Get all the testResultList where correctAnswers is greater than SMALLER_CORRECT_ANSWERS
        defaultTestResultShouldBeFound("correctAnswers.greaterThan=" + SMALLER_CORRECT_ANSWERS);
    }

    @Test
    @Transactional
    void getAllTestResultsByTestIsEqualToSomething() throws Exception {
        TopicTest test;
        if (TestUtil.findAll(em, TopicTest.class).isEmpty()) {
            testResultRepository.saveAndFlush(testResult);
            test = TopicTestResourceIT.createEntity(em);
        } else {
            test = TestUtil.findAll(em, TopicTest.class).get(0);
        }
        em.persist(test);
        em.flush();
        testResult.setTest(test);
        testResultRepository.saveAndFlush(testResult);
        Long testId = test.getId();

        // Get all the testResultList where test equals to testId
        defaultTestResultShouldBeFound("testId.equals=" + testId);

        // Get all the testResultList where test equals to (testId + 1)
        defaultTestResultShouldNotBeFound("testId.equals=" + (testId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTestResultShouldBeFound(String filter) throws Exception {
        restTestResultMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(testResult.getId().intValue())))
            .andExpect(jsonPath("$.[*].startedAt").value(hasItem(DEFAULT_STARTED_AT.toString())))
            .andExpect(jsonPath("$.[*].finishedAt").value(hasItem(DEFAULT_FINISHED_AT.toString())))
            .andExpect(jsonPath("$.[*].score").value(hasItem(DEFAULT_SCORE)))
            .andExpect(jsonPath("$.[*].correctAnswers").value(hasItem(DEFAULT_CORRECT_ANSWERS)));

        // Check, that the count call also returns 1
        restTestResultMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTestResultShouldNotBeFound(String filter) throws Exception {
        restTestResultMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTestResultMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingTestResult() throws Exception {
        // Get the testResult
        restTestResultMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingTestResult() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        int databaseSizeBeforeUpdate = testResultRepository.findAll().size();

        // Update the testResult
        TestResult updatedTestResult = testResultRepository.findById(testResult.getId()).get();
        // Disconnect from session so that the updates on updatedTestResult are not directly saved in db
        em.detach(updatedTestResult);
        updatedTestResult
            .startedAt(UPDATED_STARTED_AT)
            .finishedAt(UPDATED_FINISHED_AT)
            .score(UPDATED_SCORE)
            .correctAnswers(UPDATED_CORRECT_ANSWERS);
        TestResultDTO testResultDTO = testResultMapper.toDto(updatedTestResult);

        restTestResultMockMvc
            .perform(
                put(ENTITY_API_URL_ID, testResultDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(testResultDTO))
            )
            .andExpect(status().isOk());

        // Validate the TestResult in the database
        List<TestResult> testResultList = testResultRepository.findAll();
        assertThat(testResultList).hasSize(databaseSizeBeforeUpdate);
        TestResult testTestResult = testResultList.get(testResultList.size() - 1);
        assertThat(testTestResult.getStartedAt()).isEqualTo(UPDATED_STARTED_AT);
        assertThat(testTestResult.getFinishedAt()).isEqualTo(UPDATED_FINISHED_AT);
        assertThat(testTestResult.getScore()).isEqualTo(UPDATED_SCORE);
        assertThat(testTestResult.getCorrectAnswers()).isEqualTo(UPDATED_CORRECT_ANSWERS);
    }

    @Test
    @Transactional
    void putNonExistingTestResult() throws Exception {
        int databaseSizeBeforeUpdate = testResultRepository.findAll().size();
        testResult.setId(count.incrementAndGet());

        // Create the TestResult
        TestResultDTO testResultDTO = testResultMapper.toDto(testResult);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTestResultMockMvc
            .perform(
                put(ENTITY_API_URL_ID, testResultDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(testResultDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TestResult in the database
        List<TestResult> testResultList = testResultRepository.findAll();
        assertThat(testResultList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchTestResult() throws Exception {
        int databaseSizeBeforeUpdate = testResultRepository.findAll().size();
        testResult.setId(count.incrementAndGet());

        // Create the TestResult
        TestResultDTO testResultDTO = testResultMapper.toDto(testResult);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTestResultMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(testResultDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TestResult in the database
        List<TestResult> testResultList = testResultRepository.findAll();
        assertThat(testResultList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamTestResult() throws Exception {
        int databaseSizeBeforeUpdate = testResultRepository.findAll().size();
        testResult.setId(count.incrementAndGet());

        // Create the TestResult
        TestResultDTO testResultDTO = testResultMapper.toDto(testResult);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTestResultMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(testResultDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the TestResult in the database
        List<TestResult> testResultList = testResultRepository.findAll();
        assertThat(testResultList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateTestResultWithPatch() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        int databaseSizeBeforeUpdate = testResultRepository.findAll().size();

        // Update the testResult using partial update
        TestResult partialUpdatedTestResult = new TestResult();
        partialUpdatedTestResult.setId(testResult.getId());

        partialUpdatedTestResult.finishedAt(UPDATED_FINISHED_AT).correctAnswers(UPDATED_CORRECT_ANSWERS);

        restTestResultMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTestResult.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTestResult))
            )
            .andExpect(status().isOk());

        // Validate the TestResult in the database
        List<TestResult> testResultList = testResultRepository.findAll();
        assertThat(testResultList).hasSize(databaseSizeBeforeUpdate);
        TestResult testTestResult = testResultList.get(testResultList.size() - 1);
        assertThat(testTestResult.getStartedAt()).isEqualTo(DEFAULT_STARTED_AT);
        assertThat(testTestResult.getFinishedAt()).isEqualTo(UPDATED_FINISHED_AT);
        assertThat(testTestResult.getScore()).isEqualTo(DEFAULT_SCORE);
        assertThat(testTestResult.getCorrectAnswers()).isEqualTo(UPDATED_CORRECT_ANSWERS);
    }

    @Test
    @Transactional
    void fullUpdateTestResultWithPatch() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        int databaseSizeBeforeUpdate = testResultRepository.findAll().size();

        // Update the testResult using partial update
        TestResult partialUpdatedTestResult = new TestResult();
        partialUpdatedTestResult.setId(testResult.getId());

        partialUpdatedTestResult
            .startedAt(UPDATED_STARTED_AT)
            .finishedAt(UPDATED_FINISHED_AT)
            .score(UPDATED_SCORE)
            .correctAnswers(UPDATED_CORRECT_ANSWERS);

        restTestResultMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTestResult.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTestResult))
            )
            .andExpect(status().isOk());

        // Validate the TestResult in the database
        List<TestResult> testResultList = testResultRepository.findAll();
        assertThat(testResultList).hasSize(databaseSizeBeforeUpdate);
        TestResult testTestResult = testResultList.get(testResultList.size() - 1);
        assertThat(testTestResult.getStartedAt()).isEqualTo(UPDATED_STARTED_AT);
        assertThat(testTestResult.getFinishedAt()).isEqualTo(UPDATED_FINISHED_AT);
        assertThat(testTestResult.getScore()).isEqualTo(UPDATED_SCORE);
        assertThat(testTestResult.getCorrectAnswers()).isEqualTo(UPDATED_CORRECT_ANSWERS);
    }

    @Test
    @Transactional
    void patchNonExistingTestResult() throws Exception {
        int databaseSizeBeforeUpdate = testResultRepository.findAll().size();
        testResult.setId(count.incrementAndGet());

        // Create the TestResult
        TestResultDTO testResultDTO = testResultMapper.toDto(testResult);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTestResultMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, testResultDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(testResultDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TestResult in the database
        List<TestResult> testResultList = testResultRepository.findAll();
        assertThat(testResultList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchTestResult() throws Exception {
        int databaseSizeBeforeUpdate = testResultRepository.findAll().size();
        testResult.setId(count.incrementAndGet());

        // Create the TestResult
        TestResultDTO testResultDTO = testResultMapper.toDto(testResult);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTestResultMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(testResultDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TestResult in the database
        List<TestResult> testResultList = testResultRepository.findAll();
        assertThat(testResultList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamTestResult() throws Exception {
        int databaseSizeBeforeUpdate = testResultRepository.findAll().size();
        testResult.setId(count.incrementAndGet());

        // Create the TestResult
        TestResultDTO testResultDTO = testResultMapper.toDto(testResult);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTestResultMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(testResultDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the TestResult in the database
        List<TestResult> testResultList = testResultRepository.findAll();
        assertThat(testResultList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteTestResult() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        int databaseSizeBeforeDelete = testResultRepository.findAll().size();

        // Delete the testResult
        restTestResultMockMvc
            .perform(delete(ENTITY_API_URL_ID, testResult.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TestResult> testResultList = testResultRepository.findAll();
        assertThat(testResultList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
