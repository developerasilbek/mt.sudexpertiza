package uz.sudex.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import uz.sudex.IntegrationTest;
import uz.sudex.domain.Assessment;
import uz.sudex.domain.Glossary;
import uz.sudex.repository.GlossaryRepository;
import uz.sudex.service.criteria.GlossaryCriteria;
import uz.sudex.service.dto.GlossaryDTO;
import uz.sudex.service.mapper.GlossaryMapper;

/**
 * Integration tests for the {@link GlossaryResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class GlossaryResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/glossaries";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private GlossaryRepository glossaryRepository;

    @Autowired
    private GlossaryMapper glossaryMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restGlossaryMockMvc;

    private Glossary glossary;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Glossary createEntity(EntityManager em) {
        Glossary glossary = new Glossary().name(DEFAULT_NAME);
        return glossary;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Glossary createUpdatedEntity(EntityManager em) {
        Glossary glossary = new Glossary().name(UPDATED_NAME);
        return glossary;
    }

    @BeforeEach
    public void initTest() {
        glossary = createEntity(em);
    }

    @Test
    @Transactional
    void createGlossary() throws Exception {
        int databaseSizeBeforeCreate = glossaryRepository.findAll().size();
        // Create the Glossary
        GlossaryDTO glossaryDTO = glossaryMapper.toDto(glossary);
        restGlossaryMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(glossaryDTO)))
            .andExpect(status().isCreated());

        // Validate the Glossary in the database
        List<Glossary> glossaryList = glossaryRepository.findAll();
        assertThat(glossaryList).hasSize(databaseSizeBeforeCreate + 1);
        Glossary testGlossary = glossaryList.get(glossaryList.size() - 1);
        assertThat(testGlossary.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    void createGlossaryWithExistingId() throws Exception {
        // Create the Glossary with an existing ID
        glossary.setId(1L);
        GlossaryDTO glossaryDTO = glossaryMapper.toDto(glossary);

        int databaseSizeBeforeCreate = glossaryRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restGlossaryMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(glossaryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Glossary in the database
        List<Glossary> glossaryList = glossaryRepository.findAll();
        assertThat(glossaryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllGlossaries() throws Exception {
        // Initialize the database
        glossaryRepository.saveAndFlush(glossary);

        // Get all the glossaryList
        restGlossaryMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(glossary.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }

    @Test
    @Transactional
    void getGlossary() throws Exception {
        // Initialize the database
        glossaryRepository.saveAndFlush(glossary);

        // Get the glossary
        restGlossaryMockMvc
            .perform(get(ENTITY_API_URL_ID, glossary.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(glossary.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    void getGlossariesByIdFiltering() throws Exception {
        // Initialize the database
        glossaryRepository.saveAndFlush(glossary);

        Long id = glossary.getId();

        defaultGlossaryShouldBeFound("id.equals=" + id);
        defaultGlossaryShouldNotBeFound("id.notEquals=" + id);

        defaultGlossaryShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultGlossaryShouldNotBeFound("id.greaterThan=" + id);

        defaultGlossaryShouldBeFound("id.lessThanOrEqual=" + id);
        defaultGlossaryShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllGlossariesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        glossaryRepository.saveAndFlush(glossary);

        // Get all the glossaryList where name equals to DEFAULT_NAME
        defaultGlossaryShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the glossaryList where name equals to UPDATED_NAME
        defaultGlossaryShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllGlossariesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        glossaryRepository.saveAndFlush(glossary);

        // Get all the glossaryList where name in DEFAULT_NAME or UPDATED_NAME
        defaultGlossaryShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the glossaryList where name equals to UPDATED_NAME
        defaultGlossaryShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllGlossariesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        glossaryRepository.saveAndFlush(glossary);

        // Get all the glossaryList where name is not null
        defaultGlossaryShouldBeFound("name.specified=true");

        // Get all the glossaryList where name is null
        defaultGlossaryShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllGlossariesByNameContainsSomething() throws Exception {
        // Initialize the database
        glossaryRepository.saveAndFlush(glossary);

        // Get all the glossaryList where name contains DEFAULT_NAME
        defaultGlossaryShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the glossaryList where name contains UPDATED_NAME
        defaultGlossaryShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllGlossariesByNameNotContainsSomething() throws Exception {
        // Initialize the database
        glossaryRepository.saveAndFlush(glossary);

        // Get all the glossaryList where name does not contain DEFAULT_NAME
        defaultGlossaryShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the glossaryList where name does not contain UPDATED_NAME
        defaultGlossaryShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllGlossariesByAssessmentIsEqualToSomething() throws Exception {
        Assessment assessment;
        if (TestUtil.findAll(em, Assessment.class).isEmpty()) {
            glossaryRepository.saveAndFlush(glossary);
            assessment = AssessmentResourceIT.createEntity(em);
        } else {
            assessment = TestUtil.findAll(em, Assessment.class).get(0);
        }
        em.persist(assessment);
        em.flush();
        glossary.setAssessment(assessment);
        glossaryRepository.saveAndFlush(glossary);
        Long assessmentId = assessment.getId();

        // Get all the glossaryList where assessment equals to assessmentId
        defaultGlossaryShouldBeFound("assessmentId.equals=" + assessmentId);

        // Get all the glossaryList where assessment equals to (assessmentId + 1)
        defaultGlossaryShouldNotBeFound("assessmentId.equals=" + (assessmentId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultGlossaryShouldBeFound(String filter) throws Exception {
        restGlossaryMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(glossary.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));

        // Check, that the count call also returns 1
        restGlossaryMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultGlossaryShouldNotBeFound(String filter) throws Exception {
        restGlossaryMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restGlossaryMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingGlossary() throws Exception {
        // Get the glossary
        restGlossaryMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingGlossary() throws Exception {
        // Initialize the database
        glossaryRepository.saveAndFlush(glossary);

        int databaseSizeBeforeUpdate = glossaryRepository.findAll().size();

        // Update the glossary
        Glossary updatedGlossary = glossaryRepository.findById(glossary.getId()).get();
        // Disconnect from session so that the updates on updatedGlossary are not directly saved in db
        em.detach(updatedGlossary);
        updatedGlossary.name(UPDATED_NAME);
        GlossaryDTO glossaryDTO = glossaryMapper.toDto(updatedGlossary);

        restGlossaryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, glossaryDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(glossaryDTO))
            )
            .andExpect(status().isOk());

        // Validate the Glossary in the database
        List<Glossary> glossaryList = glossaryRepository.findAll();
        assertThat(glossaryList).hasSize(databaseSizeBeforeUpdate);
        Glossary testGlossary = glossaryList.get(glossaryList.size() - 1);
        assertThat(testGlossary.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void putNonExistingGlossary() throws Exception {
        int databaseSizeBeforeUpdate = glossaryRepository.findAll().size();
        glossary.setId(count.incrementAndGet());

        // Create the Glossary
        GlossaryDTO glossaryDTO = glossaryMapper.toDto(glossary);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGlossaryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, glossaryDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(glossaryDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Glossary in the database
        List<Glossary> glossaryList = glossaryRepository.findAll();
        assertThat(glossaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchGlossary() throws Exception {
        int databaseSizeBeforeUpdate = glossaryRepository.findAll().size();
        glossary.setId(count.incrementAndGet());

        // Create the Glossary
        GlossaryDTO glossaryDTO = glossaryMapper.toDto(glossary);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGlossaryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(glossaryDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Glossary in the database
        List<Glossary> glossaryList = glossaryRepository.findAll();
        assertThat(glossaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamGlossary() throws Exception {
        int databaseSizeBeforeUpdate = glossaryRepository.findAll().size();
        glossary.setId(count.incrementAndGet());

        // Create the Glossary
        GlossaryDTO glossaryDTO = glossaryMapper.toDto(glossary);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGlossaryMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(glossaryDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Glossary in the database
        List<Glossary> glossaryList = glossaryRepository.findAll();
        assertThat(glossaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateGlossaryWithPatch() throws Exception {
        // Initialize the database
        glossaryRepository.saveAndFlush(glossary);

        int databaseSizeBeforeUpdate = glossaryRepository.findAll().size();

        // Update the glossary using partial update
        Glossary partialUpdatedGlossary = new Glossary();
        partialUpdatedGlossary.setId(glossary.getId());

        partialUpdatedGlossary.name(UPDATED_NAME);

        restGlossaryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedGlossary.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedGlossary))
            )
            .andExpect(status().isOk());

        // Validate the Glossary in the database
        List<Glossary> glossaryList = glossaryRepository.findAll();
        assertThat(glossaryList).hasSize(databaseSizeBeforeUpdate);
        Glossary testGlossary = glossaryList.get(glossaryList.size() - 1);
        assertThat(testGlossary.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void fullUpdateGlossaryWithPatch() throws Exception {
        // Initialize the database
        glossaryRepository.saveAndFlush(glossary);

        int databaseSizeBeforeUpdate = glossaryRepository.findAll().size();

        // Update the glossary using partial update
        Glossary partialUpdatedGlossary = new Glossary();
        partialUpdatedGlossary.setId(glossary.getId());

        partialUpdatedGlossary.name(UPDATED_NAME);

        restGlossaryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedGlossary.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedGlossary))
            )
            .andExpect(status().isOk());

        // Validate the Glossary in the database
        List<Glossary> glossaryList = glossaryRepository.findAll();
        assertThat(glossaryList).hasSize(databaseSizeBeforeUpdate);
        Glossary testGlossary = glossaryList.get(glossaryList.size() - 1);
        assertThat(testGlossary.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void patchNonExistingGlossary() throws Exception {
        int databaseSizeBeforeUpdate = glossaryRepository.findAll().size();
        glossary.setId(count.incrementAndGet());

        // Create the Glossary
        GlossaryDTO glossaryDTO = glossaryMapper.toDto(glossary);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGlossaryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, glossaryDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(glossaryDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Glossary in the database
        List<Glossary> glossaryList = glossaryRepository.findAll();
        assertThat(glossaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchGlossary() throws Exception {
        int databaseSizeBeforeUpdate = glossaryRepository.findAll().size();
        glossary.setId(count.incrementAndGet());

        // Create the Glossary
        GlossaryDTO glossaryDTO = glossaryMapper.toDto(glossary);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGlossaryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(glossaryDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Glossary in the database
        List<Glossary> glossaryList = glossaryRepository.findAll();
        assertThat(glossaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamGlossary() throws Exception {
        int databaseSizeBeforeUpdate = glossaryRepository.findAll().size();
        glossary.setId(count.incrementAndGet());

        // Create the Glossary
        GlossaryDTO glossaryDTO = glossaryMapper.toDto(glossary);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGlossaryMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(glossaryDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Glossary in the database
        List<Glossary> glossaryList = glossaryRepository.findAll();
        assertThat(glossaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteGlossary() throws Exception {
        // Initialize the database
        glossaryRepository.saveAndFlush(glossary);

        int databaseSizeBeforeDelete = glossaryRepository.findAll().size();

        // Delete the glossary
        restGlossaryMockMvc
            .perform(delete(ENTITY_API_URL_ID, glossary.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Glossary> glossaryList = glossaryRepository.findAll();
        assertThat(glossaryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
