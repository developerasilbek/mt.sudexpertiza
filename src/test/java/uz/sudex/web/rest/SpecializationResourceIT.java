package uz.sudex.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import uz.sudex.IntegrationTest;
import uz.sudex.domain.Specialization;
import uz.sudex.domain.enumeration.Centre;
import uz.sudex.repository.SpecializationRepository;
import uz.sudex.service.criteria.SpecializationCriteria;
import uz.sudex.service.dto.SpecializationDTO;
import uz.sudex.service.mapper.SpecializationMapper;

/**
 * Integration tests for the {@link SpecializationResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class SpecializationResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Centre DEFAULT_CENTRE = Centre.RSEM;
    private static final Centre UPDATED_CENTRE = Centre.RSEM;

    private static final String ENTITY_API_URL = "/api/specializations";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private SpecializationRepository specializationRepository;

    @Autowired
    private SpecializationMapper specializationMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSpecializationMockMvc;

    private Specialization specialization;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Specialization createEntity(EntityManager em) {
        Specialization specialization = new Specialization().name(DEFAULT_NAME).centre(DEFAULT_CENTRE);
        return specialization;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Specialization createUpdatedEntity(EntityManager em) {
        Specialization specialization = new Specialization().name(UPDATED_NAME).centre(UPDATED_CENTRE);
        return specialization;
    }

    @BeforeEach
    public void initTest() {
        specialization = createEntity(em);
    }

    @Test
    @Transactional
    void createSpecialization() throws Exception {
        int databaseSizeBeforeCreate = specializationRepository.findAll().size();
        // Create the Specialization
        SpecializationDTO specializationDTO = specializationMapper.toDto(specialization);
        restSpecializationMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(specializationDTO))
            )
            .andExpect(status().isCreated());

        // Validate the Specialization in the database
        List<Specialization> specializationList = specializationRepository.findAll();
        assertThat(specializationList).hasSize(databaseSizeBeforeCreate + 1);
        Specialization testSpecialization = specializationList.get(specializationList.size() - 1);
        assertThat(testSpecialization.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSpecialization.getCentre()).isEqualTo(DEFAULT_CENTRE);
    }

    @Test
    @Transactional
    void createSpecializationWithExistingId() throws Exception {
        // Create the Specialization with an existing ID
        specialization.setId(1L);
        SpecializationDTO specializationDTO = specializationMapper.toDto(specialization);

        int databaseSizeBeforeCreate = specializationRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restSpecializationMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(specializationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Specialization in the database
        List<Specialization> specializationList = specializationRepository.findAll();
        assertThat(specializationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllSpecializations() throws Exception {
        // Initialize the database
        specializationRepository.saveAndFlush(specialization);

        // Get all the specializationList
        restSpecializationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(specialization.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].centre").value(hasItem(DEFAULT_CENTRE.toString())));
    }

    @Test
    @Transactional
    void getSpecialization() throws Exception {
        // Initialize the database
        specializationRepository.saveAndFlush(specialization);

        // Get the specialization
        restSpecializationMockMvc
            .perform(get(ENTITY_API_URL_ID, specialization.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(specialization.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.centre").value(DEFAULT_CENTRE.toString()));
    }

    @Test
    @Transactional
    void getSpecializationsByIdFiltering() throws Exception {
        // Initialize the database
        specializationRepository.saveAndFlush(specialization);

        Long id = specialization.getId();

        defaultSpecializationShouldBeFound("id.equals=" + id);
        defaultSpecializationShouldNotBeFound("id.notEquals=" + id);

        defaultSpecializationShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultSpecializationShouldNotBeFound("id.greaterThan=" + id);

        defaultSpecializationShouldBeFound("id.lessThanOrEqual=" + id);
        defaultSpecializationShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllSpecializationsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        specializationRepository.saveAndFlush(specialization);

        // Get all the specializationList where name equals to DEFAULT_NAME
        defaultSpecializationShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the specializationList where name equals to UPDATED_NAME
        defaultSpecializationShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllSpecializationsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        specializationRepository.saveAndFlush(specialization);

        // Get all the specializationList where name in DEFAULT_NAME or UPDATED_NAME
        defaultSpecializationShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the specializationList where name equals to UPDATED_NAME
        defaultSpecializationShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllSpecializationsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        specializationRepository.saveAndFlush(specialization);

        // Get all the specializationList where name is not null
        defaultSpecializationShouldBeFound("name.specified=true");

        // Get all the specializationList where name is null
        defaultSpecializationShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllSpecializationsByNameContainsSomething() throws Exception {
        // Initialize the database
        specializationRepository.saveAndFlush(specialization);

        // Get all the specializationList where name contains DEFAULT_NAME
        defaultSpecializationShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the specializationList where name contains UPDATED_NAME
        defaultSpecializationShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllSpecializationsByNameNotContainsSomething() throws Exception {
        // Initialize the database
        specializationRepository.saveAndFlush(specialization);

        // Get all the specializationList where name does not contain DEFAULT_NAME
        defaultSpecializationShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the specializationList where name does not contain UPDATED_NAME
        defaultSpecializationShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllSpecializationsByCentreIsEqualToSomething() throws Exception {
        // Initialize the database
        specializationRepository.saveAndFlush(specialization);

        // Get all the specializationList where centre equals to DEFAULT_CENTRE
        defaultSpecializationShouldBeFound("centre.equals=" + DEFAULT_CENTRE);

        // Get all the specializationList where centre equals to UPDATED_CENTRE
        defaultSpecializationShouldNotBeFound("centre.equals=" + UPDATED_CENTRE);
    }

    @Test
    @Transactional
    void getAllSpecializationsByCentreIsInShouldWork() throws Exception {
        // Initialize the database
        specializationRepository.saveAndFlush(specialization);

        // Get all the specializationList where centre in DEFAULT_CENTRE or UPDATED_CENTRE
        defaultSpecializationShouldBeFound("centre.in=" + DEFAULT_CENTRE + "," + UPDATED_CENTRE);

        // Get all the specializationList where centre equals to UPDATED_CENTRE
        defaultSpecializationShouldNotBeFound("centre.in=" + UPDATED_CENTRE);
    }

    @Test
    @Transactional
    void getAllSpecializationsByCentreIsNullOrNotNull() throws Exception {
        // Initialize the database
        specializationRepository.saveAndFlush(specialization);

        // Get all the specializationList where centre is not null
        defaultSpecializationShouldBeFound("centre.specified=true");

        // Get all the specializationList where centre is null
        defaultSpecializationShouldNotBeFound("centre.specified=false");
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSpecializationShouldBeFound(String filter) throws Exception {
        restSpecializationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(specialization.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].centre").value(hasItem(DEFAULT_CENTRE.toString())));

        // Check, that the count call also returns 1
        restSpecializationMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSpecializationShouldNotBeFound(String filter) throws Exception {
        restSpecializationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSpecializationMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingSpecialization() throws Exception {
        // Get the specialization
        restSpecializationMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingSpecialization() throws Exception {
        // Initialize the database
        specializationRepository.saveAndFlush(specialization);

        int databaseSizeBeforeUpdate = specializationRepository.findAll().size();

        // Update the specialization
        Specialization updatedSpecialization = specializationRepository.findById(specialization.getId()).get();
        // Disconnect from session so that the updates on updatedSpecialization are not directly saved in db
        em.detach(updatedSpecialization);
        updatedSpecialization.name(UPDATED_NAME).centre(UPDATED_CENTRE);
        SpecializationDTO specializationDTO = specializationMapper.toDto(updatedSpecialization);

        restSpecializationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, specializationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(specializationDTO))
            )
            .andExpect(status().isOk());

        // Validate the Specialization in the database
        List<Specialization> specializationList = specializationRepository.findAll();
        assertThat(specializationList).hasSize(databaseSizeBeforeUpdate);
        Specialization testSpecialization = specializationList.get(specializationList.size() - 1);
        assertThat(testSpecialization.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSpecialization.getCentre()).isEqualTo(UPDATED_CENTRE);
    }

    @Test
    @Transactional
    void putNonExistingSpecialization() throws Exception {
        int databaseSizeBeforeUpdate = specializationRepository.findAll().size();
        specialization.setId(count.incrementAndGet());

        // Create the Specialization
        SpecializationDTO specializationDTO = specializationMapper.toDto(specialization);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSpecializationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, specializationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(specializationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Specialization in the database
        List<Specialization> specializationList = specializationRepository.findAll();
        assertThat(specializationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchSpecialization() throws Exception {
        int databaseSizeBeforeUpdate = specializationRepository.findAll().size();
        specialization.setId(count.incrementAndGet());

        // Create the Specialization
        SpecializationDTO specializationDTO = specializationMapper.toDto(specialization);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSpecializationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(specializationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Specialization in the database
        List<Specialization> specializationList = specializationRepository.findAll();
        assertThat(specializationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamSpecialization() throws Exception {
        int databaseSizeBeforeUpdate = specializationRepository.findAll().size();
        specialization.setId(count.incrementAndGet());

        // Create the Specialization
        SpecializationDTO specializationDTO = specializationMapper.toDto(specialization);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSpecializationMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(specializationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Specialization in the database
        List<Specialization> specializationList = specializationRepository.findAll();
        assertThat(specializationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateSpecializationWithPatch() throws Exception {
        // Initialize the database
        specializationRepository.saveAndFlush(specialization);

        int databaseSizeBeforeUpdate = specializationRepository.findAll().size();

        // Update the specialization using partial update
        Specialization partialUpdatedSpecialization = new Specialization();
        partialUpdatedSpecialization.setId(specialization.getId());

        partialUpdatedSpecialization.name(UPDATED_NAME).centre(UPDATED_CENTRE);

        restSpecializationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSpecialization.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSpecialization))
            )
            .andExpect(status().isOk());

        // Validate the Specialization in the database
        List<Specialization> specializationList = specializationRepository.findAll();
        assertThat(specializationList).hasSize(databaseSizeBeforeUpdate);
        Specialization testSpecialization = specializationList.get(specializationList.size() - 1);
        assertThat(testSpecialization.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSpecialization.getCentre()).isEqualTo(UPDATED_CENTRE);
    }

    @Test
    @Transactional
    void fullUpdateSpecializationWithPatch() throws Exception {
        // Initialize the database
        specializationRepository.saveAndFlush(specialization);

        int databaseSizeBeforeUpdate = specializationRepository.findAll().size();

        // Update the specialization using partial update
        Specialization partialUpdatedSpecialization = new Specialization();
        partialUpdatedSpecialization.setId(specialization.getId());

        partialUpdatedSpecialization.name(UPDATED_NAME).centre(UPDATED_CENTRE);

        restSpecializationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSpecialization.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSpecialization))
            )
            .andExpect(status().isOk());

        // Validate the Specialization in the database
        List<Specialization> specializationList = specializationRepository.findAll();
        assertThat(specializationList).hasSize(databaseSizeBeforeUpdate);
        Specialization testSpecialization = specializationList.get(specializationList.size() - 1);
        assertThat(testSpecialization.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSpecialization.getCentre()).isEqualTo(UPDATED_CENTRE);
    }

    @Test
    @Transactional
    void patchNonExistingSpecialization() throws Exception {
        int databaseSizeBeforeUpdate = specializationRepository.findAll().size();
        specialization.setId(count.incrementAndGet());

        // Create the Specialization
        SpecializationDTO specializationDTO = specializationMapper.toDto(specialization);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSpecializationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, specializationDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(specializationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Specialization in the database
        List<Specialization> specializationList = specializationRepository.findAll();
        assertThat(specializationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchSpecialization() throws Exception {
        int databaseSizeBeforeUpdate = specializationRepository.findAll().size();
        specialization.setId(count.incrementAndGet());

        // Create the Specialization
        SpecializationDTO specializationDTO = specializationMapper.toDto(specialization);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSpecializationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(specializationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Specialization in the database
        List<Specialization> specializationList = specializationRepository.findAll();
        assertThat(specializationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamSpecialization() throws Exception {
        int databaseSizeBeforeUpdate = specializationRepository.findAll().size();
        specialization.setId(count.incrementAndGet());

        // Create the Specialization
        SpecializationDTO specializationDTO = specializationMapper.toDto(specialization);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSpecializationMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(specializationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Specialization in the database
        List<Specialization> specializationList = specializationRepository.findAll();
        assertThat(specializationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteSpecialization() throws Exception {
        // Initialize the database
        specializationRepository.saveAndFlush(specialization);

        int databaseSizeBeforeDelete = specializationRepository.findAll().size();

        // Delete the specialization
        restSpecializationMockMvc
            .perform(delete(ENTITY_API_URL_ID, specialization.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Specialization> specializationList = specializationRepository.findAll();
        assertThat(specializationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
