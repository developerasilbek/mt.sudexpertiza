package uz.sudex.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import uz.sudex.web.rest.TestUtil;

class ModulDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModulDTO.class);
        ModulDTO modulDTO1 = new ModulDTO();
        modulDTO1.setId(1L);
        ModulDTO modulDTO2 = new ModulDTO();
        assertThat(modulDTO1).isNotEqualTo(modulDTO2);
        modulDTO2.setId(modulDTO1.getId());
        assertThat(modulDTO1).isEqualTo(modulDTO2);
        modulDTO2.setId(2L);
        assertThat(modulDTO1).isNotEqualTo(modulDTO2);
        modulDTO1.setId(null);
        assertThat(modulDTO1).isNotEqualTo(modulDTO2);
    }
}
