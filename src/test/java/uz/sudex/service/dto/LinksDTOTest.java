package uz.sudex.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import uz.sudex.web.rest.TestUtil;

class LinksDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LinksDTO.class);
        LinksDTO linksDTO1 = new LinksDTO();
        linksDTO1.setId(1L);
        LinksDTO linksDTO2 = new LinksDTO();
        assertThat(linksDTO1).isNotEqualTo(linksDTO2);
        linksDTO2.setId(linksDTO1.getId());
        assertThat(linksDTO1).isEqualTo(linksDTO2);
        linksDTO2.setId(2L);
        assertThat(linksDTO1).isNotEqualTo(linksDTO2);
        linksDTO1.setId(null);
        assertThat(linksDTO1).isNotEqualTo(linksDTO2);
    }
}
