package uz.sudex.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import uz.sudex.web.rest.TestUtil;

class ListenerDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ListenerDTO.class);
        ListenerDTO listenerDTO1 = new ListenerDTO();
        listenerDTO1.setId(1L);
        ListenerDTO listenerDTO2 = new ListenerDTO();
        assertThat(listenerDTO1).isNotEqualTo(listenerDTO2);
        listenerDTO2.setId(listenerDTO1.getId());
        assertThat(listenerDTO1).isEqualTo(listenerDTO2);
        listenerDTO2.setId(2L);
        assertThat(listenerDTO1).isNotEqualTo(listenerDTO2);
        listenerDTO1.setId(null);
        assertThat(listenerDTO1).isNotEqualTo(listenerDTO2);
    }
}
