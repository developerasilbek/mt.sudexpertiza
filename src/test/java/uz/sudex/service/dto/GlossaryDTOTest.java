package uz.sudex.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import uz.sudex.web.rest.TestUtil;

class GlossaryDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(GlossaryDTO.class);
        GlossaryDTO glossaryDTO1 = new GlossaryDTO();
        glossaryDTO1.setId(1L);
        GlossaryDTO glossaryDTO2 = new GlossaryDTO();
        assertThat(glossaryDTO1).isNotEqualTo(glossaryDTO2);
        glossaryDTO2.setId(glossaryDTO1.getId());
        assertThat(glossaryDTO1).isEqualTo(glossaryDTO2);
        glossaryDTO2.setId(2L);
        assertThat(glossaryDTO1).isNotEqualTo(glossaryDTO2);
        glossaryDTO1.setId(null);
        assertThat(glossaryDTO1).isNotEqualTo(glossaryDTO2);
    }
}
