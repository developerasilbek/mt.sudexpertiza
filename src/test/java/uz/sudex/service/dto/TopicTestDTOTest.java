package uz.sudex.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import uz.sudex.web.rest.TestUtil;

class TopicTestDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TopicTestDTO.class);
        TopicTestDTO topicTestDTO1 = new TopicTestDTO();
        topicTestDTO1.setId(1L);
        TopicTestDTO topicTestDTO2 = new TopicTestDTO();
        assertThat(topicTestDTO1).isNotEqualTo(topicTestDTO2);
        topicTestDTO2.setId(topicTestDTO1.getId());
        assertThat(topicTestDTO1).isEqualTo(topicTestDTO2);
        topicTestDTO2.setId(2L);
        assertThat(topicTestDTO1).isNotEqualTo(topicTestDTO2);
        topicTestDTO1.setId(null);
        assertThat(topicTestDTO1).isNotEqualTo(topicTestDTO2);
    }
}
