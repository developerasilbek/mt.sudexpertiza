package uz.sudex.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import uz.sudex.web.rest.TestUtil;

class ListenerTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Listener.class);
        Listener listener1 = new Listener();
        listener1.setId(1L);
        Listener listener2 = new Listener();
        listener2.setId(listener1.getId());
        assertThat(listener1).isEqualTo(listener2);
        listener2.setId(2L);
        assertThat(listener1).isNotEqualTo(listener2);
        listener1.setId(null);
        assertThat(listener1).isNotEqualTo(listener2);
    }
}
