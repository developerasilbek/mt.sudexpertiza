package uz.sudex.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import uz.sudex.web.rest.TestUtil;

class GlossaryTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Glossary.class);
        Glossary glossary1 = new Glossary();
        glossary1.setId(1L);
        Glossary glossary2 = new Glossary();
        glossary2.setId(glossary1.getId());
        assertThat(glossary1).isEqualTo(glossary2);
        glossary2.setId(2L);
        assertThat(glossary1).isNotEqualTo(glossary2);
        glossary1.setId(null);
        assertThat(glossary1).isNotEqualTo(glossary2);
    }
}
