package uz.sudex.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import uz.sudex.web.rest.TestUtil;

class ModulTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Modul.class);
        Modul modul1 = new Modul();
        modul1.setId(1L);
        Modul modul2 = new Modul();
        modul2.setId(modul1.getId());
        assertThat(modul1).isEqualTo(modul2);
        modul2.setId(2L);
        assertThat(modul1).isNotEqualTo(modul2);
        modul1.setId(null);
        assertThat(modul1).isNotEqualTo(modul2);
    }
}
