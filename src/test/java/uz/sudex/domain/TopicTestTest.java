package uz.sudex.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import uz.sudex.web.rest.TestUtil;

class TopicTestTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TopicTest.class);
        TopicTest topicTest1 = new TopicTest();
        topicTest1.setId(1L);
        TopicTest topicTest2 = new TopicTest();
        topicTest2.setId(topicTest1.getId());
        assertThat(topicTest1).isEqualTo(topicTest2);
        topicTest2.setId(2L);
        assertThat(topicTest1).isNotEqualTo(topicTest2);
        topicTest1.setId(null);
        assertThat(topicTest1).isNotEqualTo(topicTest2);
    }
}
