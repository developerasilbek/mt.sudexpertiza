import { IAssessment } from 'app/entities/assessment/assessment.model';

export interface IReferences {
  id: number;
  name?: string | null;
  assessment?: Pick<IAssessment, 'id'> | null;
}

export type NewReferences = Omit<IReferences, 'id'> & { id: null };
