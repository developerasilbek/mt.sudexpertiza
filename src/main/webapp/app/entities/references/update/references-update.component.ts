import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ReferencesFormService, ReferencesFormGroup } from './references-form.service';
import { IReferences } from '../references.model';
import { ReferencesService } from '../service/references.service';
import { IAssessment } from 'app/entities/assessment/assessment.model';
import { AssessmentService } from 'app/entities/assessment/service/assessment.service';

@Component({
  selector: 'jhi-references-update',
  templateUrl: './references-update.component.html',
})
export class ReferencesUpdateComponent implements OnInit {
  isSaving = false;
  references: IReferences | null = null;

  assessmentsSharedCollection: IAssessment[] = [];

  editForm: ReferencesFormGroup = this.referencesFormService.createReferencesFormGroup();

  constructor(
    protected referencesService: ReferencesService,
    protected referencesFormService: ReferencesFormService,
    protected assessmentService: AssessmentService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareAssessment = (o1: IAssessment | null, o2: IAssessment | null): boolean => this.assessmentService.compareAssessment(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ references }) => {
      this.references = references;
      if (references) {
        this.updateForm(references);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const references = this.referencesFormService.getReferences(this.editForm);
    if (references.id !== null) {
      this.subscribeToSaveResponse(this.referencesService.update(references));
    } else {
      this.subscribeToSaveResponse(this.referencesService.create(references));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IReferences>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(references: IReferences): void {
    this.references = references;
    this.referencesFormService.resetForm(this.editForm, references);

    this.assessmentsSharedCollection = this.assessmentService.addAssessmentToCollectionIfMissing<IAssessment>(
      this.assessmentsSharedCollection,
      references.assessment
    );
  }

  protected loadRelationshipsOptions(): void {
    this.assessmentService
      .query()
      .pipe(map((res: HttpResponse<IAssessment[]>) => res.body ?? []))
      .pipe(
        map((assessments: IAssessment[]) =>
          this.assessmentService.addAssessmentToCollectionIfMissing<IAssessment>(assessments, this.references?.assessment)
        )
      )
      .subscribe((assessments: IAssessment[]) => (this.assessmentsSharedCollection = assessments));
  }
}
