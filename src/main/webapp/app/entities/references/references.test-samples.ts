import { IReferences, NewReferences } from './references.model';

export const sampleWithRequiredData: IReferences = {
  id: 4091,
};

export const sampleWithPartialData: IReferences = {
  id: 34329,
};

export const sampleWithFullData: IReferences = {
  id: 10331,
  name: 'Front-line program',
};

export const sampleWithNewData: NewReferences = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
