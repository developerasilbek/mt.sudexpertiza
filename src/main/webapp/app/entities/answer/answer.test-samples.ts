import { IAnswer, NewAnswer } from './answer.model';

export const sampleWithRequiredData: IAnswer = {
  id: 52963,
};

export const sampleWithPartialData: IAnswer = {
  id: 1501,
};

export const sampleWithFullData: IAnswer = {
  id: 2600,
  name: 'Chicken',
  score: 56812,
  isCorrect: false,
};

export const sampleWithNewData: NewAnswer = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
