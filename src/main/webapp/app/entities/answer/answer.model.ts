import { IQuestion } from 'app/entities/question/question.model';

export interface IAnswer {
  id: number;
  name?: string | null;
  score?: number | null;
  isCorrect?: boolean | null;
  question?: Pick<IQuestion, 'id' | 'name'> | null;
}

export type NewAnswer = Omit<IAnswer, 'id'> & { id: null };
