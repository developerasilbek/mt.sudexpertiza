import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { BlockFormService, BlockFormGroup } from './block-form.service';
import { IBlock } from '../block.model';
import { BlockService } from '../service/block.service';
import { ICourse } from 'app/entities/course/course.model';
import { CourseService } from 'app/entities/course/service/course.service';
import { ISpecialization } from 'app/entities/specialization/specialization.model';
import { SpecializationService } from 'app/entities/specialization/service/specialization.service';

@Component({
  selector: 'jhi-block-update',
  templateUrl: './block-update.component.html',
})
export class BlockUpdateComponent implements OnInit {
  isSaving = false;
  block: IBlock | null = null;

  coursesSharedCollection: ICourse[] = [];
  specializationsSharedCollection: ISpecialization[] = [];

  editForm: BlockFormGroup = this.blockFormService.createBlockFormGroup();

  constructor(
    protected blockService: BlockService,
    protected blockFormService: BlockFormService,
    protected courseService: CourseService,
    protected specializationService: SpecializationService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareCourse = (o1: ICourse | null, o2: ICourse | null): boolean => this.courseService.compareCourse(o1, o2);

  compareSpecialization = (o1: ISpecialization | null, o2: ISpecialization | null): boolean =>
    this.specializationService.compareSpecialization(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ block }) => {
      this.block = block;
      if (block) {
        this.updateForm(block);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const block = this.blockFormService.getBlock(this.editForm);
    if (block.id !== null) {
      this.subscribeToSaveResponse(this.blockService.update(block));
    } else {
      this.subscribeToSaveResponse(this.blockService.create(block));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBlock>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(block: IBlock): void {
    this.block = block;
    this.blockFormService.resetForm(this.editForm, block);

    this.coursesSharedCollection = this.courseService.addCourseToCollectionIfMissing<ICourse>(this.coursesSharedCollection, block.course);
    this.specializationsSharedCollection = this.specializationService.addSpecializationToCollectionIfMissing<ISpecialization>(
      this.specializationsSharedCollection,
      block.specialization
    );
  }

  protected loadRelationshipsOptions(): void {
    this.courseService
      .query()
      .pipe(map((res: HttpResponse<ICourse[]>) => res.body ?? []))
      .pipe(map((courses: ICourse[]) => this.courseService.addCourseToCollectionIfMissing<ICourse>(courses, this.block?.course)))
      .subscribe((courses: ICourse[]) => (this.coursesSharedCollection = courses));

    this.specializationService
      .query()
      .pipe(map((res: HttpResponse<ISpecialization[]>) => res.body ?? []))
      .pipe(
        map((specializations: ISpecialization[]) =>
          this.specializationService.addSpecializationToCollectionIfMissing<ISpecialization>(specializations, this.block?.specialization)
        )
      )
      .subscribe((specializations: ISpecialization[]) => (this.specializationsSharedCollection = specializations));
  }
}
