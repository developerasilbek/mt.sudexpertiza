import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { BlockFormService } from './block-form.service';
import { BlockService } from '../service/block.service';
import { IBlock } from '../block.model';
import { ICourse } from 'app/entities/course/course.model';
import { CourseService } from 'app/entities/course/service/course.service';
import { ISpecialization } from 'app/entities/specialization/specialization.model';
import { SpecializationService } from 'app/entities/specialization/service/specialization.service';

import { BlockUpdateComponent } from './block-update.component';

describe('Block Management Update Component', () => {
  let comp: BlockUpdateComponent;
  let fixture: ComponentFixture<BlockUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let blockFormService: BlockFormService;
  let blockService: BlockService;
  let courseService: CourseService;
  let specializationService: SpecializationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [BlockUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(BlockUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(BlockUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    blockFormService = TestBed.inject(BlockFormService);
    blockService = TestBed.inject(BlockService);
    courseService = TestBed.inject(CourseService);
    specializationService = TestBed.inject(SpecializationService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Course query and add missing value', () => {
      const block: IBlock = { id: 456 };
      const course: ICourse = { id: 40036 };
      block.course = course;

      const courseCollection: ICourse[] = [{ id: 1158 }];
      jest.spyOn(courseService, 'query').mockReturnValue(of(new HttpResponse({ body: courseCollection })));
      const additionalCourses = [course];
      const expectedCollection: ICourse[] = [...additionalCourses, ...courseCollection];
      jest.spyOn(courseService, 'addCourseToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ block });
      comp.ngOnInit();

      expect(courseService.query).toHaveBeenCalled();
      expect(courseService.addCourseToCollectionIfMissing).toHaveBeenCalledWith(
        courseCollection,
        ...additionalCourses.map(expect.objectContaining)
      );
      expect(comp.coursesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Specialization query and add missing value', () => {
      const block: IBlock = { id: 456 };
      const specialization: ISpecialization = { id: 16048 };
      block.specialization = specialization;

      const specializationCollection: ISpecialization[] = [{ id: 28505 }];
      jest.spyOn(specializationService, 'query').mockReturnValue(of(new HttpResponse({ body: specializationCollection })));
      const additionalSpecializations = [specialization];
      const expectedCollection: ISpecialization[] = [...additionalSpecializations, ...specializationCollection];
      jest.spyOn(specializationService, 'addSpecializationToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ block });
      comp.ngOnInit();

      expect(specializationService.query).toHaveBeenCalled();
      expect(specializationService.addSpecializationToCollectionIfMissing).toHaveBeenCalledWith(
        specializationCollection,
        ...additionalSpecializations.map(expect.objectContaining)
      );
      expect(comp.specializationsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const block: IBlock = { id: 456 };
      const course: ICourse = { id: 50346 };
      block.course = course;
      const specialization: ISpecialization = { id: 92151 };
      block.specialization = specialization;

      activatedRoute.data = of({ block });
      comp.ngOnInit();

      expect(comp.coursesSharedCollection).toContain(course);
      expect(comp.specializationsSharedCollection).toContain(specialization);
      expect(comp.block).toEqual(block);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IBlock>>();
      const block = { id: 123 };
      jest.spyOn(blockFormService, 'getBlock').mockReturnValue(block);
      jest.spyOn(blockService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ block });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: block }));
      saveSubject.complete();

      // THEN
      expect(blockFormService.getBlock).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(blockService.update).toHaveBeenCalledWith(expect.objectContaining(block));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IBlock>>();
      const block = { id: 123 };
      jest.spyOn(blockFormService, 'getBlock').mockReturnValue({ id: null });
      jest.spyOn(blockService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ block: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: block }));
      saveSubject.complete();

      // THEN
      expect(blockFormService.getBlock).toHaveBeenCalled();
      expect(blockService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IBlock>>();
      const block = { id: 123 };
      jest.spyOn(blockService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ block });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(blockService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareCourse', () => {
      it('Should forward to courseService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(courseService, 'compareCourse');
        comp.compareCourse(entity, entity2);
        expect(courseService.compareCourse).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareSpecialization', () => {
      it('Should forward to specializationService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(specializationService, 'compareSpecialization');
        comp.compareSpecialization(entity, entity2);
        expect(specializationService.compareSpecialization).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
