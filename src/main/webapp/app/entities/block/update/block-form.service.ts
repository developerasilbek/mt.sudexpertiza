import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IBlock, NewBlock } from '../block.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IBlock for edit and NewBlockFormGroupInput for create.
 */
type BlockFormGroupInput = IBlock | PartialWithRequiredKeyOf<NewBlock>;

type BlockFormDefaults = Pick<NewBlock, 'id'>;

type BlockFormGroupContent = {
  id: FormControl<IBlock['id'] | NewBlock['id']>;
  name: FormControl<IBlock['name']>;
  course: FormControl<IBlock['course']>;
  specialization: FormControl<IBlock['specialization']>;
};

export type BlockFormGroup = FormGroup<BlockFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class BlockFormService {
  createBlockFormGroup(block: BlockFormGroupInput = { id: null }): BlockFormGroup {
    const blockRawValue = {
      ...this.getFormDefaults(),
      ...block,
    };
    return new FormGroup<BlockFormGroupContent>({
      id: new FormControl(
        { value: blockRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      name: new FormControl(blockRawValue.name),
      course: new FormControl(blockRawValue.course),
      specialization: new FormControl(blockRawValue.specialization),
    });
  }

  getBlock(form: BlockFormGroup): IBlock | NewBlock {
    return form.getRawValue() as IBlock | NewBlock;
  }

  resetForm(form: BlockFormGroup, block: BlockFormGroupInput): void {
    const blockRawValue = { ...this.getFormDefaults(), ...block };
    form.reset(
      {
        ...blockRawValue,
        id: { value: blockRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): BlockFormDefaults {
    return {
      id: null,
    };
  }
}
