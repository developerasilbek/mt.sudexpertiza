import { ICourse } from 'app/entities/course/course.model';
import { ISpecialization } from 'app/entities/specialization/specialization.model';

export interface IBlock {
  id: number;
  name?: string | null;
  course?: Pick<ICourse, 'id'> | null;
  specialization?: Pick<ISpecialization, 'id'> | null;
}

export type NewBlock = Omit<IBlock, 'id'> & { id: null };
