import { IBlock, NewBlock } from './block.model';

export const sampleWithRequiredData: IBlock = {
  id: 80619,
};

export const sampleWithPartialData: IBlock = {
  id: 52010,
  name: 'Territories index knowledge',
};

export const sampleWithFullData: IBlock = {
  id: 77098,
  name: 'action-items',
};

export const sampleWithNewData: NewBlock = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
