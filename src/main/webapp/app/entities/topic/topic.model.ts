import { IModul } from 'app/entities/modul/modul.model';

export interface ITopic {
  id: number;
  name?: string | null;
  priority?: number | null;
  hour?: number | null;
  modul?: Pick<IModul, 'id' | 'name'> | null;
}

export type NewTopic = Omit<ITopic, 'id'> & { id: null };
