import { ITopic, NewTopic } from './topic.model';

export const sampleWithRequiredData: ITopic = {
  id: 51387,
};

export const sampleWithPartialData: ITopic = {
  id: 15056,
  name: 'payment fuchsia Loan',
  hour: 51346,
};

export const sampleWithFullData: ITopic = {
  id: 72515,
  name: 'programming',
  priority: 27724,
  hour: 89328,
};

export const sampleWithNewData: NewTopic = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
