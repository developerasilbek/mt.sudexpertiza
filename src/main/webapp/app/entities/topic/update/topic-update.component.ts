import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { TopicFormService, TopicFormGroup } from './topic-form.service';
import { ITopic } from '../topic.model';
import { TopicService } from '../service/topic.service';
import { IModul } from 'app/entities/modul/modul.model';
import { ModulService } from 'app/entities/modul/service/modul.service';

@Component({
  selector: 'jhi-topic-update',
  templateUrl: './topic-update.component.html',
})
export class TopicUpdateComponent implements OnInit {
  isSaving = false;
  topic: ITopic | null = null;

  modulsSharedCollection: IModul[] = [];

  editForm: TopicFormGroup = this.topicFormService.createTopicFormGroup();

  constructor(
    protected topicService: TopicService,
    protected topicFormService: TopicFormService,
    protected modulService: ModulService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareModul = (o1: IModul | null, o2: IModul | null): boolean => this.modulService.compareModul(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ topic }) => {
      this.topic = topic;
      if (topic) {
        this.updateForm(topic);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const topic = this.topicFormService.getTopic(this.editForm);
    if (topic.id !== null) {
      this.subscribeToSaveResponse(this.topicService.update(topic));
    } else {
      this.subscribeToSaveResponse(this.topicService.create(topic));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITopic>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(topic: ITopic): void {
    this.topic = topic;
    this.topicFormService.resetForm(this.editForm, topic);

    this.modulsSharedCollection = this.modulService.addModulToCollectionIfMissing<IModul>(this.modulsSharedCollection, topic.modul);
  }

  protected loadRelationshipsOptions(): void {
    this.modulService
      .query()
      .pipe(map((res: HttpResponse<IModul[]>) => res.body ?? []))
      .pipe(map((moduls: IModul[]) => this.modulService.addModulToCollectionIfMissing<IModul>(moduls, this.topic?.modul)))
      .subscribe((moduls: IModul[]) => (this.modulsSharedCollection = moduls));
  }
}
