import { ITopic } from 'app/entities/topic/topic.model';

export interface IAttachment {
  id: number;
  name?: string | null;
  fileOriginalName?: string | null;
  fileSize?: number | null;
  contentType?: string | null;
  topic?: Pick<ITopic, 'id'> | null;
}

export type NewAttachment = Omit<IAttachment, 'id'> & { id: null };
