import { IAttachment, NewAttachment } from './attachment.model';

export const sampleWithRequiredData: IAttachment = {
  id: 43624,
};

export const sampleWithPartialData: IAttachment = {
  id: 60095,
  fileOriginalName: 'Intranet',
  fileSize: 76947,
  contentType: 'RSS',
};

export const sampleWithFullData: IAttachment = {
  id: 63486,
  name: 'Rue',
  fileOriginalName: 'payment',
  fileSize: 43065,
  contentType: 'Meadow Programmable Sausages',
};

export const sampleWithNewData: NewAttachment = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
