import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { TestResultFormService } from './test-result-form.service';
import { TestResultService } from '../service/test-result.service';
import { ITestResult } from '../test-result.model';
import { ITopicTest } from 'app/entities/topic-test/topic-test.model';
import { TopicTestService } from 'app/entities/topic-test/service/topic-test.service';

import { TestResultUpdateComponent } from './test-result-update.component';

describe('TestResult Management Update Component', () => {
  let comp: TestResultUpdateComponent;
  let fixture: ComponentFixture<TestResultUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let testResultFormService: TestResultFormService;
  let testResultService: TestResultService;
  let topicTestService: TopicTestService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [TestResultUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(TestResultUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(TestResultUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    testResultFormService = TestBed.inject(TestResultFormService);
    testResultService = TestBed.inject(TestResultService);
    topicTestService = TestBed.inject(TopicTestService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call TopicTest query and add missing value', () => {
      const testResult: ITestResult = { id: 456 };
      const test: ITopicTest = { id: 13115 };
      testResult.test = test;

      const topicTestCollection: ITopicTest[] = [{ id: 43313 }];
      jest.spyOn(topicTestService, 'query').mockReturnValue(of(new HttpResponse({ body: topicTestCollection })));
      const additionalTopicTests = [test];
      const expectedCollection: ITopicTest[] = [...additionalTopicTests, ...topicTestCollection];
      jest.spyOn(topicTestService, 'addTopicTestToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ testResult });
      comp.ngOnInit();

      expect(topicTestService.query).toHaveBeenCalled();
      expect(topicTestService.addTopicTestToCollectionIfMissing).toHaveBeenCalledWith(
        topicTestCollection,
        ...additionalTopicTests.map(expect.objectContaining)
      );
      expect(comp.topicTestsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const testResult: ITestResult = { id: 456 };
      const test: ITopicTest = { id: 18206 };
      testResult.test = test;

      activatedRoute.data = of({ testResult });
      comp.ngOnInit();

      expect(comp.topicTestsSharedCollection).toContain(test);
      expect(comp.testResult).toEqual(testResult);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ITestResult>>();
      const testResult = { id: 123 };
      jest.spyOn(testResultFormService, 'getTestResult').mockReturnValue(testResult);
      jest.spyOn(testResultService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ testResult });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: testResult }));
      saveSubject.complete();

      // THEN
      expect(testResultFormService.getTestResult).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(testResultService.update).toHaveBeenCalledWith(expect.objectContaining(testResult));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ITestResult>>();
      const testResult = { id: 123 };
      jest.spyOn(testResultFormService, 'getTestResult').mockReturnValue({ id: null });
      jest.spyOn(testResultService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ testResult: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: testResult }));
      saveSubject.complete();

      // THEN
      expect(testResultFormService.getTestResult).toHaveBeenCalled();
      expect(testResultService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ITestResult>>();
      const testResult = { id: 123 };
      jest.spyOn(testResultService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ testResult });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(testResultService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareTopicTest', () => {
      it('Should forward to topicTestService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(topicTestService, 'compareTopicTest');
        comp.compareTopicTest(entity, entity2);
        expect(topicTestService.compareTopicTest).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
