import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { TestResultFormService, TestResultFormGroup } from './test-result-form.service';
import { ITestResult } from '../test-result.model';
import { TestResultService } from '../service/test-result.service';
import { ITopicTest } from 'app/entities/topic-test/topic-test.model';
import { TopicTestService } from 'app/entities/topic-test/service/topic-test.service';

@Component({
  selector: 'jhi-test-result-update',
  templateUrl: './test-result-update.component.html',
})
export class TestResultUpdateComponent implements OnInit {
  isSaving = false;
  testResult: ITestResult | null = null;

  topicTestsSharedCollection: ITopicTest[] = [];

  editForm: TestResultFormGroup = this.testResultFormService.createTestResultFormGroup();

  constructor(
    protected testResultService: TestResultService,
    protected testResultFormService: TestResultFormService,
    protected topicTestService: TopicTestService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareTopicTest = (o1: ITopicTest | null, o2: ITopicTest | null): boolean => this.topicTestService.compareTopicTest(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ testResult }) => {
      this.testResult = testResult;
      if (testResult) {
        this.updateForm(testResult);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const testResult = this.testResultFormService.getTestResult(this.editForm);
    if (testResult.id !== null) {
      this.subscribeToSaveResponse(this.testResultService.update(testResult));
    } else {
      this.subscribeToSaveResponse(this.testResultService.create(testResult));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITestResult>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(testResult: ITestResult): void {
    this.testResult = testResult;
    this.testResultFormService.resetForm(this.editForm, testResult);

    this.topicTestsSharedCollection = this.topicTestService.addTopicTestToCollectionIfMissing<ITopicTest>(
      this.topicTestsSharedCollection,
      testResult.test
    );
  }

  protected loadRelationshipsOptions(): void {
    this.topicTestService
      .query()
      .pipe(map((res: HttpResponse<ITopicTest[]>) => res.body ?? []))
      .pipe(
        map((topicTests: ITopicTest[]) =>
          this.topicTestService.addTopicTestToCollectionIfMissing<ITopicTest>(topicTests, this.testResult?.test)
        )
      )
      .subscribe((topicTests: ITopicTest[]) => (this.topicTestsSharedCollection = topicTests));
  }
}
