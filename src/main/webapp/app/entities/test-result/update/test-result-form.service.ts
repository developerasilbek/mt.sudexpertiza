import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { ITestResult, NewTestResult } from '../test-result.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts ITestResult for edit and NewTestResultFormGroupInput for create.
 */
type TestResultFormGroupInput = ITestResult | PartialWithRequiredKeyOf<NewTestResult>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends ITestResult | NewTestResult> = Omit<T, 'startedAt' | 'finishedAt'> & {
  startedAt?: string | null;
  finishedAt?: string | null;
};

type TestResultFormRawValue = FormValueOf<ITestResult>;

type NewTestResultFormRawValue = FormValueOf<NewTestResult>;

type TestResultFormDefaults = Pick<NewTestResult, 'id' | 'startedAt' | 'finishedAt'>;

type TestResultFormGroupContent = {
  id: FormControl<TestResultFormRawValue['id'] | NewTestResult['id']>;
  startedAt: FormControl<TestResultFormRawValue['startedAt']>;
  finishedAt: FormControl<TestResultFormRawValue['finishedAt']>;
  score: FormControl<TestResultFormRawValue['score']>;
  correctAnswers: FormControl<TestResultFormRawValue['correctAnswers']>;
  test: FormControl<TestResultFormRawValue['test']>;
};

export type TestResultFormGroup = FormGroup<TestResultFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class TestResultFormService {
  createTestResultFormGroup(testResult: TestResultFormGroupInput = { id: null }): TestResultFormGroup {
    const testResultRawValue = this.convertTestResultToTestResultRawValue({
      ...this.getFormDefaults(),
      ...testResult,
    });
    return new FormGroup<TestResultFormGroupContent>({
      id: new FormControl(
        { value: testResultRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      startedAt: new FormControl(testResultRawValue.startedAt),
      finishedAt: new FormControl(testResultRawValue.finishedAt),
      score: new FormControl(testResultRawValue.score),
      correctAnswers: new FormControl(testResultRawValue.correctAnswers),
      test: new FormControl(testResultRawValue.test),
    });
  }

  getTestResult(form: TestResultFormGroup): ITestResult | NewTestResult {
    return this.convertTestResultRawValueToTestResult(form.getRawValue() as TestResultFormRawValue | NewTestResultFormRawValue);
  }

  resetForm(form: TestResultFormGroup, testResult: TestResultFormGroupInput): void {
    const testResultRawValue = this.convertTestResultToTestResultRawValue({ ...this.getFormDefaults(), ...testResult });
    form.reset(
      {
        ...testResultRawValue,
        id: { value: testResultRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): TestResultFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      startedAt: currentTime,
      finishedAt: currentTime,
    };
  }

  private convertTestResultRawValueToTestResult(
    rawTestResult: TestResultFormRawValue | NewTestResultFormRawValue
  ): ITestResult | NewTestResult {
    return {
      ...rawTestResult,
      startedAt: dayjs(rawTestResult.startedAt, DATE_TIME_FORMAT),
      finishedAt: dayjs(rawTestResult.finishedAt, DATE_TIME_FORMAT),
    };
  }

  private convertTestResultToTestResultRawValue(
    testResult: ITestResult | (Partial<NewTestResult> & TestResultFormDefaults)
  ): TestResultFormRawValue | PartialWithRequiredKeyOf<NewTestResultFormRawValue> {
    return {
      ...testResult,
      startedAt: testResult.startedAt ? testResult.startedAt.format(DATE_TIME_FORMAT) : undefined,
      finishedAt: testResult.finishedAt ? testResult.finishedAt.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
