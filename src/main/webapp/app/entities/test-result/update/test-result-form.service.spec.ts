import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../test-result.test-samples';

import { TestResultFormService } from './test-result-form.service';

describe('TestResult Form Service', () => {
  let service: TestResultFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestResultFormService);
  });

  describe('Service methods', () => {
    describe('createTestResultFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createTestResultFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            startedAt: expect.any(Object),
            finishedAt: expect.any(Object),
            score: expect.any(Object),
            correctAnswers: expect.any(Object),
            test: expect.any(Object),
          })
        );
      });

      it('passing ITestResult should create a new form with FormGroup', () => {
        const formGroup = service.createTestResultFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            startedAt: expect.any(Object),
            finishedAt: expect.any(Object),
            score: expect.any(Object),
            correctAnswers: expect.any(Object),
            test: expect.any(Object),
          })
        );
      });
    });

    describe('getTestResult', () => {
      it('should return NewTestResult for default TestResult initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createTestResultFormGroup(sampleWithNewData);

        const testResult = service.getTestResult(formGroup) as any;

        expect(testResult).toMatchObject(sampleWithNewData);
      });

      it('should return NewTestResult for empty TestResult initial value', () => {
        const formGroup = service.createTestResultFormGroup();

        const testResult = service.getTestResult(formGroup) as any;

        expect(testResult).toMatchObject({});
      });

      it('should return ITestResult', () => {
        const formGroup = service.createTestResultFormGroup(sampleWithRequiredData);

        const testResult = service.getTestResult(formGroup) as any;

        expect(testResult).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing ITestResult should not enable id FormControl', () => {
        const formGroup = service.createTestResultFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewTestResult should disable id FormControl', () => {
        const formGroup = service.createTestResultFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
