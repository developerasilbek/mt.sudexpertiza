import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ITestResult } from '../test-result.model';
import { TestResultService } from '../service/test-result.service';

@Injectable({ providedIn: 'root' })
export class TestResultRoutingResolveService implements Resolve<ITestResult | null> {
  constructor(protected service: TestResultService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITestResult | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((testResult: HttpResponse<ITestResult>) => {
          if (testResult.body) {
            return of(testResult.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
