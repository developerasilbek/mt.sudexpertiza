import dayjs from 'dayjs/esm';

import { ITestResult, NewTestResult } from './test-result.model';

export const sampleWithRequiredData: ITestResult = {
  id: 10469,
};

export const sampleWithPartialData: ITestResult = {
  id: 66142,
  startedAt: dayjs('2023-04-17T09:21'),
};

export const sampleWithFullData: ITestResult = {
  id: 55251,
  startedAt: dayjs('2023-04-16T23:37'),
  finishedAt: dayjs('2023-04-17T04:10'),
  score: 6998,
  correctAnswers: 46926,
};

export const sampleWithNewData: NewTestResult = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
