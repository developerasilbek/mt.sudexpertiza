import dayjs from 'dayjs/esm';
import { ITopicTest } from 'app/entities/topic-test/topic-test.model';

export interface ITestResult {
  id: number;
  startedAt?: dayjs.Dayjs | null;
  finishedAt?: dayjs.Dayjs | null;
  score?: number | null;
  correctAnswers?: number | null;
  test?: Pick<ITopicTest, 'id'> | null;
}

export type NewTestResult = Omit<ITestResult, 'id'> & { id: null };
