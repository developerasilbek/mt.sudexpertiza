import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ITestResult, NewTestResult } from '../test-result.model';

export type PartialUpdateTestResult = Partial<ITestResult> & Pick<ITestResult, 'id'>;

type RestOf<T extends ITestResult | NewTestResult> = Omit<T, 'startedAt' | 'finishedAt'> & {
  startedAt?: string | null;
  finishedAt?: string | null;
};

export type RestTestResult = RestOf<ITestResult>;

export type NewRestTestResult = RestOf<NewTestResult>;

export type PartialUpdateRestTestResult = RestOf<PartialUpdateTestResult>;

export type EntityResponseType = HttpResponse<ITestResult>;
export type EntityArrayResponseType = HttpResponse<ITestResult[]>;

@Injectable({ providedIn: 'root' })
export class TestResultService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/test-results');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(testResult: NewTestResult): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(testResult);
    return this.http
      .post<RestTestResult>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(testResult: ITestResult): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(testResult);
    return this.http
      .put<RestTestResult>(`${this.resourceUrl}/${this.getTestResultIdentifier(testResult)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(testResult: PartialUpdateTestResult): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(testResult);
    return this.http
      .patch<RestTestResult>(`${this.resourceUrl}/${this.getTestResultIdentifier(testResult)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestTestResult>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestTestResult[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getTestResultIdentifier(testResult: Pick<ITestResult, 'id'>): number {
    return testResult.id;
  }

  compareTestResult(o1: Pick<ITestResult, 'id'> | null, o2: Pick<ITestResult, 'id'> | null): boolean {
    return o1 && o2 ? this.getTestResultIdentifier(o1) === this.getTestResultIdentifier(o2) : o1 === o2;
  }

  addTestResultToCollectionIfMissing<Type extends Pick<ITestResult, 'id'>>(
    testResultCollection: Type[],
    ...testResultsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const testResults: Type[] = testResultsToCheck.filter(isPresent);
    if (testResults.length > 0) {
      const testResultCollectionIdentifiers = testResultCollection.map(testResultItem => this.getTestResultIdentifier(testResultItem)!);
      const testResultsToAdd = testResults.filter(testResultItem => {
        const testResultIdentifier = this.getTestResultIdentifier(testResultItem);
        if (testResultCollectionIdentifiers.includes(testResultIdentifier)) {
          return false;
        }
        testResultCollectionIdentifiers.push(testResultIdentifier);
        return true;
      });
      return [...testResultsToAdd, ...testResultCollection];
    }
    return testResultCollection;
  }

  protected convertDateFromClient<T extends ITestResult | NewTestResult | PartialUpdateTestResult>(testResult: T): RestOf<T> {
    return {
      ...testResult,
      startedAt: testResult.startedAt?.toJSON() ?? null,
      finishedAt: testResult.finishedAt?.toJSON() ?? null,
    };
  }

  protected convertDateFromServer(restTestResult: RestTestResult): ITestResult {
    return {
      ...restTestResult,
      startedAt: restTestResult.startedAt ? dayjs(restTestResult.startedAt) : undefined,
      finishedAt: restTestResult.finishedAt ? dayjs(restTestResult.finishedAt) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestTestResult>): HttpResponse<ITestResult> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestTestResult[]>): HttpResponse<ITestResult[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
