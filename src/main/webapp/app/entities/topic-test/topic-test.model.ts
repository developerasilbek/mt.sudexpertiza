import { IAssessment } from 'app/entities/assessment/assessment.model';

export interface ITopicTest {
  id: number;
  name?: string | null;
  assessment?: Pick<IAssessment, 'id'> | null;
}

export type NewTopicTest = Omit<ITopicTest, 'id'> & { id: null };
