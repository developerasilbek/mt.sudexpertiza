import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ITopicTest } from '../topic-test.model';
import { TopicTestService } from '../service/topic-test.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './topic-test-delete-dialog.component.html',
})
export class TopicTestDeleteDialogComponent {
  topicTest?: ITopicTest;

  constructor(protected topicTestService: TopicTestService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.topicTestService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
