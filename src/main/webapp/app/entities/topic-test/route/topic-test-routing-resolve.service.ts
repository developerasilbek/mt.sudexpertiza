import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ITopicTest } from '../topic-test.model';
import { TopicTestService } from '../service/topic-test.service';

@Injectable({ providedIn: 'root' })
export class TopicTestRoutingResolveService implements Resolve<ITopicTest | null> {
  constructor(protected service: TopicTestService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITopicTest | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((topicTest: HttpResponse<ITopicTest>) => {
          if (topicTest.body) {
            return of(topicTest.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
