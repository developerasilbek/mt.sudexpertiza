import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { TopicTestComponent } from '../list/topic-test.component';
import { TopicTestDetailComponent } from '../detail/topic-test-detail.component';
import { TopicTestUpdateComponent } from '../update/topic-test-update.component';
import { TopicTestRoutingResolveService } from './topic-test-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const topicTestRoute: Routes = [
  {
    path: '',
    component: TopicTestComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TopicTestDetailComponent,
    resolve: {
      topicTest: TopicTestRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TopicTestUpdateComponent,
    resolve: {
      topicTest: TopicTestRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TopicTestUpdateComponent,
    resolve: {
      topicTest: TopicTestRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(topicTestRoute)],
  exports: [RouterModule],
})
export class TopicTestRoutingModule {}
