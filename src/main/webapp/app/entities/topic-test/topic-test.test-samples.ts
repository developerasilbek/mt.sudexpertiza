import { ITopicTest, NewTopicTest } from './topic-test.model';

export const sampleWithRequiredData: ITopicTest = {
  id: 47485,
};

export const sampleWithPartialData: ITopicTest = {
  id: 87574,
};

export const sampleWithFullData: ITopicTest = {
  id: 11940,
  name: 'Berkshire Administrator',
};

export const sampleWithNewData: NewTopicTest = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
