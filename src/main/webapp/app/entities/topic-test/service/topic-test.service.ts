import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ITopicTest, NewTopicTest } from '../topic-test.model';

export type PartialUpdateTopicTest = Partial<ITopicTest> & Pick<ITopicTest, 'id'>;

export type EntityResponseType = HttpResponse<ITopicTest>;
export type EntityArrayResponseType = HttpResponse<ITopicTest[]>;

@Injectable({ providedIn: 'root' })
export class TopicTestService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/topic-tests');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(topicTest: NewTopicTest): Observable<EntityResponseType> {
    return this.http.post<ITopicTest>(this.resourceUrl, topicTest, { observe: 'response' });
  }

  update(topicTest: ITopicTest): Observable<EntityResponseType> {
    return this.http.put<ITopicTest>(`${this.resourceUrl}/${this.getTopicTestIdentifier(topicTest)}`, topicTest, { observe: 'response' });
  }

  partialUpdate(topicTest: PartialUpdateTopicTest): Observable<EntityResponseType> {
    return this.http.patch<ITopicTest>(`${this.resourceUrl}/${this.getTopicTestIdentifier(topicTest)}`, topicTest, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITopicTest>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITopicTest[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getTopicTestIdentifier(topicTest: Pick<ITopicTest, 'id'>): number {
    return topicTest.id;
  }

  compareTopicTest(o1: Pick<ITopicTest, 'id'> | null, o2: Pick<ITopicTest, 'id'> | null): boolean {
    return o1 && o2 ? this.getTopicTestIdentifier(o1) === this.getTopicTestIdentifier(o2) : o1 === o2;
  }

  addTopicTestToCollectionIfMissing<Type extends Pick<ITopicTest, 'id'>>(
    topicTestCollection: Type[],
    ...topicTestsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const topicTests: Type[] = topicTestsToCheck.filter(isPresent);
    if (topicTests.length > 0) {
      const topicTestCollectionIdentifiers = topicTestCollection.map(topicTestItem => this.getTopicTestIdentifier(topicTestItem)!);
      const topicTestsToAdd = topicTests.filter(topicTestItem => {
        const topicTestIdentifier = this.getTopicTestIdentifier(topicTestItem);
        if (topicTestCollectionIdentifiers.includes(topicTestIdentifier)) {
          return false;
        }
        topicTestCollectionIdentifiers.push(topicTestIdentifier);
        return true;
      });
      return [...topicTestsToAdd, ...topicTestCollection];
    }
    return topicTestCollection;
  }
}
