import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ITopicTest } from '../topic-test.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../topic-test.test-samples';

import { TopicTestService } from './topic-test.service';

const requireRestSample: ITopicTest = {
  ...sampleWithRequiredData,
};

describe('TopicTest Service', () => {
  let service: TopicTestService;
  let httpMock: HttpTestingController;
  let expectedResult: ITopicTest | ITopicTest[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(TopicTestService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a TopicTest', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const topicTest = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(topicTest).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a TopicTest', () => {
      const topicTest = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(topicTest).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a TopicTest', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of TopicTest', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a TopicTest', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addTopicTestToCollectionIfMissing', () => {
      it('should add a TopicTest to an empty array', () => {
        const topicTest: ITopicTest = sampleWithRequiredData;
        expectedResult = service.addTopicTestToCollectionIfMissing([], topicTest);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(topicTest);
      });

      it('should not add a TopicTest to an array that contains it', () => {
        const topicTest: ITopicTest = sampleWithRequiredData;
        const topicTestCollection: ITopicTest[] = [
          {
            ...topicTest,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addTopicTestToCollectionIfMissing(topicTestCollection, topicTest);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a TopicTest to an array that doesn't contain it", () => {
        const topicTest: ITopicTest = sampleWithRequiredData;
        const topicTestCollection: ITopicTest[] = [sampleWithPartialData];
        expectedResult = service.addTopicTestToCollectionIfMissing(topicTestCollection, topicTest);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(topicTest);
      });

      it('should add only unique TopicTest to an array', () => {
        const topicTestArray: ITopicTest[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const topicTestCollection: ITopicTest[] = [sampleWithRequiredData];
        expectedResult = service.addTopicTestToCollectionIfMissing(topicTestCollection, ...topicTestArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const topicTest: ITopicTest = sampleWithRequiredData;
        const topicTest2: ITopicTest = sampleWithPartialData;
        expectedResult = service.addTopicTestToCollectionIfMissing([], topicTest, topicTest2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(topicTest);
        expect(expectedResult).toContain(topicTest2);
      });

      it('should accept null and undefined values', () => {
        const topicTest: ITopicTest = sampleWithRequiredData;
        expectedResult = service.addTopicTestToCollectionIfMissing([], null, topicTest, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(topicTest);
      });

      it('should return initial array if no TopicTest is added', () => {
        const topicTestCollection: ITopicTest[] = [sampleWithRequiredData];
        expectedResult = service.addTopicTestToCollectionIfMissing(topicTestCollection, undefined, null);
        expect(expectedResult).toEqual(topicTestCollection);
      });
    });

    describe('compareTopicTest', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareTopicTest(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareTopicTest(entity1, entity2);
        const compareResult2 = service.compareTopicTest(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareTopicTest(entity1, entity2);
        const compareResult2 = service.compareTopicTest(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareTopicTest(entity1, entity2);
        const compareResult2 = service.compareTopicTest(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
