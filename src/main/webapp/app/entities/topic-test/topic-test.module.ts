import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { TopicTestComponent } from './list/topic-test.component';
import { TopicTestDetailComponent } from './detail/topic-test-detail.component';
import { TopicTestUpdateComponent } from './update/topic-test-update.component';
import { TopicTestDeleteDialogComponent } from './delete/topic-test-delete-dialog.component';
import { TopicTestRoutingModule } from './route/topic-test-routing.module';

@NgModule({
  imports: [SharedModule, TopicTestRoutingModule],
  declarations: [TopicTestComponent, TopicTestDetailComponent, TopicTestUpdateComponent, TopicTestDeleteDialogComponent],
})
export class TopicTestModule {}
