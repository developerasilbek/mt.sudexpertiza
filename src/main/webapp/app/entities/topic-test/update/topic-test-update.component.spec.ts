import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { TopicTestFormService } from './topic-test-form.service';
import { TopicTestService } from '../service/topic-test.service';
import { ITopicTest } from '../topic-test.model';
import { IAssessment } from 'app/entities/assessment/assessment.model';
import { AssessmentService } from 'app/entities/assessment/service/assessment.service';

import { TopicTestUpdateComponent } from './topic-test-update.component';

describe('TopicTest Management Update Component', () => {
  let comp: TopicTestUpdateComponent;
  let fixture: ComponentFixture<TopicTestUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let topicTestFormService: TopicTestFormService;
  let topicTestService: TopicTestService;
  let assessmentService: AssessmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [TopicTestUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(TopicTestUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(TopicTestUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    topicTestFormService = TestBed.inject(TopicTestFormService);
    topicTestService = TestBed.inject(TopicTestService);
    assessmentService = TestBed.inject(AssessmentService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Assessment query and add missing value', () => {
      const topicTest: ITopicTest = { id: 456 };
      const assessment: IAssessment = { id: 7211 };
      topicTest.assessment = assessment;

      const assessmentCollection: IAssessment[] = [{ id: 60541 }];
      jest.spyOn(assessmentService, 'query').mockReturnValue(of(new HttpResponse({ body: assessmentCollection })));
      const additionalAssessments = [assessment];
      const expectedCollection: IAssessment[] = [...additionalAssessments, ...assessmentCollection];
      jest.spyOn(assessmentService, 'addAssessmentToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ topicTest });
      comp.ngOnInit();

      expect(assessmentService.query).toHaveBeenCalled();
      expect(assessmentService.addAssessmentToCollectionIfMissing).toHaveBeenCalledWith(
        assessmentCollection,
        ...additionalAssessments.map(expect.objectContaining)
      );
      expect(comp.assessmentsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const topicTest: ITopicTest = { id: 456 };
      const assessment: IAssessment = { id: 9755 };
      topicTest.assessment = assessment;

      activatedRoute.data = of({ topicTest });
      comp.ngOnInit();

      expect(comp.assessmentsSharedCollection).toContain(assessment);
      expect(comp.topicTest).toEqual(topicTest);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ITopicTest>>();
      const topicTest = { id: 123 };
      jest.spyOn(topicTestFormService, 'getTopicTest').mockReturnValue(topicTest);
      jest.spyOn(topicTestService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ topicTest });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: topicTest }));
      saveSubject.complete();

      // THEN
      expect(topicTestFormService.getTopicTest).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(topicTestService.update).toHaveBeenCalledWith(expect.objectContaining(topicTest));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ITopicTest>>();
      const topicTest = { id: 123 };
      jest.spyOn(topicTestFormService, 'getTopicTest').mockReturnValue({ id: null });
      jest.spyOn(topicTestService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ topicTest: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: topicTest }));
      saveSubject.complete();

      // THEN
      expect(topicTestFormService.getTopicTest).toHaveBeenCalled();
      expect(topicTestService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ITopicTest>>();
      const topicTest = { id: 123 };
      jest.spyOn(topicTestService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ topicTest });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(topicTestService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareAssessment', () => {
      it('Should forward to assessmentService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(assessmentService, 'compareAssessment');
        comp.compareAssessment(entity, entity2);
        expect(assessmentService.compareAssessment).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
