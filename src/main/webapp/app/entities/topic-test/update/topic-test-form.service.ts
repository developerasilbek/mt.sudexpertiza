import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ITopicTest, NewTopicTest } from '../topic-test.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts ITopicTest for edit and NewTopicTestFormGroupInput for create.
 */
type TopicTestFormGroupInput = ITopicTest | PartialWithRequiredKeyOf<NewTopicTest>;

type TopicTestFormDefaults = Pick<NewTopicTest, 'id'>;

type TopicTestFormGroupContent = {
  id: FormControl<ITopicTest['id'] | NewTopicTest['id']>;
  name: FormControl<ITopicTest['name']>;
  assessment: FormControl<ITopicTest['assessment']>;
};

export type TopicTestFormGroup = FormGroup<TopicTestFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class TopicTestFormService {
  createTopicTestFormGroup(topicTest: TopicTestFormGroupInput = { id: null }): TopicTestFormGroup {
    const topicTestRawValue = {
      ...this.getFormDefaults(),
      ...topicTest,
    };
    return new FormGroup<TopicTestFormGroupContent>({
      id: new FormControl(
        { value: topicTestRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      name: new FormControl(topicTestRawValue.name),
      assessment: new FormControl(topicTestRawValue.assessment),
    });
  }

  getTopicTest(form: TopicTestFormGroup): ITopicTest | NewTopicTest {
    return form.getRawValue() as ITopicTest | NewTopicTest;
  }

  resetForm(form: TopicTestFormGroup, topicTest: TopicTestFormGroupInput): void {
    const topicTestRawValue = { ...this.getFormDefaults(), ...topicTest };
    form.reset(
      {
        ...topicTestRawValue,
        id: { value: topicTestRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): TopicTestFormDefaults {
    return {
      id: null,
    };
  }
}
