import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { TopicTestFormService, TopicTestFormGroup } from './topic-test-form.service';
import { ITopicTest } from '../topic-test.model';
import { TopicTestService } from '../service/topic-test.service';
import { IAssessment } from 'app/entities/assessment/assessment.model';
import { AssessmentService } from 'app/entities/assessment/service/assessment.service';

@Component({
  selector: 'jhi-topic-test-update',
  templateUrl: './topic-test-update.component.html',
})
export class TopicTestUpdateComponent implements OnInit {
  isSaving = false;
  topicTest: ITopicTest | null = null;

  assessmentsSharedCollection: IAssessment[] = [];

  editForm: TopicTestFormGroup = this.topicTestFormService.createTopicTestFormGroup();

  constructor(
    protected topicTestService: TopicTestService,
    protected topicTestFormService: TopicTestFormService,
    protected assessmentService: AssessmentService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareAssessment = (o1: IAssessment | null, o2: IAssessment | null): boolean => this.assessmentService.compareAssessment(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ topicTest }) => {
      this.topicTest = topicTest;
      if (topicTest) {
        this.updateForm(topicTest);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const topicTest = this.topicTestFormService.getTopicTest(this.editForm);
    if (topicTest.id !== null) {
      this.subscribeToSaveResponse(this.topicTestService.update(topicTest));
    } else {
      this.subscribeToSaveResponse(this.topicTestService.create(topicTest));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITopicTest>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(topicTest: ITopicTest): void {
    this.topicTest = topicTest;
    this.topicTestFormService.resetForm(this.editForm, topicTest);

    this.assessmentsSharedCollection = this.assessmentService.addAssessmentToCollectionIfMissing<IAssessment>(
      this.assessmentsSharedCollection,
      topicTest.assessment
    );
  }

  protected loadRelationshipsOptions(): void {
    this.assessmentService
      .query()
      .pipe(map((res: HttpResponse<IAssessment[]>) => res.body ?? []))
      .pipe(
        map((assessments: IAssessment[]) =>
          this.assessmentService.addAssessmentToCollectionIfMissing<IAssessment>(assessments, this.topicTest?.assessment)
        )
      )
      .subscribe((assessments: IAssessment[]) => (this.assessmentsSharedCollection = assessments));
  }
}
