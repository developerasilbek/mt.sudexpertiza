import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../topic-test.test-samples';

import { TopicTestFormService } from './topic-test-form.service';

describe('TopicTest Form Service', () => {
  let service: TopicTestFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TopicTestFormService);
  });

  describe('Service methods', () => {
    describe('createTopicTestFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createTopicTestFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            assessment: expect.any(Object),
          })
        );
      });

      it('passing ITopicTest should create a new form with FormGroup', () => {
        const formGroup = service.createTopicTestFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            assessment: expect.any(Object),
          })
        );
      });
    });

    describe('getTopicTest', () => {
      it('should return NewTopicTest for default TopicTest initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createTopicTestFormGroup(sampleWithNewData);

        const topicTest = service.getTopicTest(formGroup) as any;

        expect(topicTest).toMatchObject(sampleWithNewData);
      });

      it('should return NewTopicTest for empty TopicTest initial value', () => {
        const formGroup = service.createTopicTestFormGroup();

        const topicTest = service.getTopicTest(formGroup) as any;

        expect(topicTest).toMatchObject({});
      });

      it('should return ITopicTest', () => {
        const formGroup = service.createTopicTestFormGroup(sampleWithRequiredData);

        const topicTest = service.getTopicTest(formGroup) as any;

        expect(topicTest).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing ITopicTest should not enable id FormControl', () => {
        const formGroup = service.createTopicTestFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewTopicTest should disable id FormControl', () => {
        const formGroup = service.createTopicTestFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
