import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITopicTest } from '../topic-test.model';

@Component({
  selector: 'jhi-topic-test-detail',
  templateUrl: './topic-test-detail.component.html',
})
export class TopicTestDetailComponent implements OnInit {
  topicTest: ITopicTest | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ topicTest }) => {
      this.topicTest = topicTest;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
