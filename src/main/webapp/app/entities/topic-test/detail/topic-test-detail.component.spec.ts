import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TopicTestDetailComponent } from './topic-test-detail.component';

describe('TopicTest Management Detail Component', () => {
  let comp: TopicTestDetailComponent;
  let fixture: ComponentFixture<TopicTestDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TopicTestDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ topicTest: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(TopicTestDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(TopicTestDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load topicTest on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.topicTest).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
