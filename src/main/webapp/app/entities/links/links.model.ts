import { IAssessment } from 'app/entities/assessment/assessment.model';

export interface ILinks {
  id: number;
  name?: string | null;
  assessment?: Pick<IAssessment, 'id'> | null;
}

export type NewLinks = Omit<ILinks, 'id'> & { id: null };
