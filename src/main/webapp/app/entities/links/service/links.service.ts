import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ILinks, NewLinks } from '../links.model';

export type PartialUpdateLinks = Partial<ILinks> & Pick<ILinks, 'id'>;

export type EntityResponseType = HttpResponse<ILinks>;
export type EntityArrayResponseType = HttpResponse<ILinks[]>;

@Injectable({ providedIn: 'root' })
export class LinksService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/links');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(links: NewLinks): Observable<EntityResponseType> {
    return this.http.post<ILinks>(this.resourceUrl, links, { observe: 'response' });
  }

  update(links: ILinks): Observable<EntityResponseType> {
    return this.http.put<ILinks>(`${this.resourceUrl}/${this.getLinksIdentifier(links)}`, links, { observe: 'response' });
  }

  partialUpdate(links: PartialUpdateLinks): Observable<EntityResponseType> {
    return this.http.patch<ILinks>(`${this.resourceUrl}/${this.getLinksIdentifier(links)}`, links, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ILinks>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ILinks[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getLinksIdentifier(links: Pick<ILinks, 'id'>): number {
    return links.id;
  }

  compareLinks(o1: Pick<ILinks, 'id'> | null, o2: Pick<ILinks, 'id'> | null): boolean {
    return o1 && o2 ? this.getLinksIdentifier(o1) === this.getLinksIdentifier(o2) : o1 === o2;
  }

  addLinksToCollectionIfMissing<Type extends Pick<ILinks, 'id'>>(
    linksCollection: Type[],
    ...linksToCheck: (Type | null | undefined)[]
  ): Type[] {
    const links: Type[] = linksToCheck.filter(isPresent);
    if (links.length > 0) {
      const linksCollectionIdentifiers = linksCollection.map(linksItem => this.getLinksIdentifier(linksItem)!);
      const linksToAdd = links.filter(linksItem => {
        const linksIdentifier = this.getLinksIdentifier(linksItem);
        if (linksCollectionIdentifiers.includes(linksIdentifier)) {
          return false;
        }
        linksCollectionIdentifiers.push(linksIdentifier);
        return true;
      });
      return [...linksToAdd, ...linksCollection];
    }
    return linksCollection;
  }
}
