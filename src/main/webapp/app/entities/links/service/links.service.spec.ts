import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ILinks } from '../links.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../links.test-samples';

import { LinksService } from './links.service';

const requireRestSample: ILinks = {
  ...sampleWithRequiredData,
};

describe('Links Service', () => {
  let service: LinksService;
  let httpMock: HttpTestingController;
  let expectedResult: ILinks | ILinks[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(LinksService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Links', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const links = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(links).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Links', () => {
      const links = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(links).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Links', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Links', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Links', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addLinksToCollectionIfMissing', () => {
      it('should add a Links to an empty array', () => {
        const links: ILinks = sampleWithRequiredData;
        expectedResult = service.addLinksToCollectionIfMissing([], links);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(links);
      });

      it('should not add a Links to an array that contains it', () => {
        const links: ILinks = sampleWithRequiredData;
        const linksCollection: ILinks[] = [
          {
            ...links,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addLinksToCollectionIfMissing(linksCollection, links);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Links to an array that doesn't contain it", () => {
        const links: ILinks = sampleWithRequiredData;
        const linksCollection: ILinks[] = [sampleWithPartialData];
        expectedResult = service.addLinksToCollectionIfMissing(linksCollection, links);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(links);
      });

      it('should add only unique Links to an array', () => {
        const linksArray: ILinks[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const linksCollection: ILinks[] = [sampleWithRequiredData];
        expectedResult = service.addLinksToCollectionIfMissing(linksCollection, ...linksArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const links: ILinks = sampleWithRequiredData;
        const links2: ILinks = sampleWithPartialData;
        expectedResult = service.addLinksToCollectionIfMissing([], links, links2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(links);
        expect(expectedResult).toContain(links2);
      });

      it('should accept null and undefined values', () => {
        const links: ILinks = sampleWithRequiredData;
        expectedResult = service.addLinksToCollectionIfMissing([], null, links, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(links);
      });

      it('should return initial array if no Links is added', () => {
        const linksCollection: ILinks[] = [sampleWithRequiredData];
        expectedResult = service.addLinksToCollectionIfMissing(linksCollection, undefined, null);
        expect(expectedResult).toEqual(linksCollection);
      });
    });

    describe('compareLinks', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareLinks(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareLinks(entity1, entity2);
        const compareResult2 = service.compareLinks(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareLinks(entity1, entity2);
        const compareResult2 = service.compareLinks(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareLinks(entity1, entity2);
        const compareResult2 = service.compareLinks(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
