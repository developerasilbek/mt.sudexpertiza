import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ILinks } from '../links.model';
import { LinksService } from '../service/links.service';

@Injectable({ providedIn: 'root' })
export class LinksRoutingResolveService implements Resolve<ILinks | null> {
  constructor(protected service: LinksService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ILinks | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((links: HttpResponse<ILinks>) => {
          if (links.body) {
            return of(links.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
