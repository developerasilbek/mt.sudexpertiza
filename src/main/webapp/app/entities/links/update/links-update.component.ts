import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { LinksFormService, LinksFormGroup } from './links-form.service';
import { ILinks } from '../links.model';
import { LinksService } from '../service/links.service';
import { IAssessment } from 'app/entities/assessment/assessment.model';
import { AssessmentService } from 'app/entities/assessment/service/assessment.service';

@Component({
  selector: 'jhi-links-update',
  templateUrl: './links-update.component.html',
})
export class LinksUpdateComponent implements OnInit {
  isSaving = false;
  links: ILinks | null = null;

  assessmentsSharedCollection: IAssessment[] = [];

  editForm: LinksFormGroup = this.linksFormService.createLinksFormGroup();

  constructor(
    protected linksService: LinksService,
    protected linksFormService: LinksFormService,
    protected assessmentService: AssessmentService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareAssessment = (o1: IAssessment | null, o2: IAssessment | null): boolean => this.assessmentService.compareAssessment(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ links }) => {
      this.links = links;
      if (links) {
        this.updateForm(links);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const links = this.linksFormService.getLinks(this.editForm);
    if (links.id !== null) {
      this.subscribeToSaveResponse(this.linksService.update(links));
    } else {
      this.subscribeToSaveResponse(this.linksService.create(links));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILinks>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(links: ILinks): void {
    this.links = links;
    this.linksFormService.resetForm(this.editForm, links);

    this.assessmentsSharedCollection = this.assessmentService.addAssessmentToCollectionIfMissing<IAssessment>(
      this.assessmentsSharedCollection,
      links.assessment
    );
  }

  protected loadRelationshipsOptions(): void {
    this.assessmentService
      .query()
      .pipe(map((res: HttpResponse<IAssessment[]>) => res.body ?? []))
      .pipe(
        map((assessments: IAssessment[]) =>
          this.assessmentService.addAssessmentToCollectionIfMissing<IAssessment>(assessments, this.links?.assessment)
        )
      )
      .subscribe((assessments: IAssessment[]) => (this.assessmentsSharedCollection = assessments));
  }
}
