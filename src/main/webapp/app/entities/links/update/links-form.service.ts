import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ILinks, NewLinks } from '../links.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts ILinks for edit and NewLinksFormGroupInput for create.
 */
type LinksFormGroupInput = ILinks | PartialWithRequiredKeyOf<NewLinks>;

type LinksFormDefaults = Pick<NewLinks, 'id'>;

type LinksFormGroupContent = {
  id: FormControl<ILinks['id'] | NewLinks['id']>;
  name: FormControl<ILinks['name']>;
  assessment: FormControl<ILinks['assessment']>;
};

export type LinksFormGroup = FormGroup<LinksFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class LinksFormService {
  createLinksFormGroup(links: LinksFormGroupInput = { id: null }): LinksFormGroup {
    const linksRawValue = {
      ...this.getFormDefaults(),
      ...links,
    };
    return new FormGroup<LinksFormGroupContent>({
      id: new FormControl(
        { value: linksRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      name: new FormControl(linksRawValue.name),
      assessment: new FormControl(linksRawValue.assessment),
    });
  }

  getLinks(form: LinksFormGroup): ILinks | NewLinks {
    return form.getRawValue() as ILinks | NewLinks;
  }

  resetForm(form: LinksFormGroup, links: LinksFormGroupInput): void {
    const linksRawValue = { ...this.getFormDefaults(), ...links };
    form.reset(
      {
        ...linksRawValue,
        id: { value: linksRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): LinksFormDefaults {
    return {
      id: null,
    };
  }
}
