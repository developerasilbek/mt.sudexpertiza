import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { LinksFormService } from './links-form.service';
import { LinksService } from '../service/links.service';
import { ILinks } from '../links.model';
import { IAssessment } from 'app/entities/assessment/assessment.model';
import { AssessmentService } from 'app/entities/assessment/service/assessment.service';

import { LinksUpdateComponent } from './links-update.component';

describe('Links Management Update Component', () => {
  let comp: LinksUpdateComponent;
  let fixture: ComponentFixture<LinksUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let linksFormService: LinksFormService;
  let linksService: LinksService;
  let assessmentService: AssessmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [LinksUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(LinksUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(LinksUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    linksFormService = TestBed.inject(LinksFormService);
    linksService = TestBed.inject(LinksService);
    assessmentService = TestBed.inject(AssessmentService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Assessment query and add missing value', () => {
      const links: ILinks = { id: 456 };
      const assessment: IAssessment = { id: 68122 };
      links.assessment = assessment;

      const assessmentCollection: IAssessment[] = [{ id: 14904 }];
      jest.spyOn(assessmentService, 'query').mockReturnValue(of(new HttpResponse({ body: assessmentCollection })));
      const additionalAssessments = [assessment];
      const expectedCollection: IAssessment[] = [...additionalAssessments, ...assessmentCollection];
      jest.spyOn(assessmentService, 'addAssessmentToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ links });
      comp.ngOnInit();

      expect(assessmentService.query).toHaveBeenCalled();
      expect(assessmentService.addAssessmentToCollectionIfMissing).toHaveBeenCalledWith(
        assessmentCollection,
        ...additionalAssessments.map(expect.objectContaining)
      );
      expect(comp.assessmentsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const links: ILinks = { id: 456 };
      const assessment: IAssessment = { id: 48280 };
      links.assessment = assessment;

      activatedRoute.data = of({ links });
      comp.ngOnInit();

      expect(comp.assessmentsSharedCollection).toContain(assessment);
      expect(comp.links).toEqual(links);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ILinks>>();
      const links = { id: 123 };
      jest.spyOn(linksFormService, 'getLinks').mockReturnValue(links);
      jest.spyOn(linksService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ links });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: links }));
      saveSubject.complete();

      // THEN
      expect(linksFormService.getLinks).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(linksService.update).toHaveBeenCalledWith(expect.objectContaining(links));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ILinks>>();
      const links = { id: 123 };
      jest.spyOn(linksFormService, 'getLinks').mockReturnValue({ id: null });
      jest.spyOn(linksService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ links: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: links }));
      saveSubject.complete();

      // THEN
      expect(linksFormService.getLinks).toHaveBeenCalled();
      expect(linksService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ILinks>>();
      const links = { id: 123 };
      jest.spyOn(linksService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ links });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(linksService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareAssessment', () => {
      it('Should forward to assessmentService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(assessmentService, 'compareAssessment');
        comp.compareAssessment(entity, entity2);
        expect(assessmentService.compareAssessment).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
