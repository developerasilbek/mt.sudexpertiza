import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../links.test-samples';

import { LinksFormService } from './links-form.service';

describe('Links Form Service', () => {
  let service: LinksFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LinksFormService);
  });

  describe('Service methods', () => {
    describe('createLinksFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createLinksFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            assessment: expect.any(Object),
          })
        );
      });

      it('passing ILinks should create a new form with FormGroup', () => {
        const formGroup = service.createLinksFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            assessment: expect.any(Object),
          })
        );
      });
    });

    describe('getLinks', () => {
      it('should return NewLinks for default Links initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createLinksFormGroup(sampleWithNewData);

        const links = service.getLinks(formGroup) as any;

        expect(links).toMatchObject(sampleWithNewData);
      });

      it('should return NewLinks for empty Links initial value', () => {
        const formGroup = service.createLinksFormGroup();

        const links = service.getLinks(formGroup) as any;

        expect(links).toMatchObject({});
      });

      it('should return ILinks', () => {
        const formGroup = service.createLinksFormGroup(sampleWithRequiredData);

        const links = service.getLinks(formGroup) as any;

        expect(links).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing ILinks should not enable id FormControl', () => {
        const formGroup = service.createLinksFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewLinks should disable id FormControl', () => {
        const formGroup = service.createLinksFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
