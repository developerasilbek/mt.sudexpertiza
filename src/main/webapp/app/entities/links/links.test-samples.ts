import { ILinks, NewLinks } from './links.model';

export const sampleWithRequiredData: ILinks = {
  id: 54331,
};

export const sampleWithPartialData: ILinks = {
  id: 82192,
  name: 'solution',
};

export const sampleWithFullData: ILinks = {
  id: 5584,
  name: 'SAS AGP Fully-configurable',
};

export const sampleWithNewData: NewLinks = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
