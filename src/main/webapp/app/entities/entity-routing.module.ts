import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'modul',
        data: { pageTitle: 'sudexApp.modul.home.title' },
        loadChildren: () => import('./modul/modul.module').then(m => m.ModulModule),
      },
      {
        path: 'topic',
        data: { pageTitle: 'sudexApp.topic.home.title' },
        loadChildren: () => import('./topic/topic.module').then(m => m.TopicModule),
      },
      {
        path: 'attachment',
        data: { pageTitle: 'sudexApp.attachment.home.title' },
        loadChildren: () => import('./attachment/attachment.module').then(m => m.AttachmentModule),
      },
      {
        path: 'block',
        data: { pageTitle: 'sudexApp.block.home.title' },
        loadChildren: () => import('./block/block.module').then(m => m.BlockModule),
      },
      {
        path: 'course',
        data: { pageTitle: 'sudexApp.course.home.title' },
        loadChildren: () => import('./course/course.module').then(m => m.CourseModule),
      },
      {
        path: 'assessment',
        data: { pageTitle: 'sudexApp.assessment.home.title' },
        loadChildren: () => import('./assessment/assessment.module').then(m => m.AssessmentModule),
      },
      {
        path: 'references',
        data: { pageTitle: 'sudexApp.references.home.title' },
        loadChildren: () => import('./references/references.module').then(m => m.ReferencesModule),
      },
      {
        path: 'links',
        data: { pageTitle: 'sudexApp.links.home.title' },
        loadChildren: () => import('./links/links.module').then(m => m.LinksModule),
      },
      {
        path: 'glossary',
        data: { pageTitle: 'sudexApp.glossary.home.title' },
        loadChildren: () => import('./glossary/glossary.module').then(m => m.GlossaryModule),
      },
      {
        path: 'topic-test',
        data: { pageTitle: 'sudexApp.topicTest.home.title' },
        loadChildren: () => import('./topic-test/topic-test.module').then(m => m.TopicTestModule),
      },
      {
        path: 'question',
        data: { pageTitle: 'sudexApp.question.home.title' },
        loadChildren: () => import('./question/question.module').then(m => m.QuestionModule),
      },
      {
        path: 'answer',
        data: { pageTitle: 'sudexApp.answer.home.title' },
        loadChildren: () => import('./answer/answer.module').then(m => m.AnswerModule),
      },
      {
        path: 'test-result',
        data: { pageTitle: 'sudexApp.testResult.home.title' },
        loadChildren: () => import('./test-result/test-result.module').then(m => m.TestResultModule),
      },
      {
        path: 'listener',
        data: { pageTitle: 'sudexApp.listener.home.title' },
        loadChildren: () => import('./listener/listener.module').then(m => m.ListenerModule),
      },
      {
        path: 'specialization',
        data: { pageTitle: 'sudexApp.specialization.home.title' },
        loadChildren: () => import('./specialization/specialization.module').then(m => m.SpecializationModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
