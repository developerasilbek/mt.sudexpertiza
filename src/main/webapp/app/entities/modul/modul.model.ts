import { IBlock } from 'app/entities/block/block.model';

export interface IModul {
  id: number;
  name?: string | null;
  decription?: string | null;
  block?: Pick<IBlock, 'id'> | null;
}

export type NewModul = Omit<IModul, 'id'> & { id: null };
