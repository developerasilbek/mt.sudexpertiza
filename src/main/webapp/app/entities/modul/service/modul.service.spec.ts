import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IModul } from '../modul.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../modul.test-samples';

import { ModulService } from './modul.service';

const requireRestSample: IModul = {
  ...sampleWithRequiredData,
};

describe('Modul Service', () => {
  let service: ModulService;
  let httpMock: HttpTestingController;
  let expectedResult: IModul | IModul[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ModulService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Modul', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const modul = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(modul).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Modul', () => {
      const modul = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(modul).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Modul', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Modul', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Modul', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addModulToCollectionIfMissing', () => {
      it('should add a Modul to an empty array', () => {
        const modul: IModul = sampleWithRequiredData;
        expectedResult = service.addModulToCollectionIfMissing([], modul);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(modul);
      });

      it('should not add a Modul to an array that contains it', () => {
        const modul: IModul = sampleWithRequiredData;
        const modulCollection: IModul[] = [
          {
            ...modul,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addModulToCollectionIfMissing(modulCollection, modul);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Modul to an array that doesn't contain it", () => {
        const modul: IModul = sampleWithRequiredData;
        const modulCollection: IModul[] = [sampleWithPartialData];
        expectedResult = service.addModulToCollectionIfMissing(modulCollection, modul);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(modul);
      });

      it('should add only unique Modul to an array', () => {
        const modulArray: IModul[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const modulCollection: IModul[] = [sampleWithRequiredData];
        expectedResult = service.addModulToCollectionIfMissing(modulCollection, ...modulArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const modul: IModul = sampleWithRequiredData;
        const modul2: IModul = sampleWithPartialData;
        expectedResult = service.addModulToCollectionIfMissing([], modul, modul2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(modul);
        expect(expectedResult).toContain(modul2);
      });

      it('should accept null and undefined values', () => {
        const modul: IModul = sampleWithRequiredData;
        expectedResult = service.addModulToCollectionIfMissing([], null, modul, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(modul);
      });

      it('should return initial array if no Modul is added', () => {
        const modulCollection: IModul[] = [sampleWithRequiredData];
        expectedResult = service.addModulToCollectionIfMissing(modulCollection, undefined, null);
        expect(expectedResult).toEqual(modulCollection);
      });
    });

    describe('compareModul', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareModul(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareModul(entity1, entity2);
        const compareResult2 = service.compareModul(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareModul(entity1, entity2);
        const compareResult2 = service.compareModul(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareModul(entity1, entity2);
        const compareResult2 = service.compareModul(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
