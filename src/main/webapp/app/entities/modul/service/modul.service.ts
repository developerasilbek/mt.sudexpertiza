import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IModul, NewModul } from '../modul.model';

export type PartialUpdateModul = Partial<IModul> & Pick<IModul, 'id'>;

export type EntityResponseType = HttpResponse<IModul>;
export type EntityArrayResponseType = HttpResponse<IModul[]>;

@Injectable({ providedIn: 'root' })
export class ModulService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/moduls');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(modul: NewModul): Observable<EntityResponseType> {
    return this.http.post<IModul>(this.resourceUrl, modul, { observe: 'response' });
  }

  update(modul: IModul): Observable<EntityResponseType> {
    return this.http.put<IModul>(`${this.resourceUrl}/${this.getModulIdentifier(modul)}`, modul, { observe: 'response' });
  }

  partialUpdate(modul: PartialUpdateModul): Observable<EntityResponseType> {
    return this.http.patch<IModul>(`${this.resourceUrl}/${this.getModulIdentifier(modul)}`, modul, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IModul>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IModul[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getModulIdentifier(modul: Pick<IModul, 'id'>): number {
    return modul.id;
  }

  compareModul(o1: Pick<IModul, 'id'> | null, o2: Pick<IModul, 'id'> | null): boolean {
    return o1 && o2 ? this.getModulIdentifier(o1) === this.getModulIdentifier(o2) : o1 === o2;
  }

  addModulToCollectionIfMissing<Type extends Pick<IModul, 'id'>>(
    modulCollection: Type[],
    ...modulsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const moduls: Type[] = modulsToCheck.filter(isPresent);
    if (moduls.length > 0) {
      const modulCollectionIdentifiers = modulCollection.map(modulItem => this.getModulIdentifier(modulItem)!);
      const modulsToAdd = moduls.filter(modulItem => {
        const modulIdentifier = this.getModulIdentifier(modulItem);
        if (modulCollectionIdentifiers.includes(modulIdentifier)) {
          return false;
        }
        modulCollectionIdentifiers.push(modulIdentifier);
        return true;
      });
      return [...modulsToAdd, ...modulCollection];
    }
    return modulCollection;
  }
}
