import { IModul, NewModul } from './modul.model';

export const sampleWithRequiredData: IModul = {
  id: 63197,
};

export const sampleWithPartialData: IModul = {
  id: 67624,
};

export const sampleWithFullData: IModul = {
  id: 17693,
  name: 'black incubate Manager',
  decription: 'Operations',
};

export const sampleWithNewData: NewModul = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
