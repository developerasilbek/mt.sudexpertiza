import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IModul } from '../modul.model';
import { ModulService } from '../service/modul.service';

@Injectable({ providedIn: 'root' })
export class ModulRoutingResolveService implements Resolve<IModul | null> {
  constructor(protected service: ModulService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IModul | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((modul: HttpResponse<IModul>) => {
          if (modul.body) {
            return of(modul.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
