import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ModulComponent } from '../list/modul.component';
import { ModulDetailComponent } from '../detail/modul-detail.component';
import { ModulUpdateComponent } from '../update/modul-update.component';
import { ModulRoutingResolveService } from './modul-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const modulRoute: Routes = [
  {
    path: '',
    component: ModulComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ModulDetailComponent,
    resolve: {
      modul: ModulRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ModulUpdateComponent,
    resolve: {
      modul: ModulRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ModulUpdateComponent,
    resolve: {
      modul: ModulRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(modulRoute)],
  exports: [RouterModule],
})
export class ModulRoutingModule {}
