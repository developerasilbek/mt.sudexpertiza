import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ModulComponent } from './list/modul.component';
import { ModulDetailComponent } from './detail/modul-detail.component';
import { ModulUpdateComponent } from './update/modul-update.component';
import { ModulDeleteDialogComponent } from './delete/modul-delete-dialog.component';
import { ModulRoutingModule } from './route/modul-routing.module';

@NgModule({
  imports: [SharedModule, ModulRoutingModule],
  declarations: [ModulComponent, ModulDetailComponent, ModulUpdateComponent, ModulDeleteDialogComponent],
})
export class ModulModule {}
