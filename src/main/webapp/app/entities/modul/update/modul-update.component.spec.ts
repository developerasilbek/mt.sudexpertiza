import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ModulFormService } from './modul-form.service';
import { ModulService } from '../service/modul.service';
import { IModul } from '../modul.model';
import { IBlock } from 'app/entities/block/block.model';
import { BlockService } from 'app/entities/block/service/block.service';

import { ModulUpdateComponent } from './modul-update.component';

describe('Modul Management Update Component', () => {
  let comp: ModulUpdateComponent;
  let fixture: ComponentFixture<ModulUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let modulFormService: ModulFormService;
  let modulService: ModulService;
  let blockService: BlockService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [ModulUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ModulUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ModulUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    modulFormService = TestBed.inject(ModulFormService);
    modulService = TestBed.inject(ModulService);
    blockService = TestBed.inject(BlockService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Block query and add missing value', () => {
      const modul: IModul = { id: 456 };
      const block: IBlock = { id: 11474 };
      modul.block = block;

      const blockCollection: IBlock[] = [{ id: 94589 }];
      jest.spyOn(blockService, 'query').mockReturnValue(of(new HttpResponse({ body: blockCollection })));
      const additionalBlocks = [block];
      const expectedCollection: IBlock[] = [...additionalBlocks, ...blockCollection];
      jest.spyOn(blockService, 'addBlockToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ modul });
      comp.ngOnInit();

      expect(blockService.query).toHaveBeenCalled();
      expect(blockService.addBlockToCollectionIfMissing).toHaveBeenCalledWith(
        blockCollection,
        ...additionalBlocks.map(expect.objectContaining)
      );
      expect(comp.blocksSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const modul: IModul = { id: 456 };
      const block: IBlock = { id: 9745 };
      modul.block = block;

      activatedRoute.data = of({ modul });
      comp.ngOnInit();

      expect(comp.blocksSharedCollection).toContain(block);
      expect(comp.modul).toEqual(modul);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IModul>>();
      const modul = { id: 123 };
      jest.spyOn(modulFormService, 'getModul').mockReturnValue(modul);
      jest.spyOn(modulService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ modul });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: modul }));
      saveSubject.complete();

      // THEN
      expect(modulFormService.getModul).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(modulService.update).toHaveBeenCalledWith(expect.objectContaining(modul));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IModul>>();
      const modul = { id: 123 };
      jest.spyOn(modulFormService, 'getModul').mockReturnValue({ id: null });
      jest.spyOn(modulService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ modul: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: modul }));
      saveSubject.complete();

      // THEN
      expect(modulFormService.getModul).toHaveBeenCalled();
      expect(modulService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IModul>>();
      const modul = { id: 123 };
      jest.spyOn(modulService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ modul });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(modulService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareBlock', () => {
      it('Should forward to blockService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(blockService, 'compareBlock');
        comp.compareBlock(entity, entity2);
        expect(blockService.compareBlock).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
