import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IModul, NewModul } from '../modul.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IModul for edit and NewModulFormGroupInput for create.
 */
type ModulFormGroupInput = IModul | PartialWithRequiredKeyOf<NewModul>;

type ModulFormDefaults = Pick<NewModul, 'id'>;

type ModulFormGroupContent = {
  id: FormControl<IModul['id'] | NewModul['id']>;
  name: FormControl<IModul['name']>;
  decription: FormControl<IModul['decription']>;
  block: FormControl<IModul['block']>;
};

export type ModulFormGroup = FormGroup<ModulFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class ModulFormService {
  createModulFormGroup(modul: ModulFormGroupInput = { id: null }): ModulFormGroup {
    const modulRawValue = {
      ...this.getFormDefaults(),
      ...modul,
    };
    return new FormGroup<ModulFormGroupContent>({
      id: new FormControl(
        { value: modulRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      name: new FormControl(modulRawValue.name),
      decription: new FormControl(modulRawValue.decription),
      block: new FormControl(modulRawValue.block),
    });
  }

  getModul(form: ModulFormGroup): IModul | NewModul {
    return form.getRawValue() as IModul | NewModul;
  }

  resetForm(form: ModulFormGroup, modul: ModulFormGroupInput): void {
    const modulRawValue = { ...this.getFormDefaults(), ...modul };
    form.reset(
      {
        ...modulRawValue,
        id: { value: modulRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): ModulFormDefaults {
    return {
      id: null,
    };
  }
}
