import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ModulFormService, ModulFormGroup } from './modul-form.service';
import { IModul } from '../modul.model';
import { ModulService } from '../service/modul.service';
import { IBlock } from 'app/entities/block/block.model';
import { BlockService } from 'app/entities/block/service/block.service';

@Component({
  selector: 'jhi-modul-update',
  templateUrl: './modul-update.component.html',
})
export class ModulUpdateComponent implements OnInit {
  isSaving = false;
  modul: IModul | null = null;

  blocksSharedCollection: IBlock[] = [];

  editForm: ModulFormGroup = this.modulFormService.createModulFormGroup();

  constructor(
    protected modulService: ModulService,
    protected modulFormService: ModulFormService,
    protected blockService: BlockService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareBlock = (o1: IBlock | null, o2: IBlock | null): boolean => this.blockService.compareBlock(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ modul }) => {
      this.modul = modul;
      if (modul) {
        this.updateForm(modul);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const modul = this.modulFormService.getModul(this.editForm);
    if (modul.id !== null) {
      this.subscribeToSaveResponse(this.modulService.update(modul));
    } else {
      this.subscribeToSaveResponse(this.modulService.create(modul));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IModul>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(modul: IModul): void {
    this.modul = modul;
    this.modulFormService.resetForm(this.editForm, modul);

    this.blocksSharedCollection = this.blockService.addBlockToCollectionIfMissing<IBlock>(this.blocksSharedCollection, modul.block);
  }

  protected loadRelationshipsOptions(): void {
    this.blockService
      .query()
      .pipe(map((res: HttpResponse<IBlock[]>) => res.body ?? []))
      .pipe(map((blocks: IBlock[]) => this.blockService.addBlockToCollectionIfMissing<IBlock>(blocks, this.modul?.block)))
      .subscribe((blocks: IBlock[]) => (this.blocksSharedCollection = blocks));
  }
}
