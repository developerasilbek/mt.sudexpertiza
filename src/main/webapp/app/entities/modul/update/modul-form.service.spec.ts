import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../modul.test-samples';

import { ModulFormService } from './modul-form.service';

describe('Modul Form Service', () => {
  let service: ModulFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ModulFormService);
  });

  describe('Service methods', () => {
    describe('createModulFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createModulFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            decription: expect.any(Object),
            block: expect.any(Object),
          })
        );
      });

      it('passing IModul should create a new form with FormGroup', () => {
        const formGroup = service.createModulFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            decription: expect.any(Object),
            block: expect.any(Object),
          })
        );
      });
    });

    describe('getModul', () => {
      it('should return NewModul for default Modul initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createModulFormGroup(sampleWithNewData);

        const modul = service.getModul(formGroup) as any;

        expect(modul).toMatchObject(sampleWithNewData);
      });

      it('should return NewModul for empty Modul initial value', () => {
        const formGroup = service.createModulFormGroup();

        const modul = service.getModul(formGroup) as any;

        expect(modul).toMatchObject({});
      });

      it('should return IModul', () => {
        const formGroup = service.createModulFormGroup(sampleWithRequiredData);

        const modul = service.getModul(formGroup) as any;

        expect(modul).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IModul should not enable id FormControl', () => {
        const formGroup = service.createModulFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewModul should disable id FormControl', () => {
        const formGroup = service.createModulFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
