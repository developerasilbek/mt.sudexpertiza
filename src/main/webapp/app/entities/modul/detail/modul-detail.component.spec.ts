import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ModulDetailComponent } from './modul-detail.component';

describe('Modul Management Detail Component', () => {
  let comp: ModulDetailComponent;
  let fixture: ComponentFixture<ModulDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ModulDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ modul: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(ModulDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ModulDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load modul on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.modul).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
