import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IModul } from '../modul.model';

@Component({
  selector: 'jhi-modul-detail',
  templateUrl: './modul-detail.component.html',
})
export class ModulDetailComponent implements OnInit {
  modul: IModul | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ modul }) => {
      this.modul = modul;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
