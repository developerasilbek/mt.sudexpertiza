export enum AssessmentType {
  TOPIC_TEST = 'TOPIC_TEST',

  REFERENCES = 'REFERENCES',

  LINKS = 'LINKS',

  UNKNOWN = 'UNKNOWN',
}
