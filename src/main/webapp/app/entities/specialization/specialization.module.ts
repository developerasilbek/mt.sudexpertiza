import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { SpecializationComponent } from './list/specialization.component';
import { SpecializationDetailComponent } from './detail/specialization-detail.component';
import { SpecializationUpdateComponent } from './update/specialization-update.component';
import { SpecializationDeleteDialogComponent } from './delete/specialization-delete-dialog.component';
import { SpecializationRoutingModule } from './route/specialization-routing.module';

@NgModule({
  imports: [SharedModule, SpecializationRoutingModule],
  declarations: [
    SpecializationComponent,
    SpecializationDetailComponent,
    SpecializationUpdateComponent,
    SpecializationDeleteDialogComponent,
  ],
})
export class SpecializationModule {}
