import { Centre } from 'app/entities/enumerations/centre.model';

import { ISpecialization, NewSpecialization } from './specialization.model';

export const sampleWithRequiredData: ISpecialization = {
  id: 29659,
};

export const sampleWithPartialData: ISpecialization = {
  id: 84411,
  name: 'Korea synergistic',
};

export const sampleWithFullData: ISpecialization = {
  id: 23914,
  name: 'maximize Shoes Global',
  centre: Centre['RSEM'],
};

export const sampleWithNewData: NewSpecialization = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
