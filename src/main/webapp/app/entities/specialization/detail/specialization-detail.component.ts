import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISpecialization } from '../specialization.model';

@Component({
  selector: 'jhi-specialization-detail',
  templateUrl: './specialization-detail.component.html',
})
export class SpecializationDetailComponent implements OnInit {
  specialization: ISpecialization | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ specialization }) => {
      this.specialization = specialization;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
