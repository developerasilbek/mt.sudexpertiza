import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SpecializationDetailComponent } from './specialization-detail.component';

describe('Specialization Management Detail Component', () => {
  let comp: SpecializationDetailComponent;
  let fixture: ComponentFixture<SpecializationDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SpecializationDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ specialization: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(SpecializationDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(SpecializationDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load specialization on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.specialization).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
