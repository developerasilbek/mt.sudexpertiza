import { Centre } from 'app/entities/enumerations/centre.model';

export interface ISpecialization {
  id: number;
  name?: string | null;
  centre?: Centre | null;
}

export type NewSpecialization = Omit<ISpecialization, 'id'> & { id: null };
