import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ISpecialization, NewSpecialization } from '../specialization.model';

export type PartialUpdateSpecialization = Partial<ISpecialization> & Pick<ISpecialization, 'id'>;

export type EntityResponseType = HttpResponse<ISpecialization>;
export type EntityArrayResponseType = HttpResponse<ISpecialization[]>;

@Injectable({ providedIn: 'root' })
export class SpecializationService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/specializations');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(specialization: NewSpecialization): Observable<EntityResponseType> {
    return this.http.post<ISpecialization>(this.resourceUrl, specialization, { observe: 'response' });
  }

  update(specialization: ISpecialization): Observable<EntityResponseType> {
    return this.http.put<ISpecialization>(`${this.resourceUrl}/${this.getSpecializationIdentifier(specialization)}`, specialization, {
      observe: 'response',
    });
  }

  partialUpdate(specialization: PartialUpdateSpecialization): Observable<EntityResponseType> {
    return this.http.patch<ISpecialization>(`${this.resourceUrl}/${this.getSpecializationIdentifier(specialization)}`, specialization, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISpecialization>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISpecialization[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getSpecializationIdentifier(specialization: Pick<ISpecialization, 'id'>): number {
    return specialization.id;
  }

  compareSpecialization(o1: Pick<ISpecialization, 'id'> | null, o2: Pick<ISpecialization, 'id'> | null): boolean {
    return o1 && o2 ? this.getSpecializationIdentifier(o1) === this.getSpecializationIdentifier(o2) : o1 === o2;
  }

  addSpecializationToCollectionIfMissing<Type extends Pick<ISpecialization, 'id'>>(
    specializationCollection: Type[],
    ...specializationsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const specializations: Type[] = specializationsToCheck.filter(isPresent);
    if (specializations.length > 0) {
      const specializationCollectionIdentifiers = specializationCollection.map(
        specializationItem => this.getSpecializationIdentifier(specializationItem)!
      );
      const specializationsToAdd = specializations.filter(specializationItem => {
        const specializationIdentifier = this.getSpecializationIdentifier(specializationItem);
        if (specializationCollectionIdentifiers.includes(specializationIdentifier)) {
          return false;
        }
        specializationCollectionIdentifiers.push(specializationIdentifier);
        return true;
      });
      return [...specializationsToAdd, ...specializationCollection];
    }
    return specializationCollection;
  }
}
