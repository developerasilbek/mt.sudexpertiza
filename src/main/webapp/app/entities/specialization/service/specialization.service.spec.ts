import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ISpecialization } from '../specialization.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../specialization.test-samples';

import { SpecializationService } from './specialization.service';

const requireRestSample: ISpecialization = {
  ...sampleWithRequiredData,
};

describe('Specialization Service', () => {
  let service: SpecializationService;
  let httpMock: HttpTestingController;
  let expectedResult: ISpecialization | ISpecialization[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(SpecializationService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Specialization', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const specialization = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(specialization).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Specialization', () => {
      const specialization = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(specialization).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Specialization', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Specialization', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Specialization', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addSpecializationToCollectionIfMissing', () => {
      it('should add a Specialization to an empty array', () => {
        const specialization: ISpecialization = sampleWithRequiredData;
        expectedResult = service.addSpecializationToCollectionIfMissing([], specialization);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(specialization);
      });

      it('should not add a Specialization to an array that contains it', () => {
        const specialization: ISpecialization = sampleWithRequiredData;
        const specializationCollection: ISpecialization[] = [
          {
            ...specialization,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addSpecializationToCollectionIfMissing(specializationCollection, specialization);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Specialization to an array that doesn't contain it", () => {
        const specialization: ISpecialization = sampleWithRequiredData;
        const specializationCollection: ISpecialization[] = [sampleWithPartialData];
        expectedResult = service.addSpecializationToCollectionIfMissing(specializationCollection, specialization);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(specialization);
      });

      it('should add only unique Specialization to an array', () => {
        const specializationArray: ISpecialization[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const specializationCollection: ISpecialization[] = [sampleWithRequiredData];
        expectedResult = service.addSpecializationToCollectionIfMissing(specializationCollection, ...specializationArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const specialization: ISpecialization = sampleWithRequiredData;
        const specialization2: ISpecialization = sampleWithPartialData;
        expectedResult = service.addSpecializationToCollectionIfMissing([], specialization, specialization2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(specialization);
        expect(expectedResult).toContain(specialization2);
      });

      it('should accept null and undefined values', () => {
        const specialization: ISpecialization = sampleWithRequiredData;
        expectedResult = service.addSpecializationToCollectionIfMissing([], null, specialization, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(specialization);
      });

      it('should return initial array if no Specialization is added', () => {
        const specializationCollection: ISpecialization[] = [sampleWithRequiredData];
        expectedResult = service.addSpecializationToCollectionIfMissing(specializationCollection, undefined, null);
        expect(expectedResult).toEqual(specializationCollection);
      });
    });

    describe('compareSpecialization', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareSpecialization(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareSpecialization(entity1, entity2);
        const compareResult2 = service.compareSpecialization(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareSpecialization(entity1, entity2);
        const compareResult2 = service.compareSpecialization(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareSpecialization(entity1, entity2);
        const compareResult2 = service.compareSpecialization(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
