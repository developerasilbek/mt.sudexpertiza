import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ISpecialization } from '../specialization.model';
import { SpecializationService } from '../service/specialization.service';

@Injectable({ providedIn: 'root' })
export class SpecializationRoutingResolveService implements Resolve<ISpecialization | null> {
  constructor(protected service: SpecializationService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISpecialization | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((specialization: HttpResponse<ISpecialization>) => {
          if (specialization.body) {
            return of(specialization.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
