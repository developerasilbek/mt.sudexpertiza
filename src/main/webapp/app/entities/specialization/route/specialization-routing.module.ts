import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { SpecializationComponent } from '../list/specialization.component';
import { SpecializationDetailComponent } from '../detail/specialization-detail.component';
import { SpecializationUpdateComponent } from '../update/specialization-update.component';
import { SpecializationRoutingResolveService } from './specialization-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const specializationRoute: Routes = [
  {
    path: '',
    component: SpecializationComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: SpecializationDetailComponent,
    resolve: {
      specialization: SpecializationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: SpecializationUpdateComponent,
    resolve: {
      specialization: SpecializationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: SpecializationUpdateComponent,
    resolve: {
      specialization: SpecializationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(specializationRoute)],
  exports: [RouterModule],
})
export class SpecializationRoutingModule {}
