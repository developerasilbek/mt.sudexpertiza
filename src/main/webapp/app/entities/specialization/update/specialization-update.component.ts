import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { SpecializationFormService, SpecializationFormGroup } from './specialization-form.service';
import { ISpecialization } from '../specialization.model';
import { SpecializationService } from '../service/specialization.service';
import { Centre } from 'app/entities/enumerations/centre.model';

@Component({
  selector: 'jhi-specialization-update',
  templateUrl: './specialization-update.component.html',
})
export class SpecializationUpdateComponent implements OnInit {
  isSaving = false;
  specialization: ISpecialization | null = null;
  centreValues = Object.keys(Centre);

  editForm: SpecializationFormGroup = this.specializationFormService.createSpecializationFormGroup();

  constructor(
    protected specializationService: SpecializationService,
    protected specializationFormService: SpecializationFormService,
    protected activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ specialization }) => {
      this.specialization = specialization;
      if (specialization) {
        this.updateForm(specialization);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const specialization = this.specializationFormService.getSpecialization(this.editForm);
    if (specialization.id !== null) {
      this.subscribeToSaveResponse(this.specializationService.update(specialization));
    } else {
      this.subscribeToSaveResponse(this.specializationService.create(specialization));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISpecialization>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(specialization: ISpecialization): void {
    this.specialization = specialization;
    this.specializationFormService.resetForm(this.editForm, specialization);
  }
}
