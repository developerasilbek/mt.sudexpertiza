import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { SpecializationFormService } from './specialization-form.service';
import { SpecializationService } from '../service/specialization.service';
import { ISpecialization } from '../specialization.model';

import { SpecializationUpdateComponent } from './specialization-update.component';

describe('Specialization Management Update Component', () => {
  let comp: SpecializationUpdateComponent;
  let fixture: ComponentFixture<SpecializationUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let specializationFormService: SpecializationFormService;
  let specializationService: SpecializationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [SpecializationUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(SpecializationUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(SpecializationUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    specializationFormService = TestBed.inject(SpecializationFormService);
    specializationService = TestBed.inject(SpecializationService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const specialization: ISpecialization = { id: 456 };

      activatedRoute.data = of({ specialization });
      comp.ngOnInit();

      expect(comp.specialization).toEqual(specialization);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISpecialization>>();
      const specialization = { id: 123 };
      jest.spyOn(specializationFormService, 'getSpecialization').mockReturnValue(specialization);
      jest.spyOn(specializationService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ specialization });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: specialization }));
      saveSubject.complete();

      // THEN
      expect(specializationFormService.getSpecialization).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(specializationService.update).toHaveBeenCalledWith(expect.objectContaining(specialization));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISpecialization>>();
      const specialization = { id: 123 };
      jest.spyOn(specializationFormService, 'getSpecialization').mockReturnValue({ id: null });
      jest.spyOn(specializationService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ specialization: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: specialization }));
      saveSubject.complete();

      // THEN
      expect(specializationFormService.getSpecialization).toHaveBeenCalled();
      expect(specializationService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISpecialization>>();
      const specialization = { id: 123 };
      jest.spyOn(specializationService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ specialization });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(specializationService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
