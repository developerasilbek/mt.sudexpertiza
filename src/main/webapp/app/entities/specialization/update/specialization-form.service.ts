import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ISpecialization, NewSpecialization } from '../specialization.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts ISpecialization for edit and NewSpecializationFormGroupInput for create.
 */
type SpecializationFormGroupInput = ISpecialization | PartialWithRequiredKeyOf<NewSpecialization>;

type SpecializationFormDefaults = Pick<NewSpecialization, 'id'>;

type SpecializationFormGroupContent = {
  id: FormControl<ISpecialization['id'] | NewSpecialization['id']>;
  name: FormControl<ISpecialization['name']>;
  centre: FormControl<ISpecialization['centre']>;
};

export type SpecializationFormGroup = FormGroup<SpecializationFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class SpecializationFormService {
  createSpecializationFormGroup(specialization: SpecializationFormGroupInput = { id: null }): SpecializationFormGroup {
    const specializationRawValue = {
      ...this.getFormDefaults(),
      ...specialization,
    };
    return new FormGroup<SpecializationFormGroupContent>({
      id: new FormControl(
        { value: specializationRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      name: new FormControl(specializationRawValue.name),
      centre: new FormControl(specializationRawValue.centre),
    });
  }

  getSpecialization(form: SpecializationFormGroup): ISpecialization | NewSpecialization {
    return form.getRawValue() as ISpecialization | NewSpecialization;
  }

  resetForm(form: SpecializationFormGroup, specialization: SpecializationFormGroupInput): void {
    const specializationRawValue = { ...this.getFormDefaults(), ...specialization };
    form.reset(
      {
        ...specializationRawValue,
        id: { value: specializationRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): SpecializationFormDefaults {
    return {
      id: null,
    };
  }
}
