import dayjs from 'dayjs/esm';
import { Centre } from 'app/entities/enumerations/centre.model';

export interface ICourse {
  id: number;
  name?: string | null;
  centre?: Centre | null;
  language?: string | null;
  teachingHour?: number | null;
  startedAt?: dayjs.Dayjs | null;
  finishedAt?: dayjs.Dayjs | null;
}

export type NewCourse = Omit<ICourse, 'id'> & { id: null };
