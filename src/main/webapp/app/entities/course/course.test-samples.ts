import dayjs from 'dayjs/esm';

import { Centre } from 'app/entities/enumerations/centre.model';

import { ICourse, NewCourse } from './course.model';

export const sampleWithRequiredData: ICourse = {
  id: 59109,
};

export const sampleWithPartialData: ICourse = {
  id: 84774,
  name: 'Loan Ergonomic Pants',
  centre: Centre['RSEM'],
  language: 'user-facing Table',
  teachingHour: 84664,
  startedAt: dayjs('2023-04-16T18:15'),
};

export const sampleWithFullData: ICourse = {
  id: 58881,
  name: 'bandwidth-monitored e-commerce Borders',
  centre: Centre['RSEM'],
  language: 'Rand navigating AGP',
  teachingHour: 53948,
  startedAt: dayjs('2023-04-16T19:30'),
  finishedAt: dayjs('2023-04-16T16:39'),
};

export const sampleWithNewData: NewCourse = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
