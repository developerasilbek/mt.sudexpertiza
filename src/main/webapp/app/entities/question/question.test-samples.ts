import { QuestionType } from 'app/entities/enumerations/question-type.model';

import { IQuestion, NewQuestion } from './question.model';

export const sampleWithRequiredData: IQuestion = {
  id: 47363,
};

export const sampleWithPartialData: IQuestion = {
  id: 83781,
  name: 'Concrete',
  requiredScore: 89671,
};

export const sampleWithFullData: IQuestion = {
  id: 66695,
  name: 'tan methodology',
  requiredScore: 15405,
  type: QuestionType['UNKNOWN'],
};

export const sampleWithNewData: NewQuestion = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
