import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { QuestionFormService, QuestionFormGroup } from './question-form.service';
import { IQuestion } from '../question.model';
import { QuestionService } from '../service/question.service';
import { ITopicTest } from 'app/entities/topic-test/topic-test.model';
import { TopicTestService } from 'app/entities/topic-test/service/topic-test.service';
import { QuestionType } from 'app/entities/enumerations/question-type.model';

@Component({
  selector: 'jhi-question-update',
  templateUrl: './question-update.component.html',
})
export class QuestionUpdateComponent implements OnInit {
  isSaving = false;
  question: IQuestion | null = null;
  questionTypeValues = Object.keys(QuestionType);

  topicTestsSharedCollection: ITopicTest[] = [];

  editForm: QuestionFormGroup = this.questionFormService.createQuestionFormGroup();

  constructor(
    protected questionService: QuestionService,
    protected questionFormService: QuestionFormService,
    protected topicTestService: TopicTestService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareTopicTest = (o1: ITopicTest | null, o2: ITopicTest | null): boolean => this.topicTestService.compareTopicTest(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ question }) => {
      this.question = question;
      if (question) {
        this.updateForm(question);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const question = this.questionFormService.getQuestion(this.editForm);
    if (question.id !== null) {
      this.subscribeToSaveResponse(this.questionService.update(question));
    } else {
      this.subscribeToSaveResponse(this.questionService.create(question));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IQuestion>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(question: IQuestion): void {
    this.question = question;
    this.questionFormService.resetForm(this.editForm, question);

    this.topicTestsSharedCollection = this.topicTestService.addTopicTestToCollectionIfMissing<ITopicTest>(
      this.topicTestsSharedCollection,
      question.test
    );
  }

  protected loadRelationshipsOptions(): void {
    this.topicTestService
      .query()
      .pipe(map((res: HttpResponse<ITopicTest[]>) => res.body ?? []))
      .pipe(
        map((topicTests: ITopicTest[]) =>
          this.topicTestService.addTopicTestToCollectionIfMissing<ITopicTest>(topicTests, this.question?.test)
        )
      )
      .subscribe((topicTests: ITopicTest[]) => (this.topicTestsSharedCollection = topicTests));
  }
}
