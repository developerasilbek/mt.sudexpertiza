import { ITopicTest } from 'app/entities/topic-test/topic-test.model';
import { QuestionType } from 'app/entities/enumerations/question-type.model';

export interface IQuestion {
  id: number;
  name?: string | null;
  requiredScore?: number | null;
  type?: QuestionType | null;
  test?: Pick<ITopicTest, 'id' | 'name'> | null;
}

export type NewQuestion = Omit<IQuestion, 'id'> & { id: null };
