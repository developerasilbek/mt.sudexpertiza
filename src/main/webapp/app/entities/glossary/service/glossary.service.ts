import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IGlossary, NewGlossary } from '../glossary.model';

export type PartialUpdateGlossary = Partial<IGlossary> & Pick<IGlossary, 'id'>;

export type EntityResponseType = HttpResponse<IGlossary>;
export type EntityArrayResponseType = HttpResponse<IGlossary[]>;

@Injectable({ providedIn: 'root' })
export class GlossaryService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/glossaries');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(glossary: NewGlossary): Observable<EntityResponseType> {
    return this.http.post<IGlossary>(this.resourceUrl, glossary, { observe: 'response' });
  }

  update(glossary: IGlossary): Observable<EntityResponseType> {
    return this.http.put<IGlossary>(`${this.resourceUrl}/${this.getGlossaryIdentifier(glossary)}`, glossary, { observe: 'response' });
  }

  partialUpdate(glossary: PartialUpdateGlossary): Observable<EntityResponseType> {
    return this.http.patch<IGlossary>(`${this.resourceUrl}/${this.getGlossaryIdentifier(glossary)}`, glossary, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IGlossary>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IGlossary[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getGlossaryIdentifier(glossary: Pick<IGlossary, 'id'>): number {
    return glossary.id;
  }

  compareGlossary(o1: Pick<IGlossary, 'id'> | null, o2: Pick<IGlossary, 'id'> | null): boolean {
    return o1 && o2 ? this.getGlossaryIdentifier(o1) === this.getGlossaryIdentifier(o2) : o1 === o2;
  }

  addGlossaryToCollectionIfMissing<Type extends Pick<IGlossary, 'id'>>(
    glossaryCollection: Type[],
    ...glossariesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const glossaries: Type[] = glossariesToCheck.filter(isPresent);
    if (glossaries.length > 0) {
      const glossaryCollectionIdentifiers = glossaryCollection.map(glossaryItem => this.getGlossaryIdentifier(glossaryItem)!);
      const glossariesToAdd = glossaries.filter(glossaryItem => {
        const glossaryIdentifier = this.getGlossaryIdentifier(glossaryItem);
        if (glossaryCollectionIdentifiers.includes(glossaryIdentifier)) {
          return false;
        }
        glossaryCollectionIdentifiers.push(glossaryIdentifier);
        return true;
      });
      return [...glossariesToAdd, ...glossaryCollection];
    }
    return glossaryCollection;
  }
}
