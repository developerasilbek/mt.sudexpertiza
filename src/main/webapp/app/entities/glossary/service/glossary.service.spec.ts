import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IGlossary } from '../glossary.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../glossary.test-samples';

import { GlossaryService } from './glossary.service';

const requireRestSample: IGlossary = {
  ...sampleWithRequiredData,
};

describe('Glossary Service', () => {
  let service: GlossaryService;
  let httpMock: HttpTestingController;
  let expectedResult: IGlossary | IGlossary[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(GlossaryService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Glossary', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const glossary = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(glossary).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Glossary', () => {
      const glossary = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(glossary).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Glossary', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Glossary', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Glossary', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addGlossaryToCollectionIfMissing', () => {
      it('should add a Glossary to an empty array', () => {
        const glossary: IGlossary = sampleWithRequiredData;
        expectedResult = service.addGlossaryToCollectionIfMissing([], glossary);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(glossary);
      });

      it('should not add a Glossary to an array that contains it', () => {
        const glossary: IGlossary = sampleWithRequiredData;
        const glossaryCollection: IGlossary[] = [
          {
            ...glossary,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addGlossaryToCollectionIfMissing(glossaryCollection, glossary);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Glossary to an array that doesn't contain it", () => {
        const glossary: IGlossary = sampleWithRequiredData;
        const glossaryCollection: IGlossary[] = [sampleWithPartialData];
        expectedResult = service.addGlossaryToCollectionIfMissing(glossaryCollection, glossary);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(glossary);
      });

      it('should add only unique Glossary to an array', () => {
        const glossaryArray: IGlossary[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const glossaryCollection: IGlossary[] = [sampleWithRequiredData];
        expectedResult = service.addGlossaryToCollectionIfMissing(glossaryCollection, ...glossaryArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const glossary: IGlossary = sampleWithRequiredData;
        const glossary2: IGlossary = sampleWithPartialData;
        expectedResult = service.addGlossaryToCollectionIfMissing([], glossary, glossary2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(glossary);
        expect(expectedResult).toContain(glossary2);
      });

      it('should accept null and undefined values', () => {
        const glossary: IGlossary = sampleWithRequiredData;
        expectedResult = service.addGlossaryToCollectionIfMissing([], null, glossary, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(glossary);
      });

      it('should return initial array if no Glossary is added', () => {
        const glossaryCollection: IGlossary[] = [sampleWithRequiredData];
        expectedResult = service.addGlossaryToCollectionIfMissing(glossaryCollection, undefined, null);
        expect(expectedResult).toEqual(glossaryCollection);
      });
    });

    describe('compareGlossary', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareGlossary(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareGlossary(entity1, entity2);
        const compareResult2 = service.compareGlossary(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareGlossary(entity1, entity2);
        const compareResult2 = service.compareGlossary(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareGlossary(entity1, entity2);
        const compareResult2 = service.compareGlossary(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
