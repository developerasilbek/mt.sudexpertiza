import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { GlossaryComponent } from './list/glossary.component';
import { GlossaryDetailComponent } from './detail/glossary-detail.component';
import { GlossaryUpdateComponent } from './update/glossary-update.component';
import { GlossaryDeleteDialogComponent } from './delete/glossary-delete-dialog.component';
import { GlossaryRoutingModule } from './route/glossary-routing.module';

@NgModule({
  imports: [SharedModule, GlossaryRoutingModule],
  declarations: [GlossaryComponent, GlossaryDetailComponent, GlossaryUpdateComponent, GlossaryDeleteDialogComponent],
})
export class GlossaryModule {}
