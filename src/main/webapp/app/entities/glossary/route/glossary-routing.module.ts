import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { GlossaryComponent } from '../list/glossary.component';
import { GlossaryDetailComponent } from '../detail/glossary-detail.component';
import { GlossaryUpdateComponent } from '../update/glossary-update.component';
import { GlossaryRoutingResolveService } from './glossary-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const glossaryRoute: Routes = [
  {
    path: '',
    component: GlossaryComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: GlossaryDetailComponent,
    resolve: {
      glossary: GlossaryRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: GlossaryUpdateComponent,
    resolve: {
      glossary: GlossaryRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: GlossaryUpdateComponent,
    resolve: {
      glossary: GlossaryRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(glossaryRoute)],
  exports: [RouterModule],
})
export class GlossaryRoutingModule {}
