import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IGlossary } from '../glossary.model';
import { GlossaryService } from '../service/glossary.service';

@Injectable({ providedIn: 'root' })
export class GlossaryRoutingResolveService implements Resolve<IGlossary | null> {
  constructor(protected service: GlossaryService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IGlossary | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((glossary: HttpResponse<IGlossary>) => {
          if (glossary.body) {
            return of(glossary.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
