import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../glossary.test-samples';

import { GlossaryFormService } from './glossary-form.service';

describe('Glossary Form Service', () => {
  let service: GlossaryFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GlossaryFormService);
  });

  describe('Service methods', () => {
    describe('createGlossaryFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createGlossaryFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            assessment: expect.any(Object),
          })
        );
      });

      it('passing IGlossary should create a new form with FormGroup', () => {
        const formGroup = service.createGlossaryFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            assessment: expect.any(Object),
          })
        );
      });
    });

    describe('getGlossary', () => {
      it('should return NewGlossary for default Glossary initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createGlossaryFormGroup(sampleWithNewData);

        const glossary = service.getGlossary(formGroup) as any;

        expect(glossary).toMatchObject(sampleWithNewData);
      });

      it('should return NewGlossary for empty Glossary initial value', () => {
        const formGroup = service.createGlossaryFormGroup();

        const glossary = service.getGlossary(formGroup) as any;

        expect(glossary).toMatchObject({});
      });

      it('should return IGlossary', () => {
        const formGroup = service.createGlossaryFormGroup(sampleWithRequiredData);

        const glossary = service.getGlossary(formGroup) as any;

        expect(glossary).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IGlossary should not enable id FormControl', () => {
        const formGroup = service.createGlossaryFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewGlossary should disable id FormControl', () => {
        const formGroup = service.createGlossaryFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
