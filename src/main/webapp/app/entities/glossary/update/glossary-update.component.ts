import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { GlossaryFormService, GlossaryFormGroup } from './glossary-form.service';
import { IGlossary } from '../glossary.model';
import { GlossaryService } from '../service/glossary.service';
import { IAssessment } from 'app/entities/assessment/assessment.model';
import { AssessmentService } from 'app/entities/assessment/service/assessment.service';

@Component({
  selector: 'jhi-glossary-update',
  templateUrl: './glossary-update.component.html',
})
export class GlossaryUpdateComponent implements OnInit {
  isSaving = false;
  glossary: IGlossary | null = null;

  assessmentsSharedCollection: IAssessment[] = [];

  editForm: GlossaryFormGroup = this.glossaryFormService.createGlossaryFormGroup();

  constructor(
    protected glossaryService: GlossaryService,
    protected glossaryFormService: GlossaryFormService,
    protected assessmentService: AssessmentService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareAssessment = (o1: IAssessment | null, o2: IAssessment | null): boolean => this.assessmentService.compareAssessment(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ glossary }) => {
      this.glossary = glossary;
      if (glossary) {
        this.updateForm(glossary);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const glossary = this.glossaryFormService.getGlossary(this.editForm);
    if (glossary.id !== null) {
      this.subscribeToSaveResponse(this.glossaryService.update(glossary));
    } else {
      this.subscribeToSaveResponse(this.glossaryService.create(glossary));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IGlossary>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(glossary: IGlossary): void {
    this.glossary = glossary;
    this.glossaryFormService.resetForm(this.editForm, glossary);

    this.assessmentsSharedCollection = this.assessmentService.addAssessmentToCollectionIfMissing<IAssessment>(
      this.assessmentsSharedCollection,
      glossary.assessment
    );
  }

  protected loadRelationshipsOptions(): void {
    this.assessmentService
      .query()
      .pipe(map((res: HttpResponse<IAssessment[]>) => res.body ?? []))
      .pipe(
        map((assessments: IAssessment[]) =>
          this.assessmentService.addAssessmentToCollectionIfMissing<IAssessment>(assessments, this.glossary?.assessment)
        )
      )
      .subscribe((assessments: IAssessment[]) => (this.assessmentsSharedCollection = assessments));
  }
}
