import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IGlossary, NewGlossary } from '../glossary.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IGlossary for edit and NewGlossaryFormGroupInput for create.
 */
type GlossaryFormGroupInput = IGlossary | PartialWithRequiredKeyOf<NewGlossary>;

type GlossaryFormDefaults = Pick<NewGlossary, 'id'>;

type GlossaryFormGroupContent = {
  id: FormControl<IGlossary['id'] | NewGlossary['id']>;
  name: FormControl<IGlossary['name']>;
  assessment: FormControl<IGlossary['assessment']>;
};

export type GlossaryFormGroup = FormGroup<GlossaryFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class GlossaryFormService {
  createGlossaryFormGroup(glossary: GlossaryFormGroupInput = { id: null }): GlossaryFormGroup {
    const glossaryRawValue = {
      ...this.getFormDefaults(),
      ...glossary,
    };
    return new FormGroup<GlossaryFormGroupContent>({
      id: new FormControl(
        { value: glossaryRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      name: new FormControl(glossaryRawValue.name),
      assessment: new FormControl(glossaryRawValue.assessment),
    });
  }

  getGlossary(form: GlossaryFormGroup): IGlossary | NewGlossary {
    return form.getRawValue() as IGlossary | NewGlossary;
  }

  resetForm(form: GlossaryFormGroup, glossary: GlossaryFormGroupInput): void {
    const glossaryRawValue = { ...this.getFormDefaults(), ...glossary };
    form.reset(
      {
        ...glossaryRawValue,
        id: { value: glossaryRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): GlossaryFormDefaults {
    return {
      id: null,
    };
  }
}
