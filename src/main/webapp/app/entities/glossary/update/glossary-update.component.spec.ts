import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { GlossaryFormService } from './glossary-form.service';
import { GlossaryService } from '../service/glossary.service';
import { IGlossary } from '../glossary.model';
import { IAssessment } from 'app/entities/assessment/assessment.model';
import { AssessmentService } from 'app/entities/assessment/service/assessment.service';

import { GlossaryUpdateComponent } from './glossary-update.component';

describe('Glossary Management Update Component', () => {
  let comp: GlossaryUpdateComponent;
  let fixture: ComponentFixture<GlossaryUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let glossaryFormService: GlossaryFormService;
  let glossaryService: GlossaryService;
  let assessmentService: AssessmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [GlossaryUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(GlossaryUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(GlossaryUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    glossaryFormService = TestBed.inject(GlossaryFormService);
    glossaryService = TestBed.inject(GlossaryService);
    assessmentService = TestBed.inject(AssessmentService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Assessment query and add missing value', () => {
      const glossary: IGlossary = { id: 456 };
      const assessment: IAssessment = { id: 45080 };
      glossary.assessment = assessment;

      const assessmentCollection: IAssessment[] = [{ id: 94841 }];
      jest.spyOn(assessmentService, 'query').mockReturnValue(of(new HttpResponse({ body: assessmentCollection })));
      const additionalAssessments = [assessment];
      const expectedCollection: IAssessment[] = [...additionalAssessments, ...assessmentCollection];
      jest.spyOn(assessmentService, 'addAssessmentToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ glossary });
      comp.ngOnInit();

      expect(assessmentService.query).toHaveBeenCalled();
      expect(assessmentService.addAssessmentToCollectionIfMissing).toHaveBeenCalledWith(
        assessmentCollection,
        ...additionalAssessments.map(expect.objectContaining)
      );
      expect(comp.assessmentsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const glossary: IGlossary = { id: 456 };
      const assessment: IAssessment = { id: 56376 };
      glossary.assessment = assessment;

      activatedRoute.data = of({ glossary });
      comp.ngOnInit();

      expect(comp.assessmentsSharedCollection).toContain(assessment);
      expect(comp.glossary).toEqual(glossary);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IGlossary>>();
      const glossary = { id: 123 };
      jest.spyOn(glossaryFormService, 'getGlossary').mockReturnValue(glossary);
      jest.spyOn(glossaryService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ glossary });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: glossary }));
      saveSubject.complete();

      // THEN
      expect(glossaryFormService.getGlossary).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(glossaryService.update).toHaveBeenCalledWith(expect.objectContaining(glossary));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IGlossary>>();
      const glossary = { id: 123 };
      jest.spyOn(glossaryFormService, 'getGlossary').mockReturnValue({ id: null });
      jest.spyOn(glossaryService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ glossary: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: glossary }));
      saveSubject.complete();

      // THEN
      expect(glossaryFormService.getGlossary).toHaveBeenCalled();
      expect(glossaryService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IGlossary>>();
      const glossary = { id: 123 };
      jest.spyOn(glossaryService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ glossary });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(glossaryService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareAssessment', () => {
      it('Should forward to assessmentService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(assessmentService, 'compareAssessment');
        comp.compareAssessment(entity, entity2);
        expect(assessmentService.compareAssessment).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
