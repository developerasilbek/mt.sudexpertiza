import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IGlossary } from '../glossary.model';

@Component({
  selector: 'jhi-glossary-detail',
  templateUrl: './glossary-detail.component.html',
})
export class GlossaryDetailComponent implements OnInit {
  glossary: IGlossary | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ glossary }) => {
      this.glossary = glossary;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
