import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GlossaryDetailComponent } from './glossary-detail.component';

describe('Glossary Management Detail Component', () => {
  let comp: GlossaryDetailComponent;
  let fixture: ComponentFixture<GlossaryDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GlossaryDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ glossary: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(GlossaryDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(GlossaryDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load glossary on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.glossary).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
