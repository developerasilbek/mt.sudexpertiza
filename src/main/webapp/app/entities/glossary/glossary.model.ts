import { IAssessment } from 'app/entities/assessment/assessment.model';

export interface IGlossary {
  id: number;
  name?: string | null;
  assessment?: Pick<IAssessment, 'id'> | null;
}

export type NewGlossary = Omit<IGlossary, 'id'> & { id: null };
