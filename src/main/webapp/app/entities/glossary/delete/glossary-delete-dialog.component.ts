import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IGlossary } from '../glossary.model';
import { GlossaryService } from '../service/glossary.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './glossary-delete-dialog.component.html',
})
export class GlossaryDeleteDialogComponent {
  glossary?: IGlossary;

  constructor(protected glossaryService: GlossaryService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.glossaryService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
