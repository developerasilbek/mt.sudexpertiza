import { IGlossary, NewGlossary } from './glossary.model';

export const sampleWithRequiredData: IGlossary = {
  id: 30125,
};

export const sampleWithPartialData: IGlossary = {
  id: 69458,
};

export const sampleWithFullData: IGlossary = {
  id: 78799,
  name: 'global Consultant',
};

export const sampleWithNewData: NewGlossary = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
