import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ListenerDetailComponent } from './listener-detail.component';

describe('Listener Management Detail Component', () => {
  let comp: ListenerDetailComponent;
  let fixture: ComponentFixture<ListenerDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ListenerDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ listener: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(ListenerDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ListenerDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load listener on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.listener).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
