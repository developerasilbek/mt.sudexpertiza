import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IListener } from '../listener.model';

@Component({
  selector: 'jhi-listener-detail',
  templateUrl: './listener-detail.component.html',
})
export class ListenerDetailComponent implements OnInit {
  listener: IListener | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ listener }) => {
      this.listener = listener;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
