import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IListener, NewListener } from '../listener.model';

export type PartialUpdateListener = Partial<IListener> & Pick<IListener, 'id'>;

export type EntityResponseType = HttpResponse<IListener>;
export type EntityArrayResponseType = HttpResponse<IListener[]>;

@Injectable({ providedIn: 'root' })
export class ListenerService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/listeners');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(listener: NewListener): Observable<EntityResponseType> {
    return this.http.post<IListener>(this.resourceUrl, listener, { observe: 'response' });
  }

  update(listener: IListener): Observable<EntityResponseType> {
    return this.http.put<IListener>(`${this.resourceUrl}/${this.getListenerIdentifier(listener)}`, listener, { observe: 'response' });
  }

  partialUpdate(listener: PartialUpdateListener): Observable<EntityResponseType> {
    return this.http.patch<IListener>(`${this.resourceUrl}/${this.getListenerIdentifier(listener)}`, listener, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IListener>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IListener[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getListenerIdentifier(listener: Pick<IListener, 'id'>): number {
    return listener.id;
  }

  compareListener(o1: Pick<IListener, 'id'> | null, o2: Pick<IListener, 'id'> | null): boolean {
    return o1 && o2 ? this.getListenerIdentifier(o1) === this.getListenerIdentifier(o2) : o1 === o2;
  }

  addListenerToCollectionIfMissing<Type extends Pick<IListener, 'id'>>(
    listenerCollection: Type[],
    ...listenersToCheck: (Type | null | undefined)[]
  ): Type[] {
    const listeners: Type[] = listenersToCheck.filter(isPresent);
    if (listeners.length > 0) {
      const listenerCollectionIdentifiers = listenerCollection.map(listenerItem => this.getListenerIdentifier(listenerItem)!);
      const listenersToAdd = listeners.filter(listenerItem => {
        const listenerIdentifier = this.getListenerIdentifier(listenerItem);
        if (listenerCollectionIdentifiers.includes(listenerIdentifier)) {
          return false;
        }
        listenerCollectionIdentifiers.push(listenerIdentifier);
        return true;
      });
      return [...listenersToAdd, ...listenerCollection];
    }
    return listenerCollection;
  }
}
