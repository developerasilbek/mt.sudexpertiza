import { ConditionType } from 'app/entities/enumerations/condition-type.model';
import { StudyType } from 'app/entities/enumerations/study-type.model';

import { IListener, NewListener } from './listener.model';

export const sampleWithRequiredData: IListener = {
  id: 14150,
  phone: '1-876-556-0891',
};

export const sampleWithPartialData: IListener = {
  id: 89744,
  phone: '337.665.9094 x3',
  dateOfBirth: 'Pizza',
  condition: ConditionType['FINISHED'],
};

export const sampleWithFullData: IListener = {
  id: 53580,
  phone: '1-285-256-4438 ',
  region: 'Rubber',
  workplace: 'Metal',
  dateOfBirth: 'syndicate',
  condition: ConditionType['UNKNOWN'],
  type: StudyType['SHARTNOMA'],
};

export const sampleWithNewData: NewListener = {
  phone: '535-842-4874',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
