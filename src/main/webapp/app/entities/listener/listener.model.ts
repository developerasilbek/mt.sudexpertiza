import { IUser } from 'app/entities/user/user.model';
import { IAttachment } from 'app/entities/attachment/attachment.model';
import { ICourse } from 'app/entities/course/course.model';
import { ConditionType } from 'app/entities/enumerations/condition-type.model';
import { StudyType } from 'app/entities/enumerations/study-type.model';

export interface IListener {
  id: number;
  phone?: string | null;
  region?: string | null;
  workplace?: string | null;
  dateOfBirth?: string | null;
  condition?: ConditionType | null;
  type?: StudyType | null;
  user?: Pick<IUser, 'id'> | null;
  contract?: Pick<IAttachment, 'id'> | null;
  course?: Pick<ICourse, 'id'> | null;
}

export type NewListener = Omit<IListener, 'id'> & { id: null };
