import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ListenerFormService, ListenerFormGroup } from './listener-form.service';
import { IListener } from '../listener.model';
import { ListenerService } from '../service/listener.service';
import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';
import { IAttachment } from 'app/entities/attachment/attachment.model';
import { AttachmentService } from 'app/entities/attachment/service/attachment.service';
import { ICourse } from 'app/entities/course/course.model';
import { CourseService } from 'app/entities/course/service/course.service';
import { ConditionType } from 'app/entities/enumerations/condition-type.model';
import { StudyType } from 'app/entities/enumerations/study-type.model';

@Component({
  selector: 'jhi-listener-update',
  templateUrl: './listener-update.component.html',
})
export class ListenerUpdateComponent implements OnInit {
  isSaving = false;
  listener: IListener | null = null;
  conditionTypeValues = Object.keys(ConditionType);
  studyTypeValues = Object.keys(StudyType);

  usersSharedCollection: IUser[] = [];
  attachmentsSharedCollection: IAttachment[] = [];
  coursesSharedCollection: ICourse[] = [];

  editForm: ListenerFormGroup = this.listenerFormService.createListenerFormGroup();

  constructor(
    protected listenerService: ListenerService,
    protected listenerFormService: ListenerFormService,
    protected userService: UserService,
    protected attachmentService: AttachmentService,
    protected courseService: CourseService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareUser = (o1: IUser | null, o2: IUser | null): boolean => this.userService.compareUser(o1, o2);

  compareAttachment = (o1: IAttachment | null, o2: IAttachment | null): boolean => this.attachmentService.compareAttachment(o1, o2);

  compareCourse = (o1: ICourse | null, o2: ICourse | null): boolean => this.courseService.compareCourse(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ listener }) => {
      this.listener = listener;
      if (listener) {
        this.updateForm(listener);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const listener = this.listenerFormService.getListener(this.editForm);
    if (listener.id !== null) {
      this.subscribeToSaveResponse(this.listenerService.update(listener));
    } else {
      this.subscribeToSaveResponse(this.listenerService.create(listener));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IListener>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(listener: IListener): void {
    this.listener = listener;
    this.listenerFormService.resetForm(this.editForm, listener);

    this.usersSharedCollection = this.userService.addUserToCollectionIfMissing<IUser>(this.usersSharedCollection, listener.user);
    this.attachmentsSharedCollection = this.attachmentService.addAttachmentToCollectionIfMissing<IAttachment>(
      this.attachmentsSharedCollection,
      listener.contract
    );
    this.coursesSharedCollection = this.courseService.addCourseToCollectionIfMissing<ICourse>(
      this.coursesSharedCollection,
      listener.course
    );
  }

  protected loadRelationshipsOptions(): void {
    this.userService
      .query()
      .pipe(map((res: HttpResponse<IUser[]>) => res.body ?? []))
      .pipe(map((users: IUser[]) => this.userService.addUserToCollectionIfMissing<IUser>(users, this.listener?.user)))
      .subscribe((users: IUser[]) => (this.usersSharedCollection = users));

    this.attachmentService
      .query()
      .pipe(map((res: HttpResponse<IAttachment[]>) => res.body ?? []))
      .pipe(
        map((attachments: IAttachment[]) =>
          this.attachmentService.addAttachmentToCollectionIfMissing<IAttachment>(attachments, this.listener?.contract)
        )
      )
      .subscribe((attachments: IAttachment[]) => (this.attachmentsSharedCollection = attachments));

    this.courseService
      .query()
      .pipe(map((res: HttpResponse<ICourse[]>) => res.body ?? []))
      .pipe(map((courses: ICourse[]) => this.courseService.addCourseToCollectionIfMissing<ICourse>(courses, this.listener?.course)))
      .subscribe((courses: ICourse[]) => (this.coursesSharedCollection = courses));
  }
}
