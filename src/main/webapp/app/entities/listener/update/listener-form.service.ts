import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IListener, NewListener } from '../listener.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IListener for edit and NewListenerFormGroupInput for create.
 */
type ListenerFormGroupInput = IListener | PartialWithRequiredKeyOf<NewListener>;

type ListenerFormDefaults = Pick<NewListener, 'id'>;

type ListenerFormGroupContent = {
  id: FormControl<IListener['id'] | NewListener['id']>;
  phone: FormControl<IListener['phone']>;
  region: FormControl<IListener['region']>;
  workplace: FormControl<IListener['workplace']>;
  dateOfBirth: FormControl<IListener['dateOfBirth']>;
  condition: FormControl<IListener['condition']>;
  type: FormControl<IListener['type']>;
  user: FormControl<IListener['user']>;
  contract: FormControl<IListener['contract']>;
  course: FormControl<IListener['course']>;
};

export type ListenerFormGroup = FormGroup<ListenerFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class ListenerFormService {
  createListenerFormGroup(listener: ListenerFormGroupInput = { id: null }): ListenerFormGroup {
    const listenerRawValue = {
      ...this.getFormDefaults(),
      ...listener,
    };
    return new FormGroup<ListenerFormGroupContent>({
      id: new FormControl(
        { value: listenerRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      phone: new FormControl(listenerRawValue.phone, {
        validators: [Validators.required, Validators.maxLength(15)],
      }),
      region: new FormControl(listenerRawValue.region),
      workplace: new FormControl(listenerRawValue.workplace),
      dateOfBirth: new FormControl(listenerRawValue.dateOfBirth),
      condition: new FormControl(listenerRawValue.condition),
      type: new FormControl(listenerRawValue.type),
      user: new FormControl(listenerRawValue.user),
      contract: new FormControl(listenerRawValue.contract),
      course: new FormControl(listenerRawValue.course),
    });
  }

  getListener(form: ListenerFormGroup): IListener | NewListener {
    return form.getRawValue() as IListener | NewListener;
  }

  resetForm(form: ListenerFormGroup, listener: ListenerFormGroupInput): void {
    const listenerRawValue = { ...this.getFormDefaults(), ...listener };
    form.reset(
      {
        ...listenerRawValue,
        id: { value: listenerRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): ListenerFormDefaults {
    return {
      id: null,
    };
  }
}
