import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../listener.test-samples';

import { ListenerFormService } from './listener-form.service';

describe('Listener Form Service', () => {
  let service: ListenerFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListenerFormService);
  });

  describe('Service methods', () => {
    describe('createListenerFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createListenerFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            phone: expect.any(Object),
            region: expect.any(Object),
            workplace: expect.any(Object),
            dateOfBirth: expect.any(Object),
            condition: expect.any(Object),
            type: expect.any(Object),
            user: expect.any(Object),
            contract: expect.any(Object),
            course: expect.any(Object),
          })
        );
      });

      it('passing IListener should create a new form with FormGroup', () => {
        const formGroup = service.createListenerFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            phone: expect.any(Object),
            region: expect.any(Object),
            workplace: expect.any(Object),
            dateOfBirth: expect.any(Object),
            condition: expect.any(Object),
            type: expect.any(Object),
            user: expect.any(Object),
            contract: expect.any(Object),
            course: expect.any(Object),
          })
        );
      });
    });

    describe('getListener', () => {
      it('should return NewListener for default Listener initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createListenerFormGroup(sampleWithNewData);

        const listener = service.getListener(formGroup) as any;

        expect(listener).toMatchObject(sampleWithNewData);
      });

      it('should return NewListener for empty Listener initial value', () => {
        const formGroup = service.createListenerFormGroup();

        const listener = service.getListener(formGroup) as any;

        expect(listener).toMatchObject({});
      });

      it('should return IListener', () => {
        const formGroup = service.createListenerFormGroup(sampleWithRequiredData);

        const listener = service.getListener(formGroup) as any;

        expect(listener).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IListener should not enable id FormControl', () => {
        const formGroup = service.createListenerFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewListener should disable id FormControl', () => {
        const formGroup = service.createListenerFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
