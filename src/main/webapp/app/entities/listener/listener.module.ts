import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ListenerComponent } from './list/listener.component';
import { ListenerDetailComponent } from './detail/listener-detail.component';
import { ListenerUpdateComponent } from './update/listener-update.component';
import { ListenerDeleteDialogComponent } from './delete/listener-delete-dialog.component';
import { ListenerRoutingModule } from './route/listener-routing.module';

@NgModule({
  imports: [SharedModule, ListenerRoutingModule],
  declarations: [ListenerComponent, ListenerDetailComponent, ListenerUpdateComponent, ListenerDeleteDialogComponent],
})
export class ListenerModule {}
