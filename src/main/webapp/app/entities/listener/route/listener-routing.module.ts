import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ListenerComponent } from '../list/listener.component';
import { ListenerDetailComponent } from '../detail/listener-detail.component';
import { ListenerUpdateComponent } from '../update/listener-update.component';
import { ListenerRoutingResolveService } from './listener-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const listenerRoute: Routes = [
  {
    path: '',
    component: ListenerComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ListenerDetailComponent,
    resolve: {
      listener: ListenerRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ListenerUpdateComponent,
    resolve: {
      listener: ListenerRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ListenerUpdateComponent,
    resolve: {
      listener: ListenerRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(listenerRoute)],
  exports: [RouterModule],
})
export class ListenerRoutingModule {}
