import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IListener } from '../listener.model';
import { ListenerService } from '../service/listener.service';

@Injectable({ providedIn: 'root' })
export class ListenerRoutingResolveService implements Resolve<IListener | null> {
  constructor(protected service: ListenerService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IListener | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((listener: HttpResponse<IListener>) => {
          if (listener.body) {
            return of(listener.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
