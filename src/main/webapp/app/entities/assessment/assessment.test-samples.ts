import { AssessmentType } from 'app/entities/enumerations/assessment-type.model';

import { IAssessment, NewAssessment } from './assessment.model';

export const sampleWithRequiredData: IAssessment = {
  id: 82522,
};

export const sampleWithPartialData: IAssessment = {
  id: 38578,
};

export const sampleWithFullData: IAssessment = {
  id: 68788,
  type: AssessmentType['TOPIC_TEST'],
};

export const sampleWithNewData: NewAssessment = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
