import { ITopic } from 'app/entities/topic/topic.model';
import { AssessmentType } from 'app/entities/enumerations/assessment-type.model';

export interface IAssessment {
  id: number;
  type?: AssessmentType | null;
  topic?: Pick<ITopic, 'id'> | null;
}

export type NewAssessment = Omit<IAssessment, 'id'> & { id: null };
