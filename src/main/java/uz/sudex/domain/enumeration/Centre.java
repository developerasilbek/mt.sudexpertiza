package uz.sudex.domain.enumeration;

/**
 * Centre: RSEM (Respublika Sud Expertiza markazi)
 */
public enum Centre {
    RSEM,
}
