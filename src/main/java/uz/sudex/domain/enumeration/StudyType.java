package uz.sudex.domain.enumeration;

/**
 * StudyType: SHARTNOMA, REJA_BUYICHA
 */
public enum StudyType {
    SHARTNOMA,
    REJA_BUYICHA,
}
