package uz.sudex.domain.enumeration;

/**
 * QuestionType: SINGLE_ANSWER, MULTIPLE_ANSWER
 */
public enum QuestionType {
    SINGLE_ANSWER,
    MULTIPLE_ANSWER,
    UNKNOWN,
}
