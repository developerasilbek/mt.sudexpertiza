package uz.sudex.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;
import uz.sudex.domain.*; // for static metamodels
import uz.sudex.domain.Links;
import uz.sudex.repository.LinksRepository;
import uz.sudex.service.criteria.LinksCriteria;
import uz.sudex.service.dto.LinksDTO;
import uz.sudex.service.mapper.LinksMapper;

/**
 * Service for executing complex queries for {@link Links} entities in the database.
 * The main input is a {@link LinksCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link LinksDTO} or a {@link Page} of {@link LinksDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class LinksQueryService extends QueryService<Links> {

    private final Logger log = LoggerFactory.getLogger(LinksQueryService.class);

    private final LinksRepository linksRepository;

    private final LinksMapper linksMapper;

    public LinksQueryService(LinksRepository linksRepository, LinksMapper linksMapper) {
        this.linksRepository = linksRepository;
        this.linksMapper = linksMapper;
    }

    /**
     * Return a {@link List} of {@link LinksDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<LinksDTO> findByCriteria(LinksCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Links> specification = createSpecification(criteria);
        return linksMapper.toDto(linksRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link LinksDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<LinksDTO> findByCriteria(LinksCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Links> specification = createSpecification(criteria);
        return linksRepository.findAll(specification, page).map(linksMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(LinksCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Links> specification = createSpecification(criteria);
        return linksRepository.count(specification);
    }

    /**
     * Function to convert {@link LinksCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Links> createSpecification(LinksCriteria criteria) {
        Specification<Links> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Links_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Links_.name));
            }
            if (criteria.getAssessmentId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getAssessmentId(),
                            root -> root.join(Links_.assessment, JoinType.LEFT).get(Assessment_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
