package uz.sudex.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;
import uz.sudex.domain.*; // for static metamodels
import uz.sudex.domain.TopicTest;
import uz.sudex.repository.TopicTestRepository;
import uz.sudex.service.criteria.TopicTestCriteria;
import uz.sudex.service.dto.TopicTestDTO;
import uz.sudex.service.mapper.TopicTestMapper;

/**
 * Service for executing complex queries for {@link TopicTest} entities in the database.
 * The main input is a {@link TopicTestCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TopicTestDTO} or a {@link Page} of {@link TopicTestDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TopicTestQueryService extends QueryService<TopicTest> {

    private final Logger log = LoggerFactory.getLogger(TopicTestQueryService.class);

    private final TopicTestRepository topicTestRepository;

    private final TopicTestMapper topicTestMapper;

    public TopicTestQueryService(TopicTestRepository topicTestRepository, TopicTestMapper topicTestMapper) {
        this.topicTestRepository = topicTestRepository;
        this.topicTestMapper = topicTestMapper;
    }

    /**
     * Return a {@link List} of {@link TopicTestDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TopicTestDTO> findByCriteria(TopicTestCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TopicTest> specification = createSpecification(criteria);
        return topicTestMapper.toDto(topicTestRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TopicTestDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TopicTestDTO> findByCriteria(TopicTestCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TopicTest> specification = createSpecification(criteria);
        return topicTestRepository.findAll(specification, page).map(topicTestMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TopicTestCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TopicTest> specification = createSpecification(criteria);
        return topicTestRepository.count(specification);
    }

    /**
     * Function to convert {@link TopicTestCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TopicTest> createSpecification(TopicTestCriteria criteria) {
        Specification<TopicTest> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TopicTest_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), TopicTest_.name));
            }
            if (criteria.getAssessmentId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getAssessmentId(),
                            root -> root.join(TopicTest_.assessment, JoinType.LEFT).get(Assessment_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
