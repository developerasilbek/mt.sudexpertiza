package uz.sudex.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;
import uz.sudex.domain.*; // for static metamodels
import uz.sudex.domain.TestResult;
import uz.sudex.repository.TestResultRepository;
import uz.sudex.service.criteria.TestResultCriteria;
import uz.sudex.service.dto.TestResultDTO;
import uz.sudex.service.mapper.TestResultMapper;

/**
 * Service for executing complex queries for {@link TestResult} entities in the database.
 * The main input is a {@link TestResultCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TestResultDTO} or a {@link Page} of {@link TestResultDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TestResultQueryService extends QueryService<TestResult> {

    private final Logger log = LoggerFactory.getLogger(TestResultQueryService.class);

    private final TestResultRepository testResultRepository;

    private final TestResultMapper testResultMapper;

    public TestResultQueryService(TestResultRepository testResultRepository, TestResultMapper testResultMapper) {
        this.testResultRepository = testResultRepository;
        this.testResultMapper = testResultMapper;
    }

    /**
     * Return a {@link List} of {@link TestResultDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TestResultDTO> findByCriteria(TestResultCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TestResult> specification = createSpecification(criteria);
        return testResultMapper.toDto(testResultRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TestResultDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TestResultDTO> findByCriteria(TestResultCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TestResult> specification = createSpecification(criteria);
        return testResultRepository.findAll(specification, page).map(testResultMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TestResultCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TestResult> specification = createSpecification(criteria);
        return testResultRepository.count(specification);
    }

    /**
     * Function to convert {@link TestResultCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TestResult> createSpecification(TestResultCriteria criteria) {
        Specification<TestResult> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TestResult_.id));
            }
            if (criteria.getStartedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStartedAt(), TestResult_.startedAt));
            }
            if (criteria.getFinishedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFinishedAt(), TestResult_.finishedAt));
            }
            if (criteria.getScore() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getScore(), TestResult_.score));
            }
            if (criteria.getCorrectAnswers() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCorrectAnswers(), TestResult_.correctAnswers));
            }
            if (criteria.getTestId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getTestId(), root -> root.join(TestResult_.test, JoinType.LEFT).get(TopicTest_.id))
                    );
            }
        }
        return specification;
    }
}
