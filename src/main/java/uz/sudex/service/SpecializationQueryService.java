package uz.sudex.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;
import uz.sudex.domain.*; // for static metamodels
import uz.sudex.domain.Specialization;
import uz.sudex.repository.SpecializationRepository;
import uz.sudex.service.criteria.SpecializationCriteria;
import uz.sudex.service.dto.SpecializationDTO;
import uz.sudex.service.mapper.SpecializationMapper;

/**
 * Service for executing complex queries for {@link Specialization} entities in the database.
 * The main input is a {@link SpecializationCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SpecializationDTO} or a {@link Page} of {@link SpecializationDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SpecializationQueryService extends QueryService<Specialization> {

    private final Logger log = LoggerFactory.getLogger(SpecializationQueryService.class);

    private final SpecializationRepository specializationRepository;

    private final SpecializationMapper specializationMapper;

    public SpecializationQueryService(SpecializationRepository specializationRepository, SpecializationMapper specializationMapper) {
        this.specializationRepository = specializationRepository;
        this.specializationMapper = specializationMapper;
    }

    /**
     * Return a {@link List} of {@link SpecializationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SpecializationDTO> findByCriteria(SpecializationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Specialization> specification = createSpecification(criteria);
        return specializationMapper.toDto(specializationRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SpecializationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SpecializationDTO> findByCriteria(SpecializationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Specialization> specification = createSpecification(criteria);
        return specializationRepository.findAll(specification, page).map(specializationMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SpecializationCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Specialization> specification = createSpecification(criteria);
        return specializationRepository.count(specification);
    }

    /**
     * Function to convert {@link SpecializationCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Specialization> createSpecification(SpecializationCriteria criteria) {
        Specification<Specialization> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Specialization_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Specialization_.name));
            }
            if (criteria.getCentre() != null) {
                specification = specification.and(buildSpecification(criteria.getCentre(), Specialization_.centre));
            }
        }
        return specification;
    }
}
