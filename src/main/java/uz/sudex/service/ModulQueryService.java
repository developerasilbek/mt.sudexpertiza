package uz.sudex.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;
import uz.sudex.domain.*; // for static metamodels
import uz.sudex.domain.Modul;
import uz.sudex.repository.ModulRepository;
import uz.sudex.service.criteria.ModulCriteria;
import uz.sudex.service.dto.ModulDTO;
import uz.sudex.service.mapper.ModulMapper;

/**
 * Service for executing complex queries for {@link Modul} entities in the database.
 * The main input is a {@link ModulCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ModulDTO} or a {@link Page} of {@link ModulDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ModulQueryService extends QueryService<Modul> {

    private final Logger log = LoggerFactory.getLogger(ModulQueryService.class);

    private final ModulRepository modulRepository;

    private final ModulMapper modulMapper;

    public ModulQueryService(ModulRepository modulRepository, ModulMapper modulMapper) {
        this.modulRepository = modulRepository;
        this.modulMapper = modulMapper;
    }

    /**
     * Return a {@link List} of {@link ModulDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ModulDTO> findByCriteria(ModulCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Modul> specification = createSpecification(criteria);
        return modulMapper.toDto(modulRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ModulDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ModulDTO> findByCriteria(ModulCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Modul> specification = createSpecification(criteria);
        return modulRepository.findAll(specification, page).map(modulMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ModulCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Modul> specification = createSpecification(criteria);
        return modulRepository.count(specification);
    }

    /**
     * Function to convert {@link ModulCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Modul> createSpecification(ModulCriteria criteria) {
        Specification<Modul> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Modul_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Modul_.name));
            }
            if (criteria.getDecription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDecription(), Modul_.decription));
            }
            if (criteria.getBlockId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getBlockId(), root -> root.join(Modul_.block, JoinType.LEFT).get(Block_.id))
                    );
            }
        }
        return specification;
    }
}
