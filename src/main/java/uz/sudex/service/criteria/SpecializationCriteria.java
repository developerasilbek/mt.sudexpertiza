package uz.sudex.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;
import uz.sudex.domain.enumeration.Centre;

/**
 * Criteria class for the {@link uz.sudex.domain.Specialization} entity. This class is used
 * in {@link uz.sudex.web.rest.SpecializationResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /specializations?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class SpecializationCriteria implements Serializable, Criteria {

    /**
     * Class for filtering Centre
     */
    public static class CentreFilter extends Filter<Centre> {

        public CentreFilter() {}

        public CentreFilter(CentreFilter filter) {
            super(filter);
        }

        @Override
        public CentreFilter copy() {
            return new CentreFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private CentreFilter centre;

    private Boolean distinct;

    public SpecializationCriteria() {}

    public SpecializationCriteria(SpecializationCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.centre = other.centre == null ? null : other.centre.copy();
        this.distinct = other.distinct;
    }

    @Override
    public SpecializationCriteria copy() {
        return new SpecializationCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public CentreFilter getCentre() {
        return centre;
    }

    public CentreFilter centre() {
        if (centre == null) {
            centre = new CentreFilter();
        }
        return centre;
    }

    public void setCentre(CentreFilter centre) {
        this.centre = centre;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SpecializationCriteria that = (SpecializationCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(centre, that.centre) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, centre, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SpecializationCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (centre != null ? "centre=" + centre + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
