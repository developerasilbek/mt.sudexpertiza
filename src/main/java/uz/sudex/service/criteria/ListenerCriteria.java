package uz.sudex.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;
import uz.sudex.domain.enumeration.ConditionType;
import uz.sudex.domain.enumeration.StudyType;

/**
 * Criteria class for the {@link uz.sudex.domain.Listener} entity. This class is used
 * in {@link uz.sudex.web.rest.ListenerResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /listeners?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ListenerCriteria implements Serializable, Criteria {

    /**
     * Class for filtering ConditionType
     */
    public static class ConditionTypeFilter extends Filter<ConditionType> {

        public ConditionTypeFilter() {}

        public ConditionTypeFilter(ConditionTypeFilter filter) {
            super(filter);
        }

        @Override
        public ConditionTypeFilter copy() {
            return new ConditionTypeFilter(this);
        }
    }

    /**
     * Class for filtering StudyType
     */
    public static class StudyTypeFilter extends Filter<StudyType> {

        public StudyTypeFilter() {}

        public StudyTypeFilter(StudyTypeFilter filter) {
            super(filter);
        }

        @Override
        public StudyTypeFilter copy() {
            return new StudyTypeFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter phone;

    private StringFilter region;

    private StringFilter workplace;

    private StringFilter dateOfBirth;

    private ConditionTypeFilter condition;

    private StudyTypeFilter type;

    private LongFilter userId;

    private LongFilter contractId;

    private LongFilter courseId;

    private Boolean distinct;

    public ListenerCriteria() {}

    public ListenerCriteria(ListenerCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.phone = other.phone == null ? null : other.phone.copy();
        this.region = other.region == null ? null : other.region.copy();
        this.workplace = other.workplace == null ? null : other.workplace.copy();
        this.dateOfBirth = other.dateOfBirth == null ? null : other.dateOfBirth.copy();
        this.condition = other.condition == null ? null : other.condition.copy();
        this.type = other.type == null ? null : other.type.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.contractId = other.contractId == null ? null : other.contractId.copy();
        this.courseId = other.courseId == null ? null : other.courseId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public ListenerCriteria copy() {
        return new ListenerCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getPhone() {
        return phone;
    }

    public StringFilter phone() {
        if (phone == null) {
            phone = new StringFilter();
        }
        return phone;
    }

    public void setPhone(StringFilter phone) {
        this.phone = phone;
    }

    public StringFilter getRegion() {
        return region;
    }

    public StringFilter region() {
        if (region == null) {
            region = new StringFilter();
        }
        return region;
    }

    public void setRegion(StringFilter region) {
        this.region = region;
    }

    public StringFilter getWorkplace() {
        return workplace;
    }

    public StringFilter workplace() {
        if (workplace == null) {
            workplace = new StringFilter();
        }
        return workplace;
    }

    public void setWorkplace(StringFilter workplace) {
        this.workplace = workplace;
    }

    public StringFilter getDateOfBirth() {
        return dateOfBirth;
    }

    public StringFilter dateOfBirth() {
        if (dateOfBirth == null) {
            dateOfBirth = new StringFilter();
        }
        return dateOfBirth;
    }

    public void setDateOfBirth(StringFilter dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public ConditionTypeFilter getCondition() {
        return condition;
    }

    public ConditionTypeFilter condition() {
        if (condition == null) {
            condition = new ConditionTypeFilter();
        }
        return condition;
    }

    public void setCondition(ConditionTypeFilter condition) {
        this.condition = condition;
    }

    public StudyTypeFilter getType() {
        return type;
    }

    public StudyTypeFilter type() {
        if (type == null) {
            type = new StudyTypeFilter();
        }
        return type;
    }

    public void setType(StudyTypeFilter type) {
        this.type = type;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public LongFilter userId() {
        if (userId == null) {
            userId = new LongFilter();
        }
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getContractId() {
        return contractId;
    }

    public LongFilter contractId() {
        if (contractId == null) {
            contractId = new LongFilter();
        }
        return contractId;
    }

    public void setContractId(LongFilter contractId) {
        this.contractId = contractId;
    }

    public LongFilter getCourseId() {
        return courseId;
    }

    public LongFilter courseId() {
        if (courseId == null) {
            courseId = new LongFilter();
        }
        return courseId;
    }

    public void setCourseId(LongFilter courseId) {
        this.courseId = courseId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ListenerCriteria that = (ListenerCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(phone, that.phone) &&
            Objects.equals(region, that.region) &&
            Objects.equals(workplace, that.workplace) &&
            Objects.equals(dateOfBirth, that.dateOfBirth) &&
            Objects.equals(condition, that.condition) &&
            Objects.equals(type, that.type) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(contractId, that.contractId) &&
            Objects.equals(courseId, that.courseId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, phone, region, workplace, dateOfBirth, condition, type, userId, contractId, courseId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ListenerCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (phone != null ? "phone=" + phone + ", " : "") +
            (region != null ? "region=" + region + ", " : "") +
            (workplace != null ? "workplace=" + workplace + ", " : "") +
            (dateOfBirth != null ? "dateOfBirth=" + dateOfBirth + ", " : "") +
            (condition != null ? "condition=" + condition + ", " : "") +
            (type != null ? "type=" + type + ", " : "") +
            (userId != null ? "userId=" + userId + ", " : "") +
            (contractId != null ? "contractId=" + contractId + ", " : "") +
            (courseId != null ? "courseId=" + courseId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
