package uz.sudex.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;
import uz.sudex.domain.enumeration.QuestionType;

/**
 * Criteria class for the {@link uz.sudex.domain.Question} entity. This class is used
 * in {@link uz.sudex.web.rest.QuestionResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /questions?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class QuestionCriteria implements Serializable, Criteria {

    /**
     * Class for filtering QuestionType
     */
    public static class QuestionTypeFilter extends Filter<QuestionType> {

        public QuestionTypeFilter() {}

        public QuestionTypeFilter(QuestionTypeFilter filter) {
            super(filter);
        }

        @Override
        public QuestionTypeFilter copy() {
            return new QuestionTypeFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private IntegerFilter requiredScore;

    private QuestionTypeFilter type;

    private LongFilter testId;

    private Boolean distinct;

    public QuestionCriteria() {}

    public QuestionCriteria(QuestionCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.requiredScore = other.requiredScore == null ? null : other.requiredScore.copy();
        this.type = other.type == null ? null : other.type.copy();
        this.testId = other.testId == null ? null : other.testId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public QuestionCriteria copy() {
        return new QuestionCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public IntegerFilter getRequiredScore() {
        return requiredScore;
    }

    public IntegerFilter requiredScore() {
        if (requiredScore == null) {
            requiredScore = new IntegerFilter();
        }
        return requiredScore;
    }

    public void setRequiredScore(IntegerFilter requiredScore) {
        this.requiredScore = requiredScore;
    }

    public QuestionTypeFilter getType() {
        return type;
    }

    public QuestionTypeFilter type() {
        if (type == null) {
            type = new QuestionTypeFilter();
        }
        return type;
    }

    public void setType(QuestionTypeFilter type) {
        this.type = type;
    }

    public LongFilter getTestId() {
        return testId;
    }

    public LongFilter testId() {
        if (testId == null) {
            testId = new LongFilter();
        }
        return testId;
    }

    public void setTestId(LongFilter testId) {
        this.testId = testId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final QuestionCriteria that = (QuestionCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(requiredScore, that.requiredScore) &&
            Objects.equals(type, that.type) &&
            Objects.equals(testId, that.testId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, requiredScore, type, testId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "QuestionCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (requiredScore != null ? "requiredScore=" + requiredScore + ", " : "") +
            (type != null ? "type=" + type + ", " : "") +
            (testId != null ? "testId=" + testId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
