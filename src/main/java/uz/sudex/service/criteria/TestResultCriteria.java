package uz.sudex.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link uz.sudex.domain.TestResult} entity. This class is used
 * in {@link uz.sudex.web.rest.TestResultResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /test-results?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class TestResultCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private InstantFilter startedAt;

    private InstantFilter finishedAt;

    private IntegerFilter score;

    private IntegerFilter correctAnswers;

    private LongFilter testId;

    private Boolean distinct;

    public TestResultCriteria() {}

    public TestResultCriteria(TestResultCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.startedAt = other.startedAt == null ? null : other.startedAt.copy();
        this.finishedAt = other.finishedAt == null ? null : other.finishedAt.copy();
        this.score = other.score == null ? null : other.score.copy();
        this.correctAnswers = other.correctAnswers == null ? null : other.correctAnswers.copy();
        this.testId = other.testId == null ? null : other.testId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public TestResultCriteria copy() {
        return new TestResultCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public InstantFilter getStartedAt() {
        return startedAt;
    }

    public InstantFilter startedAt() {
        if (startedAt == null) {
            startedAt = new InstantFilter();
        }
        return startedAt;
    }

    public void setStartedAt(InstantFilter startedAt) {
        this.startedAt = startedAt;
    }

    public InstantFilter getFinishedAt() {
        return finishedAt;
    }

    public InstantFilter finishedAt() {
        if (finishedAt == null) {
            finishedAt = new InstantFilter();
        }
        return finishedAt;
    }

    public void setFinishedAt(InstantFilter finishedAt) {
        this.finishedAt = finishedAt;
    }

    public IntegerFilter getScore() {
        return score;
    }

    public IntegerFilter score() {
        if (score == null) {
            score = new IntegerFilter();
        }
        return score;
    }

    public void setScore(IntegerFilter score) {
        this.score = score;
    }

    public IntegerFilter getCorrectAnswers() {
        return correctAnswers;
    }

    public IntegerFilter correctAnswers() {
        if (correctAnswers == null) {
            correctAnswers = new IntegerFilter();
        }
        return correctAnswers;
    }

    public void setCorrectAnswers(IntegerFilter correctAnswers) {
        this.correctAnswers = correctAnswers;
    }

    public LongFilter getTestId() {
        return testId;
    }

    public LongFilter testId() {
        if (testId == null) {
            testId = new LongFilter();
        }
        return testId;
    }

    public void setTestId(LongFilter testId) {
        this.testId = testId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TestResultCriteria that = (TestResultCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(startedAt, that.startedAt) &&
            Objects.equals(finishedAt, that.finishedAt) &&
            Objects.equals(score, that.score) &&
            Objects.equals(correctAnswers, that.correctAnswers) &&
            Objects.equals(testId, that.testId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, startedAt, finishedAt, score, correctAnswers, testId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TestResultCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (startedAt != null ? "startedAt=" + startedAt + ", " : "") +
            (finishedAt != null ? "finishedAt=" + finishedAt + ", " : "") +
            (score != null ? "score=" + score + ", " : "") +
            (correctAnswers != null ? "correctAnswers=" + correctAnswers + ", " : "") +
            (testId != null ? "testId=" + testId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
