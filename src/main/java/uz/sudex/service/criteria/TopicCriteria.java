package uz.sudex.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link uz.sudex.domain.Topic} entity. This class is used
 * in {@link uz.sudex.web.rest.TopicResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /topics?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class TopicCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private IntegerFilter priority;

    private IntegerFilter hour;

    private LongFilter fileId;

    private LongFilter modulId;

    private Boolean distinct;

    public TopicCriteria() {}

    public TopicCriteria(TopicCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.priority = other.priority == null ? null : other.priority.copy();
        this.hour = other.hour == null ? null : other.hour.copy();
        this.fileId = other.fileId == null ? null : other.fileId.copy();
        this.modulId = other.modulId == null ? null : other.modulId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public TopicCriteria copy() {
        return new TopicCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public IntegerFilter getPriority() {
        return priority;
    }

    public IntegerFilter priority() {
        if (priority == null) {
            priority = new IntegerFilter();
        }
        return priority;
    }

    public void setPriority(IntegerFilter priority) {
        this.priority = priority;
    }

    public IntegerFilter getHour() {
        return hour;
    }

    public IntegerFilter hour() {
        if (hour == null) {
            hour = new IntegerFilter();
        }
        return hour;
    }

    public void setHour(IntegerFilter hour) {
        this.hour = hour;
    }

    public LongFilter getFileId() {
        return fileId;
    }

    public LongFilter fileId() {
        if (fileId == null) {
            fileId = new LongFilter();
        }
        return fileId;
    }

    public void setFileId(LongFilter fileId) {
        this.fileId = fileId;
    }

    public LongFilter getModulId() {
        return modulId;
    }

    public LongFilter modulId() {
        if (modulId == null) {
            modulId = new LongFilter();
        }
        return modulId;
    }

    public void setModulId(LongFilter modulId) {
        this.modulId = modulId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TopicCriteria that = (TopicCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(priority, that.priority) &&
            Objects.equals(hour, that.hour) &&
            Objects.equals(fileId, that.fileId) &&
            Objects.equals(modulId, that.modulId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, priority, hour, fileId, modulId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TopicCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (priority != null ? "priority=" + priority + ", " : "") +
            (hour != null ? "hour=" + hour + ", " : "") +
            (fileId != null ? "fileId=" + fileId + ", " : "") +
            (modulId != null ? "modulId=" + modulId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
