package uz.sudex.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;
import uz.sudex.domain.enumeration.AssessmentType;

/**
 * Criteria class for the {@link uz.sudex.domain.Assessment} entity. This class is used
 * in {@link uz.sudex.web.rest.AssessmentResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /assessments?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class AssessmentCriteria implements Serializable, Criteria {

    /**
     * Class for filtering AssessmentType
     */
    public static class AssessmentTypeFilter extends Filter<AssessmentType> {

        public AssessmentTypeFilter() {}

        public AssessmentTypeFilter(AssessmentTypeFilter filter) {
            super(filter);
        }

        @Override
        public AssessmentTypeFilter copy() {
            return new AssessmentTypeFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private AssessmentTypeFilter type;

    private LongFilter topicId;

    private Boolean distinct;

    public AssessmentCriteria() {}

    public AssessmentCriteria(AssessmentCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.type = other.type == null ? null : other.type.copy();
        this.topicId = other.topicId == null ? null : other.topicId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public AssessmentCriteria copy() {
        return new AssessmentCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public AssessmentTypeFilter getType() {
        return type;
    }

    public AssessmentTypeFilter type() {
        if (type == null) {
            type = new AssessmentTypeFilter();
        }
        return type;
    }

    public void setType(AssessmentTypeFilter type) {
        this.type = type;
    }

    public LongFilter getTopicId() {
        return topicId;
    }

    public LongFilter topicId() {
        if (topicId == null) {
            topicId = new LongFilter();
        }
        return topicId;
    }

    public void setTopicId(LongFilter topicId) {
        this.topicId = topicId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AssessmentCriteria that = (AssessmentCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(type, that.type) &&
            Objects.equals(topicId, that.topicId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, topicId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AssessmentCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (type != null ? "type=" + type + ", " : "") +
            (topicId != null ? "topicId=" + topicId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
