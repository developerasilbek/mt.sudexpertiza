package uz.sudex.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;
import uz.sudex.domain.enumeration.Centre;

/**
 * Criteria class for the {@link uz.sudex.domain.Course} entity. This class is used
 * in {@link uz.sudex.web.rest.CourseResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /courses?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CourseCriteria implements Serializable, Criteria {

    /**
     * Class for filtering Centre
     */
    public static class CentreFilter extends Filter<Centre> {

        public CentreFilter() {}

        public CentreFilter(CentreFilter filter) {
            super(filter);
        }

        @Override
        public CentreFilter copy() {
            return new CentreFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private CentreFilter centre;

    private StringFilter language;

    private IntegerFilter teachingHour;

    private InstantFilter startedAt;

    private InstantFilter finishedAt;

    private Boolean distinct;

    public CourseCriteria() {}

    public CourseCriteria(CourseCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.centre = other.centre == null ? null : other.centre.copy();
        this.language = other.language == null ? null : other.language.copy();
        this.teachingHour = other.teachingHour == null ? null : other.teachingHour.copy();
        this.startedAt = other.startedAt == null ? null : other.startedAt.copy();
        this.finishedAt = other.finishedAt == null ? null : other.finishedAt.copy();
        this.distinct = other.distinct;
    }

    @Override
    public CourseCriteria copy() {
        return new CourseCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public CentreFilter getCentre() {
        return centre;
    }

    public CentreFilter centre() {
        if (centre == null) {
            centre = new CentreFilter();
        }
        return centre;
    }

    public void setCentre(CentreFilter centre) {
        this.centre = centre;
    }

    public StringFilter getLanguage() {
        return language;
    }

    public StringFilter language() {
        if (language == null) {
            language = new StringFilter();
        }
        return language;
    }

    public void setLanguage(StringFilter language) {
        this.language = language;
    }

    public IntegerFilter getTeachingHour() {
        return teachingHour;
    }

    public IntegerFilter teachingHour() {
        if (teachingHour == null) {
            teachingHour = new IntegerFilter();
        }
        return teachingHour;
    }

    public void setTeachingHour(IntegerFilter teachingHour) {
        this.teachingHour = teachingHour;
    }

    public InstantFilter getStartedAt() {
        return startedAt;
    }

    public InstantFilter startedAt() {
        if (startedAt == null) {
            startedAt = new InstantFilter();
        }
        return startedAt;
    }

    public void setStartedAt(InstantFilter startedAt) {
        this.startedAt = startedAt;
    }

    public InstantFilter getFinishedAt() {
        return finishedAt;
    }

    public InstantFilter finishedAt() {
        if (finishedAt == null) {
            finishedAt = new InstantFilter();
        }
        return finishedAt;
    }

    public void setFinishedAt(InstantFilter finishedAt) {
        this.finishedAt = finishedAt;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CourseCriteria that = (CourseCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(centre, that.centre) &&
            Objects.equals(language, that.language) &&
            Objects.equals(teachingHour, that.teachingHour) &&
            Objects.equals(startedAt, that.startedAt) &&
            Objects.equals(finishedAt, that.finishedAt) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, centre, language, teachingHour, startedAt, finishedAt, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CourseCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (centre != null ? "centre=" + centre + ", " : "") +
            (language != null ? "language=" + language + ", " : "") +
            (teachingHour != null ? "teachingHour=" + teachingHour + ", " : "") +
            (startedAt != null ? "startedAt=" + startedAt + ", " : "") +
            (finishedAt != null ? "finishedAt=" + finishedAt + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
