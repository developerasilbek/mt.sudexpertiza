package uz.sudex.service;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.sudex.domain.References;
import uz.sudex.repository.ReferencesRepository;
import uz.sudex.service.dto.ReferencesDTO;
import uz.sudex.service.mapper.ReferencesMapper;

/**
 * Service Implementation for managing {@link References}.
 */
@Service
@Transactional
public class ReferencesService {

    private final Logger log = LoggerFactory.getLogger(ReferencesService.class);

    private final ReferencesRepository referencesRepository;

    private final ReferencesMapper referencesMapper;

    public ReferencesService(ReferencesRepository referencesRepository, ReferencesMapper referencesMapper) {
        this.referencesRepository = referencesRepository;
        this.referencesMapper = referencesMapper;
    }

    /**
     * Save a references.
     *
     * @param referencesDTO the entity to save.
     * @return the persisted entity.
     */
    public ReferencesDTO save(ReferencesDTO referencesDTO) {
        log.debug("Request to save References : {}", referencesDTO);
        References references = referencesMapper.toEntity(referencesDTO);
        references = referencesRepository.save(references);
        return referencesMapper.toDto(references);
    }

    /**
     * Update a references.
     *
     * @param referencesDTO the entity to save.
     * @return the persisted entity.
     */
    public ReferencesDTO update(ReferencesDTO referencesDTO) {
        log.debug("Request to update References : {}", referencesDTO);
        References references = referencesMapper.toEntity(referencesDTO);
        references = referencesRepository.save(references);
        return referencesMapper.toDto(references);
    }

    /**
     * Partially update a references.
     *
     * @param referencesDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ReferencesDTO> partialUpdate(ReferencesDTO referencesDTO) {
        log.debug("Request to partially update References : {}", referencesDTO);

        return referencesRepository
            .findById(referencesDTO.getId())
            .map(existingReferences -> {
                referencesMapper.partialUpdate(existingReferences, referencesDTO);

                return existingReferences;
            })
            .map(referencesRepository::save)
            .map(referencesMapper::toDto);
    }

    /**
     * Get all the references.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ReferencesDTO> findAll(Pageable pageable) {
        log.debug("Request to get all References");
        return referencesRepository.findAll(pageable).map(referencesMapper::toDto);
    }

    /**
     * Get one references by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ReferencesDTO> findOne(Long id) {
        log.debug("Request to get References : {}", id);
        return referencesRepository.findById(id).map(referencesMapper::toDto);
    }

    /**
     * Delete the references by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete References : {}", id);
        referencesRepository.deleteById(id);
    }
}
