package uz.sudex.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;
import uz.sudex.domain.*; // for static metamodels
import uz.sudex.domain.Glossary;
import uz.sudex.repository.GlossaryRepository;
import uz.sudex.service.criteria.GlossaryCriteria;
import uz.sudex.service.dto.GlossaryDTO;
import uz.sudex.service.mapper.GlossaryMapper;

/**
 * Service for executing complex queries for {@link Glossary} entities in the database.
 * The main input is a {@link GlossaryCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link GlossaryDTO} or a {@link Page} of {@link GlossaryDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class GlossaryQueryService extends QueryService<Glossary> {

    private final Logger log = LoggerFactory.getLogger(GlossaryQueryService.class);

    private final GlossaryRepository glossaryRepository;

    private final GlossaryMapper glossaryMapper;

    public GlossaryQueryService(GlossaryRepository glossaryRepository, GlossaryMapper glossaryMapper) {
        this.glossaryRepository = glossaryRepository;
        this.glossaryMapper = glossaryMapper;
    }

    /**
     * Return a {@link List} of {@link GlossaryDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<GlossaryDTO> findByCriteria(GlossaryCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Glossary> specification = createSpecification(criteria);
        return glossaryMapper.toDto(glossaryRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link GlossaryDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<GlossaryDTO> findByCriteria(GlossaryCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Glossary> specification = createSpecification(criteria);
        return glossaryRepository.findAll(specification, page).map(glossaryMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(GlossaryCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Glossary> specification = createSpecification(criteria);
        return glossaryRepository.count(specification);
    }

    /**
     * Function to convert {@link GlossaryCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Glossary> createSpecification(GlossaryCriteria criteria) {
        Specification<Glossary> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Glossary_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Glossary_.name));
            }
            if (criteria.getAssessmentId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getAssessmentId(),
                            root -> root.join(Glossary_.assessment, JoinType.LEFT).get(Assessment_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
