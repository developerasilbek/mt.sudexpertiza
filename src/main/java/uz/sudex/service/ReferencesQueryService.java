package uz.sudex.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;
import uz.sudex.domain.*; // for static metamodels
import uz.sudex.domain.References;
import uz.sudex.repository.ReferencesRepository;
import uz.sudex.service.criteria.ReferencesCriteria;
import uz.sudex.service.dto.ReferencesDTO;
import uz.sudex.service.mapper.ReferencesMapper;

/**
 * Service for executing complex queries for {@link References} entities in the database.
 * The main input is a {@link ReferencesCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ReferencesDTO} or a {@link Page} of {@link ReferencesDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ReferencesQueryService extends QueryService<References> {

    private final Logger log = LoggerFactory.getLogger(ReferencesQueryService.class);

    private final ReferencesRepository referencesRepository;

    private final ReferencesMapper referencesMapper;

    public ReferencesQueryService(ReferencesRepository referencesRepository, ReferencesMapper referencesMapper) {
        this.referencesRepository = referencesRepository;
        this.referencesMapper = referencesMapper;
    }

    /**
     * Return a {@link List} of {@link ReferencesDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ReferencesDTO> findByCriteria(ReferencesCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<References> specification = createSpecification(criteria);
        return referencesMapper.toDto(referencesRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ReferencesDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ReferencesDTO> findByCriteria(ReferencesCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<References> specification = createSpecification(criteria);
        return referencesRepository.findAll(specification, page).map(referencesMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ReferencesCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<References> specification = createSpecification(criteria);
        return referencesRepository.count(specification);
    }

    /**
     * Function to convert {@link ReferencesCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<References> createSpecification(ReferencesCriteria criteria) {
        Specification<References> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), References_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), References_.name));
            }
            if (criteria.getAssessmentId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getAssessmentId(),
                            root -> root.join(References_.assessment, JoinType.LEFT).get(Assessment_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
