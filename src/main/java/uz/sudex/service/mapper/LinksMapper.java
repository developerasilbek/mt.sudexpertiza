package uz.sudex.service.mapper;

import org.mapstruct.*;
import uz.sudex.domain.Assessment;
import uz.sudex.domain.Links;
import uz.sudex.service.dto.AssessmentDTO;
import uz.sudex.service.dto.LinksDTO;

/**
 * Mapper for the entity {@link Links} and its DTO {@link LinksDTO}.
 */
@Mapper(componentModel = "spring")
public interface LinksMapper extends EntityMapper<LinksDTO, Links> {
    @Mapping(target = "assessment", source = "assessment", qualifiedByName = "assessmentId")
    LinksDTO toDto(Links s);

    @Named("assessmentId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    AssessmentDTO toDtoAssessmentId(Assessment assessment);
}
