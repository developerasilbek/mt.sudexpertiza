package uz.sudex.service.mapper;

import org.mapstruct.*;
import uz.sudex.domain.Assessment;
import uz.sudex.domain.Topic;
import uz.sudex.service.dto.AssessmentDTO;
import uz.sudex.service.dto.TopicDTO;

/**
 * Mapper for the entity {@link Assessment} and its DTO {@link AssessmentDTO}.
 */
@Mapper(componentModel = "spring")
public interface AssessmentMapper extends EntityMapper<AssessmentDTO, Assessment> {
    @Mapping(target = "topic", source = "topic", qualifiedByName = "topicId")
    AssessmentDTO toDto(Assessment s);

    @Named("topicId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    TopicDTO toDtoTopicId(Topic topic);
}
