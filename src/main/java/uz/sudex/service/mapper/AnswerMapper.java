package uz.sudex.service.mapper;

import org.mapstruct.*;
import uz.sudex.domain.Answer;
import uz.sudex.domain.Question;
import uz.sudex.service.dto.AnswerDTO;
import uz.sudex.service.dto.QuestionDTO;

/**
 * Mapper for the entity {@link Answer} and its DTO {@link AnswerDTO}.
 */
@Mapper(componentModel = "spring")
public interface AnswerMapper extends EntityMapper<AnswerDTO, Answer> {
    @Mapping(target = "question", source = "question", qualifiedByName = "questionName")
    AnswerDTO toDto(Answer s);

    @Named("questionName")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    QuestionDTO toDtoQuestionName(Question question);
}
