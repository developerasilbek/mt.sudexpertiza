package uz.sudex.service.mapper;

import org.mapstruct.*;
import uz.sudex.domain.Modul;
import uz.sudex.domain.Topic;
import uz.sudex.service.dto.ModulDTO;
import uz.sudex.service.dto.TopicDTO;

/**
 * Mapper for the entity {@link Topic} and its DTO {@link TopicDTO}.
 */
@Mapper(componentModel = "spring")
public interface TopicMapper extends EntityMapper<TopicDTO, Topic> {
    @Mapping(target = "modul", source = "modul", qualifiedByName = "modulName")
    TopicDTO toDto(Topic s);

    @Named("modulName")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    ModulDTO toDtoModulName(Modul modul);
}
