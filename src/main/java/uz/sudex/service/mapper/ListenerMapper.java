package uz.sudex.service.mapper;

import org.mapstruct.*;
import uz.sudex.domain.Attachment;
import uz.sudex.domain.Course;
import uz.sudex.domain.Listener;
import uz.sudex.domain.User;
import uz.sudex.service.dto.AttachmentDTO;
import uz.sudex.service.dto.CourseDTO;
import uz.sudex.service.dto.ListenerDTO;
import uz.sudex.service.dto.UserDTO;

/**
 * Mapper for the entity {@link Listener} and its DTO {@link ListenerDTO}.
 */
@Mapper(componentModel = "spring")
public interface ListenerMapper extends EntityMapper<ListenerDTO, Listener> {
    @Mapping(target = "user", source = "user", qualifiedByName = "userId")
    @Mapping(target = "contract", source = "contract", qualifiedByName = "attachmentId")
    @Mapping(target = "course", source = "course", qualifiedByName = "courseId")
    ListenerDTO toDto(Listener s);

    @Named("userId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    UserDTO toDtoUserId(User user);

    @Named("attachmentId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    AttachmentDTO toDtoAttachmentId(Attachment attachment);

    @Named("courseId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CourseDTO toDtoCourseId(Course course);
}
