package uz.sudex.service.mapper;

import org.mapstruct.*;
import uz.sudex.domain.Course;
import uz.sudex.service.dto.CourseDTO;

/**
 * Mapper for the entity {@link Course} and its DTO {@link CourseDTO}.
 */
@Mapper(componentModel = "spring")
public interface CourseMapper extends EntityMapper<CourseDTO, Course> {}
