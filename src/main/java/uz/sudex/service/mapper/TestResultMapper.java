package uz.sudex.service.mapper;

import org.mapstruct.*;
import uz.sudex.domain.TestResult;
import uz.sudex.domain.TopicTest;
import uz.sudex.service.dto.TestResultDTO;
import uz.sudex.service.dto.TopicTestDTO;

/**
 * Mapper for the entity {@link TestResult} and its DTO {@link TestResultDTO}.
 */
@Mapper(componentModel = "spring")
public interface TestResultMapper extends EntityMapper<TestResultDTO, TestResult> {
    @Mapping(target = "test", source = "test", qualifiedByName = "topicTestId")
    TestResultDTO toDto(TestResult s);

    @Named("topicTestId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    TopicTestDTO toDtoTopicTestId(TopicTest topicTest);
}
