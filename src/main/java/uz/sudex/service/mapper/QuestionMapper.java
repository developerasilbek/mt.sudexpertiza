package uz.sudex.service.mapper;

import org.mapstruct.*;
import uz.sudex.domain.Question;
import uz.sudex.domain.TopicTest;
import uz.sudex.service.dto.QuestionDTO;
import uz.sudex.service.dto.TopicTestDTO;

/**
 * Mapper for the entity {@link Question} and its DTO {@link QuestionDTO}.
 */
@Mapper(componentModel = "spring")
public interface QuestionMapper extends EntityMapper<QuestionDTO, Question> {
    @Mapping(target = "test", source = "test", qualifiedByName = "topicTestName")
    QuestionDTO toDto(Question s);

    @Named("topicTestName")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    TopicTestDTO toDtoTopicTestName(TopicTest topicTest);
}
