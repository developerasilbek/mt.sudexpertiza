package uz.sudex.service.mapper;

import org.mapstruct.*;
import uz.sudex.domain.Assessment;
import uz.sudex.domain.TopicTest;
import uz.sudex.service.dto.AssessmentDTO;
import uz.sudex.service.dto.TopicTestDTO;

/**
 * Mapper for the entity {@link TopicTest} and its DTO {@link TopicTestDTO}.
 */
@Mapper(componentModel = "spring")
public interface TopicTestMapper extends EntityMapper<TopicTestDTO, TopicTest> {
    @Mapping(target = "assessment", source = "assessment", qualifiedByName = "assessmentId")
    TopicTestDTO toDto(TopicTest s);

    @Named("assessmentId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    AssessmentDTO toDtoAssessmentId(Assessment assessment);
}
