package uz.sudex.service.mapper;

import org.mapstruct.*;
import uz.sudex.domain.Specialization;
import uz.sudex.service.dto.SpecializationDTO;

/**
 * Mapper for the entity {@link Specialization} and its DTO {@link SpecializationDTO}.
 */
@Mapper(componentModel = "spring")
public interface SpecializationMapper extends EntityMapper<SpecializationDTO, Specialization> {}
