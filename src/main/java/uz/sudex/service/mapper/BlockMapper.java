package uz.sudex.service.mapper;

import org.mapstruct.*;
import uz.sudex.domain.Block;
import uz.sudex.domain.Course;
import uz.sudex.domain.Specialization;
import uz.sudex.service.dto.BlockDTO;
import uz.sudex.service.dto.CourseDTO;
import uz.sudex.service.dto.SpecializationDTO;

/**
 * Mapper for the entity {@link Block} and its DTO {@link BlockDTO}.
 */
@Mapper(componentModel = "spring")
public interface BlockMapper extends EntityMapper<BlockDTO, Block> {
    @Mapping(target = "course", source = "course", qualifiedByName = "courseId")
    @Mapping(target = "specialization", source = "specialization", qualifiedByName = "specializationId")
    BlockDTO toDto(Block s);

    @Named("courseId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CourseDTO toDtoCourseId(Course course);

    @Named("specializationId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    SpecializationDTO toDtoSpecializationId(Specialization specialization);
}
