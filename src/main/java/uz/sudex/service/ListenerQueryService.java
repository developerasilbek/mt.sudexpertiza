package uz.sudex.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;
import uz.sudex.domain.*; // for static metamodels
import uz.sudex.domain.Listener;
import uz.sudex.repository.ListenerRepository;
import uz.sudex.service.criteria.ListenerCriteria;
import uz.sudex.service.dto.ListenerDTO;
import uz.sudex.service.mapper.ListenerMapper;

/**
 * Service for executing complex queries for {@link Listener} entities in the database.
 * The main input is a {@link ListenerCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ListenerDTO} or a {@link Page} of {@link ListenerDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ListenerQueryService extends QueryService<Listener> {

    private final Logger log = LoggerFactory.getLogger(ListenerQueryService.class);

    private final ListenerRepository listenerRepository;

    private final ListenerMapper listenerMapper;

    public ListenerQueryService(ListenerRepository listenerRepository, ListenerMapper listenerMapper) {
        this.listenerRepository = listenerRepository;
        this.listenerMapper = listenerMapper;
    }

    /**
     * Return a {@link List} of {@link ListenerDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ListenerDTO> findByCriteria(ListenerCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Listener> specification = createSpecification(criteria);
        return listenerMapper.toDto(listenerRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ListenerDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ListenerDTO> findByCriteria(ListenerCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Listener> specification = createSpecification(criteria);
        return listenerRepository.findAll(specification, page).map(listenerMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ListenerCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Listener> specification = createSpecification(criteria);
        return listenerRepository.count(specification);
    }

    /**
     * Function to convert {@link ListenerCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Listener> createSpecification(ListenerCriteria criteria) {
        Specification<Listener> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Listener_.id));
            }
            if (criteria.getPhone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhone(), Listener_.phone));
            }
            if (criteria.getRegion() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRegion(), Listener_.region));
            }
            if (criteria.getWorkplace() != null) {
                specification = specification.and(buildStringSpecification(criteria.getWorkplace(), Listener_.workplace));
            }
            if (criteria.getDateOfBirth() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDateOfBirth(), Listener_.dateOfBirth));
            }
            if (criteria.getCondition() != null) {
                specification = specification.and(buildSpecification(criteria.getCondition(), Listener_.condition));
            }
            if (criteria.getType() != null) {
                specification = specification.and(buildSpecification(criteria.getType(), Listener_.type));
            }
            if (criteria.getUserId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getUserId(), root -> root.join(Listener_.user, JoinType.LEFT).get(User_.id))
                    );
            }
            if (criteria.getContractId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getContractId(),
                            root -> root.join(Listener_.contract, JoinType.LEFT).get(Attachment_.id)
                        )
                    );
            }
            if (criteria.getCourseId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getCourseId(), root -> root.join(Listener_.course, JoinType.LEFT).get(Course_.id))
                    );
            }
        }
        return specification;
    }
}
