/**
 * View Models used by Spring MVC REST controllers.
 */
package uz.sudex.web.rest.vm;
