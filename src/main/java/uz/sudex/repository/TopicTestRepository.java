package uz.sudex.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import uz.sudex.domain.TopicTest;

/**
 * Spring Data JPA repository for the TopicTest entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TopicTestRepository extends JpaRepository<TopicTest, Long>, JpaSpecificationExecutor<TopicTest> {}
