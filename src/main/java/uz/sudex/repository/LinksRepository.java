package uz.sudex.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import uz.sudex.domain.Links;

/**
 * Spring Data JPA repository for the Links entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LinksRepository extends JpaRepository<Links, Long>, JpaSpecificationExecutor<Links> {}
