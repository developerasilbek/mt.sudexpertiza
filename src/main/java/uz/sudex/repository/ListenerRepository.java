package uz.sudex.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import uz.sudex.domain.Listener;

/**
 * Spring Data JPA repository for the Listener entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ListenerRepository extends JpaRepository<Listener, Long>, JpaSpecificationExecutor<Listener> {}
