package uz.sudex.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import uz.sudex.domain.References;

/**
 * Spring Data JPA repository for the References entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReferencesRepository extends JpaRepository<References, Long>, JpaSpecificationExecutor<References> {}
