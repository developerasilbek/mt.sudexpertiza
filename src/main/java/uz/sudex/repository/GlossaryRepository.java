package uz.sudex.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import uz.sudex.domain.Glossary;

/**
 * Spring Data JPA repository for the Glossary entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GlossaryRepository extends JpaRepository<Glossary, Long>, JpaSpecificationExecutor<Glossary> {}
