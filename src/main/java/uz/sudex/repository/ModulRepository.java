package uz.sudex.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import uz.sudex.domain.Modul;

/**
 * Spring Data JPA repository for the Modul entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ModulRepository extends JpaRepository<Modul, Long>, JpaSpecificationExecutor<Modul> {}
